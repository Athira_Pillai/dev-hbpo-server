describe('test text', function() {
  it('should check title', function() {
    browser.waitForAngularEnabled(false);
   
    browser.get('http://localhost:4200/login');
    expect(browser.getTitle()).toEqual('MineIntel');

    
  });
  it('should check breadcrumb to be true', function() {
    var de = element(by.xpath('//*[@class="breadcrumb-item list active"]/span'));
     expect(de.getText()).toEqual('Login');
   

})
it('should check breadcrumb to be false', function() {
  var de = element(by.xpath('//*[@class="breadcrumb-item list active"]/span'));
   expect(de.getText()).not.toEqual('Login1');
 

})});