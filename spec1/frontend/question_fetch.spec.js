describe("Test questionService", function() {

  var qs;
  var qsm;

  beforeEach(angular.mock.module("userBubble"));

  beforeEach(inject(function(_questionService_, _questionSequenceManager_) {
    qs = _questionService_;
    qsm = _questionSequenceManager_;

    qsm._test.clearCache();
  }));
  it("has no questions to begin with", function() {
    expect(qsm._test.originalQuestions()).toBeNull();
    expect(qsm._test.cachedQuestions()).toBeNull();
    expect(qsm._test.currentIndex()).toEqual(-1);
    expect(qsm._test.nextFetchDate()).toBeNull();
  });

  it("fetches questions when there are none in the cache", inject(function($httpBackend) {
    $httpBackend.expect('GET', '/questions').respond(200, [{_id: 1}, {_id: 2}]);

    spyOn(qs, "getQuestions").and.callThrough();

    qsm.current()
      .then(function(question) {
        //console.log(question.next._id);
        expect(question.current._id).toBeDefined();
        expect(question.next._id).toBeDefined();
        expect(qsm._test.nextFetchDate()).not.toBeNull();
      });

    expect(qs.getQuestions).toHaveBeenCalledTimes(1);

    $httpBackend.flush();

    qsm.current();
    expect(qs.getQuestions).toHaveBeenCalledTimes(1);
  }));

  describe("Test proper fetch with cached data", function() {
    beforeEach(inject(function($httpBackend) {
      spyOn(qs, "getQuestions").and.callThrough();

      $httpBackend.expect('GET', '/questions').respond(200, [{_id: 1}, {_id: 2}]);
      qsm.current();
      $httpBackend.flush();
    }));

    it("should start at index 0", function() {
      expect(qsm._test.currentIndex()).toEqual(0);
      qsm.next();
      expect(qsm._test.currentIndex()).toEqual(1);
    });

    it("does not fetch if there are questions in the cache", inject(function($httpBackend) {
      $httpBackend.expect('GET', '/questions').respond(200, [{}]);
      expect(qsm._test.cachedQuestions().length).toEqual(2);
      expect(qsm._test.currentIndex()).not.toEqual(-1);
      expect(qsm._test.nextFetchDate()).not.toBeNull();
      qsm.current();
      expect($httpBackend.flush).toThrowError("No pending request to flush !");
    }));

    it("calculates the next fetch date", function() {
      function expectDate(d1, d2, hours) {
        expect(nextFetchDate.getHours()).toEqual(hours);
        expect(d1.getFullYear()).toEqual(d2.getFullYear());
        expect(d1.getMonth()).toEqual(d2.getMonth());
        expect(d1.getDate()).toEqual(d2.getDate());
        expect(d1.getDay()).toEqual(d2.getDay());
        expect(d2.getMinutes()).toEqual(0);
        expect(d2.getSeconds()).toEqual(0);
        expect(d2.getMilliseconds()).toEqual(0);
      }

      var today = new Date(1999, 11, 31, 5, 59, 59, 999);

      jasmine.clock().install();

      jasmine.clock().mockDate(today);
      var nextFetchDate = qsm._test.calculateNextFetchDate();
      expectDate(today, nextFetchDate, 6);

      today.setHours(17);
      today.setMinutes(59);

      jasmine.clock().mockDate(today);
      nextFetchDate = qsm._test.calculateNextFetchDate();
      expectDate(today, nextFetchDate, 18);

      today.setHours(23);
      today.setMinutes(59);

      jasmine.clock().mockDate(today);
      nextFetchDate = qsm._test.calculateNextFetchDate();

      jasmine.clock().uninstall();

      expect(nextFetchDate.getFullYear()).toEqual(2000);
      expect(nextFetchDate.getMonth()).toEqual(0);
      expect(nextFetchDate.getDate()).toEqual(1);
      expect(nextFetchDate.getDay()).toEqual(today.getDay() + 1);
      expect(nextFetchDate.getHours()).toEqual(6);
      expect(nextFetchDate.getMinutes()).toEqual(0);
      expect(nextFetchDate.getSeconds()).toEqual(0);
      expect(nextFetchDate.getMilliseconds()).toEqual(0);
    });

    it("fetches new questions once a new shift begins", inject(function($httpBackend) {
      $httpBackend.expect('GET', '/questions').respond(200, [{_id: 1}, {_id: 2}, {_id: 3}]);

      jasmine.clock().install();
      var nextFetchDate = new Date(qsm._test.nextFetchDate().valueOf() + 1);
      jasmine.clock().mockDate(nextFetchDate);

      qsm.current()
        .then(function() {
          expect(qsm._test.cachedQuestions().length).toEqual(3);
          expect(qsm._test.currentIndex()).toEqual(0);
        });

      $httpBackend.flush();

      jasmine.clock().uninstall();
    }));
  });

  describe("Cache answers when there's no network connection", function() {
    let online = false;
    const a1 = {"questionId": 1, "answerId": 2, "skipped": false, "dontKnow": false};
    const a2 = {"questionId": 2, "answerId": 3, "skipped": true, "dontKnow": false};

    beforeEach(function() {
      qs._test.clearCache();

      online = false;
      isNetworkAvailable = jasmine.createSpy("networkAvailableSpy").and.callFake(function() {
        return online;
      });

      calculateTimeOffline = jasmine.createSpy("timeOfflineSpy");
    });

    it("has no answers in the cache initially", function() {
      const answers = qs._test.cachedAnswers();
      expect(answers.length).toEqual(0);
    });

    it("caches answers in local storage", function() {
      qs.sendAnswer(a1);
      const answers = qs._test.cachedAnswers();
      expect(answers.length).toEqual(1);
      expect(answers[0].cacheTime).not.toBeUndefined();
    });

    it("sends all cached answers when network is available again", inject(function($httpBackend) {
      qs.sendAnswer(a1);

      online = true;
      qs.sendAnswer(a2);

      $httpBackend.expectPOST('/questions/answers', [a1, a2]).respond(200, '');
      $httpBackend.flush();

      const answers = qs._test.cachedAnswers();
      expect(answers.length).toEqual(0);
    }));

    it("sends all cached answers on application load", inject(function($httpBackend, _$controller_) {
      qs.sendAnswer(a1);
      qs.sendAnswer(a2);
      expect(qs._test.cachedAnswers().length).toEqual(2);

      online = true;
      const $controller = _$controller_;
      $controller('QuestionAnswerController', { $scope: {} });

      $httpBackend.expectGET('/questions').respond(200, [{_id: 1}, {_id: 2}]);
      $httpBackend.expectGET('/notification').respond(200, "");
      $httpBackend.expectPOST('/questions/answers', [a1, a2]).respond(200, '');
      $httpBackend.flush();

      expect(qs._test.cachedAnswers().length).toEqual(0);
    }));

    it("caches answers if an error occurs while sending", inject(function($httpBackend) {
      qs.sendAnswer(a1);

      online = true;
      qs.sendAnswer(a2);

      $httpBackend.expectPOST('/questions/answers', [a1, a2]).respond(500, '');
      $httpBackend.flush();

      const answers = qs._test.cachedAnswers();
      expect(answers.length).toEqual(2);
    }));

    it("uses previously-cached questions if we can't fetch a new set", inject(function($httpBackend) {
      online = true;
      $httpBackend.expect('GET', '/questions').respond(200, [{_id: 1}, {_id: 2}]);
      qsm.current();
      $httpBackend.flush();

      qsm.next();
      expect(qsm._test.currentIndex()).toEqual(1);

      jasmine.clock().install();
      var nextFetchDate = new Date(qsm._test.nextFetchDate().valueOf() + 1);
      jasmine.clock().mockDate(nextFetchDate);

      online = false;
      qsm.current();
      expect(qsm._test.cachedQuestions().length).toEqual(2);
      expect(qsm._test.currentIndex()).toEqual(0);

      jasmine.clock().uninstall();
    }));

    describe("Undo cached responses", function() {
      it("should remove a previously-cached answer if the user resubmits a new one", inject(function() {
        const a3 = {"questionId": 1, "answerId": 2, "skipped": false, "dontKnow": false};
        const a4 = {"questionId": 1, "answerId": 3, "skipped": false, "dontKnow": false};

        qs.sendAnswer(a3);
        qs.sendAnswer(a4);
        const answers = qs._test.cachedAnswers();
        expect(answers.length).toEqual(1);
      }));
    });
  });
});
