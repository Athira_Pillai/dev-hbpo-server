const rawData = [
  {
    question_id: 1,
    question: 'Do vehicles respect site following distances?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Following distances are not respected on site',
       'I don\'t know' ],
    help_text: 'Following distances on site for haul trucks are 140m in the winter and 70m in the summer.',
    perf_name: 'Following Distance'
  },
  { question_id: 25,
    question: 'Do vehicles follow the posted speed limits?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Speed limits were not followed on site',
       'I don\'t know' ],
       help_text: 'Speed limits within active mining areas or heavy equipment areas that are designated with left hand traffic are: Haul Roads – 60km/h; Dumping and Loading Areas – 20 km/h; Shops, Ready Lines, and Fuel Bays – 15km/h.',
       perf_name: 'Speed Limits'
  },
  { question_id: 26,
    question: 'Was signage appropriate for communicating to drivers?',
    answers:
     [ 'Signage communicated appropriate messages',
       'Signage was misleading',
       'Old signage was not removed',
       'Signage used on site is inconsistent',
       'I don\'t know' ],
       help_text: 'Signage availability: all necessary resources such as information cards, barrier tape and barricade materials are in a known location and can be accessed when required.  Signage Informative: barriers and barricades describe hazards and has installation date.  Signage Visibility: Can be seen by operators of vehicles and pedestrians from front on and approaching angles (i.e. not hidden around corners)',
       perf_name: 'Signage'
  },
  { question_id: 27,
    question: 'Are marked pedestrian crossings present at required locations?',
    answers:
     [ 'All locations are marked',
       'Most locations are marked',
       'Some locations are marked',
       'Pedestrian crossings marked inconsistently',
       'I don\'t know' ],
    help_text: 'Pedestrian traffic has demarcation to separate pedestrians from vehicles in areas where the two will interact.  At crossings, there is sufficient signage and protected areas for pedestrians to cross vehicles roads safely.',
    perf_name: 'Pedestrian Crossings'
  },
  { question_id: 28,
    question: 'Do road conditions meet expected standards (friction, condition, grade, angle, berm, weather)?',
    answers:
     [ 'Road and berm quality was high',
       'Some areas of roads and berms required grading or additional work',
       'Road and berm quality was inconsistent',
       'Road and berm quality was poor.',
       'I don\'t know' ],
       help_text: 'Road conditions - \nFriction: road design caused wheels to slip\nCondition: not affected by weather or loose rocks\nGrade: areas of steep slopes are marked\nAngle: roads did not have lateral banking that made navigation difficult\nBerms: designed to half the height of the tallest tire on site and pitched at 45°\nWeather: adverse weather does not affect the quality of roads including ruts and washing away berms',
       perf_name: 'Road Conditions'
  },
  { question_id: 29,
    question: 'Are communications with other vehicles reliable?',
    answers:
     [ 'Always',
       'Mostly',
       'Somewhat',
       'Communication channels were unreliable',
       'I don\'t know' ],
       help_text: 'Vehicle Radio signals are strong and consistent across the site.  Radio channels are known and complied with across the site.',
       perf_name: 'Communication'
  },
  { question_id: 30,
    question: 'Do vehicles meet visibility requirements?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Visibility of vehicles is inconsistent',
       'I don\'t know' ],
       help_text: 'Lights: All lights functioning that helps vehicles be seen from all angles when moving forward, reversing or in a stationary position; Buggy whips: Tall flags in the back of light vehicles that allow for greater visibility; Reflection: decals and permanent reflectors that are clean allow for increased visibility by other vehicles; Additional visibility aides may be used as well.',
       perf_name: 'Vehicle Visibility'
  },
  { question_id: 31,
    question: 'Were vehicles parked in designated areas?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Vehicle parking is inconsistent',
       'I don\'t know' ],
       help_text: 'Parking areas are those areas that are designated for parked heavy and light vehicles.  These areas are clearly demarcated, have good flow of traffic in and out and facilitate pull through or safe reverse parking.',
       perf_name: 'Vehicle Parking'
  },
  { question_id: 32,
    question: 'Do vehicles respect right of way (RoW) of other vehicles?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'RoW is inconsistent',
       'I don\'t know' ],
       help_text: 'Surface right-of- way shall be granted in the following order:\no Pedestrians\no Ambulance or other emergency mobile equipment \no Explosives transport\no Blind side equipment\no Haulage equipment\no Road maintenance equipment\no Light vehicles\nAnywhere haul roads merge, right-of-way shall be given to trucks on the left due to the blind side merging with traffic, unless signage dictates otherwise.',
       perf_name: 'Right of Way'
  },
  { question_id: 33,
    question: 'Were vehicles operating in the correct areas of the site?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Inconsistent',
       'I don\'t know' ],
       help_text: 'Correct areas refer to areas of the site where certain types of vehicles have access and these areas are clearly demarcated',
       perf_name: 'Exclusion Zones'
  },
  { question_id: 34,
    question: 'Were access control procedures applied across the site?',
    answers:
     [ 'Always',
       'Mostly',
       'Inconsistent',
       'Not at all',
       'I don\'t know' ],
       help_text: 'Access controls are physical checks of vehicle upon entry to restricted area at all times and access stations are resourced accordingly.',
       perf_name: 'Access Control'
  },
  { question_id: 35,
    question: 'Were contractors\' practices consistent with that to of site vehicles?',
    answers: [ 'Always', 'Usually', 'Sometimes', 'Never', 'I don\'t know' ],
    help_text: 'Contractors are obliged to follow the same site rules as company vehicles as it relates to:\n- driving behaviours\n- speed limits\n- vehicle condition and requirements\n- access requirements',
    perf_name: 'Contractors\' Practices'
  },
  { question_id: 36,
    question: 'Were waste dump areas designed to standard?',
    answers: [ 'Always', 'Mostly', 'Sometimes', 'Not at all', 'I don\'t know' ],
    help_text: 'Waste dump traffic: Vehicles have known path to unload waste and clear exit path \nWaste dump design: berm and crest integrity is high\nWaste dump spotter: Spotters are available to support drivers; where spotters are not available, vehicles should dump short',
    perf_name: 'Waste Dumps'
  },
  { question_id: 37,
    question: 'Were mobile devices used in moving vehicles by operators?',
    answers: [ 'Never', 'Once', 'Sometimes', 'Almost always', 'I don\'t know' ],
    help_text: 'Site rules state that communication devices are not to be used by the operator while vehicle is in motion.',
    perf_name: 'Mobile Devices'
  },
  { question_id: 38,
    question: 'Do vehicles pass each other in prohibited passing areas?',
    answers:
     [ 'Never',
       'Rarely',
       'Sometimes',
       'Vehicles are passed regularly regardless of area of site',
       'I don\'t know' ],
       help_text: 'Passing (or overtaking) vehicles can be a dangerous manoeuvre.  Certain areas of the site cannot have vehicles overtaking.  Those areas of clearly demarcated and the practice does not take place.',
       perf_name: 'Passing Vehicles'
  },
  { question_id: 39,
    question: 'What are the traction conditions on the site?',
    answers:
     [ 'Excellent - no slippage',
       'Only one area where slippage occurrs',
       'Multiple areas where slippage occurrs',
       'Slippage is a regular occurrence',
       'I don\'t know' ],
       help_text: 'Traction control is very important in heavy equipment.  Through road condition, grade or other factors, maintaining tractor throughout each cycle should not be compromised.',
       perf_name: 'Traction'
  },
  { question_id: 40,
    question: 'Do non-permitted vehicles access prohibited roads?',
    answers: [ 'Never', 'Once', 'Sometimes', 'Usually', 'I don\'t know' ],
    help_text: 'Certain roads will only be accessible to designated vehicles for a variety of reasons.  Is this vehicle permitted to access prohibited areas of site?',
    perf_name: 'Permitted Access'
  },
  { question_id: 41,
    question: 'Are prohibited roads adequately barricaded to block vehicle access?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Barricading is poor and/or inconsistent',
       'I don\'t know' ],
       help_text: 'Prohibited roads should be adequately barricaded that accidental access should be discouraged by the design of the barricade (including berms, concrete or waste rock).  Also, barricades that are not required are removed in a timely manner.',
       perf_name: 'Barricading'
  },
  { question_id: 42,
    question: 'Are appropriate start up/reversing procedures followed?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Start up/reversing procedures not followed/inconsistent',
       'I don\'t know' ],
       help_text: 'Prior to starting or moving a piece of equipment, the following warning signals shall be used:\n• Starting a piece of equipment – ONE horn blast followed by a delay\n• Moving a piece of equipment forward – TWO horn blasts followed by a delay\n• Moving a piece of equipment in reverse – THREE horn blasts followed by a delay',
       perf_name: 'Start up/Reversing Procedures'
  },
  { question_id: 43,
    question: 'Are vehicles requiring escort/convoy accompanied by an appropriate vehicle?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Vehicle convoys and escorts were not followed across site',
       'I don\'t know' ],
       help_text: 'Escort vehicles are required for vehicles that have abnormal loads or require access to otherwise prohibited areas of the site.',
       perf_name: 'Escort / Convoy'
  },
  { question_id: 44,
    question: 'Are vehicles parking in tight spaces accompanied by flag personnel?',
    answers:
     [ 'Always',
       'Mostly',
       'Sometimes',
       'Tight parking not regularly controled by flag personnel',
       'I don\'t know' ],
       help_text: 'When parking vehicles in tight parking spots, best in class practice includes having one or two people direct the vehicle into place using flags and communicating directly with the operator of the heavy vehicle.',
       perf_name: 'Tight Parking'
  }
];

module.exports = exports = rawData;
