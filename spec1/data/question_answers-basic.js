const rawData = [
  {
    performance: null,
    answers: []
  },
  {
    performance: 1,
    answers: [
      {'answerId': 0},
      {'answerId': 0},
      {'answerId': 0},
      {'answerId': 0},
      {'answerId': 0}
    ]
  },
  {
    performance: 0.631,
    answers: [
      {'answerId': 1},
      {'answerId': 0},
      {'answerId': 1},
      {'answerId': 2},
      {'answerId': 3},
      {'answerId': 2},
      {'answerId': 1},
      {'answerId': 0},
      {'answerId': 0},
      {'answerId': 1}
    ]
  },
  {
    performance: 0.6336363636,
    close: true,
    answers: [
      {'answerId': -1, 'skipped': true},
      {'answerId': 1},
      {'answerId': 0},
      {'answerId': -1, 'skipped': true},
      {'answerId': 1},
      {'answerId': 2},
      {'answerId': -1, 'dontKnow': true},
      {'answerId': 3},
      {'answerId': -1, 'skipped': true},
      {'answerId': 2},
      {'answerId': 1},
      {'answerId': -1, 'skipped': true},
      {'answerId': 0},
      {'answerId': 0},
      {'answerId': -1, 'dontKnow': true},
      {'answerId': 1},
      {'answerId': 1}
    ]
  },
  {
    performance: null,
    answers: [
      {'answerId': -1, 'dontKnow': true},
      {'answerId': -1, 'skipped': true},
      {'answerId': -1, 'dontKnow': true},
      {'answerId': -1, 'dontKnow': true},
      {'answerId': -1, 'skipped': true},
      {'answerId': -1, 'dontKnow': true}
    ]
  }
];

module.exports = exports = rawData;
