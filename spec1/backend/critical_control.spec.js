describe('Critical Control', () => {
  const proxyquire =require('proxyquire');
  const moment = require('moment');

  const startISODate = '2017-05-20';
  const start = new Date(startISODate);
  const end = new Date('2017-05-21');
  const answerModel = require('../../src/answer');

  answerModelStub = (dateRange) => {
    fetch = (callback) => {
      const dayStart = new Date(start.valueOf());
      dayStart.setHours(6);

      if (moment(dateRange.start).isSame(moment(start).hours(6))) {
        const result = require(`../data/question_answers-${startISODate}-day`);
        callback(null, result);
      } else {
        const result = require(`../data/question_answers-${startISODate}-night`);
        callback(null, result);
      }
    }

    pointsPerAnswer = (answerID) => {
      return answerModel().pointsPerAnswer(answerID);
    },

    averagePointsPerOperator = (answers) => {
      return answerModel().averagePointsPerOperator(answers);
    }

    return {
      fetch,
      pointsPerAnswer,
      averagePointsPerOperator
    };
  }
  const criticalControl = proxyquire('../../src/critical_control',{
    './answer': answerModelStub
  });
  it('should not crash', () => {
    const cc = criticalControl('traffic-management');
    expect(cc).not.toBeNull();
  });

  it('should return performance data', () => {
    mockDb = {};
    const cc = criticalControl('traffic-management', {start, end}, mockDb);
    cc.performance((result) => {
      expect(result.length).toEqual(1);
      expect(result[0].toFixed(10)).toEqual('0.9132832244');
    });
  });

  it('should calculate the response rate', () => {
    const mockDb = {};
    const questions = require('../data/questions');
    const cc = criticalControl('traffic-management', {start, end}, mockDb);
    cc.responseRate(questions, (result) => {
      expect(result).toEqual({
        day: 0.8412698412698413,
        average: 0.8412698412698413
      });
    });
  });
});
