describe("Date Iterator", function() {
  const dateIterator = require('../../src/date_iterator');

  const dateTemplate = '2017-01-0';

  const start = new Date(`${dateTemplate}1`);
  const end = new Date(`${dateTemplate}5`);

  it('should not crash', () => {
    const i = dateIterator(start, end);
    expect(i).not.toBe(null);
    expect(i.isValid()).toBe(true);
  });

  it('should create an invalid iterator', () => {
    const di = dateIterator;
    expect(di().isValid()).toBe(false);
    expect(di(end, start).isValid()).toBe(false);
    expect(di(start, start).isValid()).toBe(false);
  });

  it('should iterate over a simple date range', () => {
    const i = dateIterator(start, end);

    const expectedDates = [];
    for (let i = 1; i < 5; i++) {
      expectedDates.push(new Date(`${dateTemplate}${i}`));
    }

    let expectedIndex = 0;
    this.callback = (date, index) => {
      expect(date instanceof Date).toBe(true);
      expect(expectedIndex++).toBe(index);
      expect(expectedDates[index]).toEqual(date);
    };
    spyOn(this, 'callback').and.callThrough();

    i.forEach(this.callback);

    expect(this.callback.calls.count()).toEqual(expectedDates.length);
  });

  it('should iterate over 12-hour time periods', () => {
    const i = dateIterator(start, end, true);

    const expectedDates = [];
    for (let i = 1; i < 5; i++) {
      for (let j = 0; j < 2; j++) {
        const date = new Date(`${dateTemplate}${i}`);
        date.setHours((j === 0) ? 6 : 18);
        expectedDates.push(date);
      }
    }

    this.callback = (date, index, dayIndex) => {
      expect(expectedDates[index]).toEqual(date);
      expect(expectedDates[dayIndex * 2].toDateString()).toBe(expectedDates[dayIndex * 2 + 1].toDateString());
    };
    spyOn(this, 'callback').and.callThrough();

    i.forEach(this.callback);

    expect(this.callback.calls.count()).toEqual(expectedDates.length);
  });
});
