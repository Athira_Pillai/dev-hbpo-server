const moment = require("moment");

describe("Operator Shift", () => {
  const operatorShift = require('../../src/operator_shift')();

  it('should not crash', () => {
    expect(operatorShift).not.toBeNull();
  });

  // By default, moment.js will create dates in the system's timezone. In app.js,
  // we explicitly set the default timezone to GMT.
  it('should return the shift date range for a given date', () => {
    let date = moment('2017-06-22T07:00:00');
    let shiftDate = moment('2017-06-22T00:00:00').startOf('day');
    let dateRange = operatorShift.dateRange(date);
    expect(dateRange.start.valueOf()).toBe(shiftDate.hours(6).toDate().valueOf());
    expect(dateRange.end.valueOf()).toBe(shiftDate.add(12, 'hours').toDate().valueOf());

    date = moment('2017-06-22T05:59:59');
    shiftDate = moment('2017-06-21T00:00:00').startOf('day');
    dateRange = operatorShift.dateRange(date);
    expect(dateRange.start.valueOf()).toBe(shiftDate.hours(18).toDate().valueOf());
    expect(dateRange.end.valueOf()).toBe(shiftDate.add(12, 'hours').toDate().valueOf());

    date = moment('2017-06-22T18:20:00');
    shiftDate = moment('2017-06-22T00:00:00').startOf('day');
    dateRange = operatorShift.dateRange(date);
    expect(dateRange.start.valueOf()).toBe(shiftDate.hours(18).toDate().valueOf());
    expect(dateRange.end.valueOf()).toBe(shiftDate.add(12, 'hours').toDate().valueOf());
  });
});
