describe('Question', () => {
  const moment = require('moment');
  const question = require('../../src/question');
  const questionData = require('../data/questions');
  const questionAnswerDataDay = require('../data/question_answers-2017-05-20-day');
  const questionAnswerDataNight = require('../data/question_answers-2017-05-20-night');
  const questionAnswerData = questionAnswerDataDay.concat(questionAnswerDataNight);
  // var mysql = require('mysql');
  // var con =mysql.createConnection({
  //   hostname:"localhost",
  //    user:"root",
  //    password:"password",
  //    database:"siteintel",
  //    port:3306,
  //    timeZone:'utc'
  // });
 
  var data = "Traffic Management";
  
  const start = new Date('2017-05-20');
  const end = new Date('2017-05-21');

  const mockDb = {
    collection: (collection) => ({
      find: (query) => ({
        sort: (sort) => ({
          toArray: (callback) => {
            let result = [];
            if (collection === 'questions') {
              result = questionData;
            } else if (
              collection === 'question_answers' &&
              moment(query.clientDate.$gte).isSame(moment(start).hours(6)) &&
              moment(query.clientDate.$lte).isSame(moment(end).hours(6))
            ) {
              result = questionAnswerData;
              if (query.questionId !== undefined && query.questionId > 0) {
                result = result.filter(({questionId}) => {
                  return questionId === query.questionId;
                });
              }
            }
            callback(null, result);
          }
        })
      })
    })
  };

  it('should not crash', () => {
    
    const model = question(mockDb);
    expect(model).not.toBeNull();
  });

  it('should fetch raw question data from the DB', () => {
    const model = question(mockDb,data);
    model.fetch((error, result) => {
      expect(result.length).toEqual(questionData.length);
    });
  });

  it('should calculate average points per question', () => {
    const model = question(mockDb);
    const alignToShift = true;
    model.performanceRequirements({start, end}, alignToShift, (result) => {
      expect(result.length).toBe(questionData.length);
      const [first, second, third] = result;
      expect(first.averagePoints).toBeCloseTo(0.886666666666667);
      expect(second.averagePoints).toBeCloseTo(0.886666666666667);
      expect(third.averagePoints).toBe(1);
    });
  });

  it('should calculate average points per day for a single question', () => {
    const model = question(mockDb);
    model.performance(1, {start, end}, (result) => {
      expect(result.length).toBe(1);
      expect(result[0]).toBeCloseTo(0.886666666666667);
    });

    model.performance(29, {start, end}, (result) => {
      expect(result.length).toBe(1);
      expect(result[0]).toBeCloseTo(0.73);
    });

    model.performance(40, {start, end}, (result) => {
      expect(result.length).toBe(1);
      expect(result[0]).toBeCloseTo(1);
    });
  });

  it('should calculate days in green and red', () => {
    const testData = [
      {
        description: 'exactly 1 week of data',
        expected: {
          green: [3, '-', '-', '-', '-', '-'],
          red: [2, '-', '-', '-', '-', '-']
        },
        performanceData: [0.35, 0.85, 0.65, 0.85, 0.35, 0.85, 0.65]
      },
      {
        description: 'multiple time periods and incomplete time period',
        expected: {
          green: [3, 4, '-', '-', '-', '-'],
          red: [2, 3, '-', '-', '-', '-']
        },
        performanceData: [0.85, 0.35, 0.35, 0.85, 0.65, 0.85, 0.35, 0.85, 0.65]
      },
      {
        description: 'null values in performance data',
        expected: {
          green: [3, 4, '-', '-', '-', '-'],
          red: [2, 3, '-', '-', '-', '-']
        },
        performanceData: [0.85, null, 0.35, null, 0.35, 0.85, 0.65, 0.85, 0.35, 0.85, 0.65]
      },
      {
        description: 'a full year of data',
        expected: {
          green: [5, 11, 25, 66, 133, 263],
          red: [0, 0, 0, 5, 12, 21]
        },
        performanceData: [
          0.81, 0.65, 0.91, 0.86, 0.78, 0.78, 0.78, 0.78, 0.78, 0.65,
          0.78, 0.86, 0.73, 0.78, 0.65, 0.19, 0.73, 0.81, 0.86, 0.78,
          0.78, 0.78, 0.67, 0.67, 0.71, 0.81, 0.78, 0.65, 0.67, 0.84,
          0.78, 0.81, 1.00, 0.73, 0.94, 0.93, 0.73, 1.00, 0.73, 0.86,
          0.81, 1.00, 0.86, 0.81, 1.00, 0.46, 1.00, 0.76, 0.89, 0.67,
          0.73, 1.00, 0.86, 0.73, 0.78, 0.65, 0.19, 0.73, 0.81, 0.86,
          0.78, 0.78, 0.78, 0.67, 0.67, 0.71, 0.46, 1.00, 0.76, 0.89,
          0.67, 0.73, 1.00, 0.86, 0.73, 0.78, 0.65, 0.19, 0.73, 0.81,
          0.86, 0.78, 0.78, 0.78, 0.78, 0.78, 0.65, 0.78, 0.86, 0.86,
          0.78, 0.78, 0.78, 0.67, 0.67, 0.71, 0.81, 0.73, 0.74, 0.98,
          0.89, 1.00, 0.84, 1.00, 0.78, 0.89, 1.00, 1.00, 1.00, 0.76,
          0.86, 0.45, 0.86, 0.78, 0.78, 0.78, 0.78, 0.78, 0.65, 0.78,
          0.86, 0.73, 0.78, 0.65, 0.19, 0.73, 0.81, 0.86, 0.78, 0.78,
          0.78, 0.78, 0.65, 0.78, 0.86, 0.86, 0.78, 0.78, 0.78, 0.86,
          0.78, 0.78, 0.78, 0.78, 0.65, 0.78, 0.86, 0.86, 0.78, 0.45,
          0.86, 0.78, 0.78, 0.78, 0.78, 0.78, 0.65, 0.78, 0.86, 0.73,
          0.78, 0.65, 0.19, 0.73, 0.81, 0.86, 0.78, 0.78, 0.78, 0.78,
          0.65, 0.78, 0.86, 0.86, 0.78, 0.78, 0.78, 0.86, 0.78, 0.78,
          0.78, 0.78, 0.65, 0.78, 0.86, 0.86, 0.78, 0.81, 0.73, 0.74,
          0.98, 0.89, 1.00, 0.84, 1.00, 0.78, 0.89, 1.00, 0.46, 1.00,
          0.76, 0.89, 0.45, 0.73, 0.74, 0.67, 0.73, 1.00, 0.78, 0.86,
          0.86, 0.78, 0.81, 0.73, 0.74, 0.98, 0.89, 0.78, 0.78, 0.78,
          0.65, 0.78, 0.86, 0.73, 0.78, 0.86, 0.86, 0.78, 0.45, 0.86,
          0.78, 0.78, 0.78, 0.65, 0.19, 0.73, 0.81, 0.86, 0.78, 0.78,
          0.78, 0.78, 0.65, 0.78, 0.86, 0.86, 0.78, 0.78, 0.78, 0.86,
          0.78, 0.78, 0.78, 0.81, 1.00, 0.73, 0.94, 0.93, 0.73, 0.84,
          1.00, 0.78, 0.89, 0.46, 0.73, 0.76, 0.89, 0.91, 1.00, 0.49,
          0.86, 0.46, 0.89, 0.78, 0.81, 0.56, 0.49, 0.65, 0.19, 0.73,
          0.81, 0.81, 0.81, 0.69, 0.71, 0.81, 0.81, 0.81, 0.81, 0.57,
          0.81, 0.81, 0.81, 0.81, 0.81, 0.94, 0.93, 0.73, 0.84, 1.00,
          0.78, 0.89, 0.46, 0.73, 0.76, 0.89, 0.91, 1.00, 0.49, 0.73,
          1.00, 0.78, 0.86, 0.86, 0.78, 0.81, 0.73, 0.74, 0.98, 0.89,
          0.78, 0.78, 0.78, 0.65, 0.78, 0.86, 0.73, 0.78, 0.86, 1.00,
          0.36, 0.73, 0.73, 0.81, 0.81, 0.76, 1.00, 0.76, 1.00, 0.56,
          0.76, 1.00, 0.81, 0.76, 0.58, 1.00, 0.81, 0.76, 1.00, 0.76,
          0.78, 0.81, 0.73, 0.74, 0.98, 0.89, 0.78, 0.78, 0.78, 0.65,
          0.78, 0.86, 0.73, 0.86, 0.81
        ]
      }
    ];

    const model = question(mockDb);
    testData.forEach(({expected, performanceData}) => {
      expect(model.colourSummary(performanceData)).toEqual(expected);
    });
  });
});
