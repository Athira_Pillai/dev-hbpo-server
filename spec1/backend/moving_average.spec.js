describe('Moving Average', () => {
  const simpleMovingAverage = require('../../src/moving_average');
  const data = [0.1, 0.2, 0.3, 0.4, 0.5];

  it('should not crash', () => {
    simpleMovingAverage(data, 3);
  });

  it('should handle invalid input', () => {
    expect(simpleMovingAverage()).toBe(undefined);
    expect(simpleMovingAverage({})).toBe(undefined);
    expect(simpleMovingAverage([])).toBe(undefined);
    expect(simpleMovingAverage([1, 2, 3, 4], 2.5)).toBe(undefined);
    expect(simpleMovingAverage([1, 2, 3], 4)).toBe(undefined);
    expect(simpleMovingAverage([1, 2, 3, 4, 5, 6])).toBe(undefined);
  });

  it('should default the period length to 7', () => {
    expect(simpleMovingAverage([1, 2, 3, 4, 5, 6, 7])).not.toBe(undefined);
  });

  it('should calculate the simple moving average of a data array', () => {
    const movingAveragePeriod = 3;
    const nullCount = movingAveragePeriod - 1;
    const result = simpleMovingAverage(data, movingAveragePeriod);
    expect(result.length).toBe(data.length - nullCount);

    const expected = [0.2, 0.3, 0.4];
    result.forEach((value, index) => {
      expect(value).toBeCloseTo(expected[index]);
    });
  });

  it('should not truncate initial null values', () => {
    const movingAveragePeriod = 3;
    const result = simpleMovingAverage(data, movingAveragePeriod, false);
  
    expect(result.length).toBe(data.length);

    const expected = [null, null, 0.2, 0.3, 0.4];
    result.forEach((value, index) => {
      expect(value).toBeCloseTo(expected[index]);
    });
  });

  it('should return a null moving average', () => {
    const data = [2, 3, 4, null, null, null];
    const result = simpleMovingAverage(data, 3);

    const expected = [3, 3.5, 4, null];
    result.forEach((value, index) => {
      expect(value).toBe(expected[index]);
    });
  });
});
