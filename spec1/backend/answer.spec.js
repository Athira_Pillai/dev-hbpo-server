
describe("Answer", () => {
 // const answer = require('../../src/answer');

  const start = new Date('2017-01-01');
  const end = new Date('2017-01-05');

  const questionAnswerData = require('../data/question_answers');
  const mockDb = {
    collection: (collection) => ({
      find: (query) => ({
        sort: (sort) => ({
          toArray: (callback) => {
            let result = [];
            if (
              collection === 'question_answers' &&
              query.clientDate.$gte === start &&
              query.clientDate.$lte === end
            ) {
              result = questionAnswerData;
            }
            callback(null, result);
            console.log(result);
          }
        })
      })
    })
  };

  it('should not crash', () => {
    const mockDb = {};
    const model = answer({start, end}, mockDb);
    expect(model).not.toBeNull();
  });

  it('should fetch raw answer data from the DB', () => {
    const model = answer({start, end},mockDb);
    model.fetch((error, result) => {
      expect(result.length).toEqual(questionAnswerData.length);
    });
  });

  it('should calculate average points per operator', () => {
    const answers1 = require('../data/question_answers-2017-05-20-day');
    const averagePoints1 = answer().averagePointsPerOperator(answers1);
    expect(averagePoints1.responseCount).toBe(3);
    expect(averagePoints1.points).toEqual([1, 0.9164999999999999, 0.7005882352941176]);

    const answers2 = require('../data/question_answers-2017-05-23-night');
    const averagePoints2 = answer().averagePointsPerOperator(answers2);
    expect(averagePoints2.responseCount).toBe(2);
    expect(averagePoints2.points).toEqual([0.983, 0.8005882352941177]);
  });

  it('should return unique point values for each possible answer ID', () => {
    const model = answer();
    let previousPointValue = 999;
    for (let i = 0; i < 4; i++) {
      const points = model.pointsPerAnswer(i);
      expect(points).toBeGreaterThan(0);
      expect(points).toBeLessThan(previousPointValue);
      previousPointValue = points;
    }
  });

  it('should calculate the performance of a list of answers', () => {
    const model = answer();
    const rawAnswerData = require('../data/question_answers-basic');
    rawAnswerData.forEach(({performance, close, answers}) => {
      if (close) {
        expect(model.performance(answers)).toBeCloseTo(performance);
      } else {
        expect(model.performance(answers)).toBe(performance);
      }
    });
  });
});
