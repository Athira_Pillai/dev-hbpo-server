import { Router } from '@angular/router';
// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-feedback',
//   templateUrl: './feedback.component.html',
//   styleUrls: ['./feedback.component.css']
// })
// export class FeedbackComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators, FormControl } from '@angular/forms';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { FeedbackApi, LoopBackConfig } from '../shared/sdk/index';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  emailAddress;
  subject;
  constructor(private formbld: FormBuilder, private feedbackApi: FeedbackApi, private http: Http, private _router: Router) { }
  feedbackForm = this.formbld.group({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(4)]),

    feedback: new FormControl('', [
      Validators.required,
      Validators.minLength(4)]),

    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.minLength(4)])
  });

  ngOnInit() {
  }

  onSubmit() {
    this.subject = `Feedback sent by ${this.emailAddress}`;
    this.feedbackApi.create({
      email: this.feedbackForm.value['email'],
      feedback: this.feedbackForm.value['feedback'],
      name: this.feedbackForm.value['name'],

    }).subscribe(res => {
      alert('Feedback sent');
      this._router.navigate(['']);
      this.emailSent(this.feedbackForm.value['email'], this.subject, this.feedbackForm.value['feedback']);
    });

  }

  emailSent(from, subject, text) {
    const url = LoopBackConfig.getPath() + '/send-email';
    const body = {
      to: 'hacksstable@gmail.com', from: from,
      subject: subject, content: text
    };
    const headers = new Headers({
      'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'
      , 'Access-Control-Allow-Headers': '*'
    });
    return this.http.post(url, body, { headers })
      .toPromise()
      .then(res => {
        // this.logger.log('Response', res)

      })
      .catch(err => {
      });
  }
}
