
import { Journey_checkpointsApi } from '../shared/sdk/services/custom/Journey_checkpoints';

import {combineLatest, from} from 'rxjs';

import {map} from 'rxjs/operators';

import { TripApi } from '../shared/sdk/services/custom/Trip';
import { Injectable } from '@angular/core';
import { SessionsApi, Question_answersApi, QuestionsApi, Question_answers,
   Trip, AssetApi, Journey_routesApi, Job_questionsApi, LocationApi, M_usersApi, M_users } from '../shared/sdk';
import * as moment from 'moment';
import { NGXLogger } from 'ngx-logger';
@Injectable({
  providedIn: 'root'
})
export class TripReportService {


  // tripDetails:TripDetails;


  fetchQuestionAnswer: Array<Object> = []; fetchQuestionId: Array<Object> = []; fetchQuestion: Array<Object> = [];
  fetchReplyAnswer: Array<Object> = [];
  fetchTripId: Array<number> = [];
  fetchTestId: Array<string> = [];
  tripStatus = '';
  resultArray: Array<Object> = [];

  constructor(private _trip: TripApi, private _session: SessionsApi, private _questionAnswers: Question_answersApi,
    private _question: QuestionsApi, private _logger: NGXLogger,
    private _asset: AssetApi, private _journey_route: Journey_routesApi, private _job_question: Job_questionsApi,
    private _journey_checkpoints: Journey_checkpointsApi, private _location: LocationApi, private _users: M_usersApi) {
  }

  // fetch trip id base on selected Date
  // getTripId(selectedDate) {
  //   return this._trip.find({
  //     where :{
  //       // timestamp:{gt:selectedDate}
  //       or : [
  //         {and:[
  //           {timestamp : {gte : selectedDate}},
  //           {timestamp : {lt:moment(selectedDate).add(1,'days')}}
  //         ]},
  //         {id:selectedDate}]
  //     },
  //     include:{
  //       relation : "journey_routes"
  //     }
  //   }).map((result)=>{
  //     return result;
  //   });
  // }
  getTripId(selectedDate, selectedDriver, selectedStatus, selectedRoute: number) {
    // console.log('getTripId function in service', moment(selectedDate).startOf('day').utc().format());
    // console.log('getTripId function in service', moment(selectedDate).endOf('day').utc().format());
    // console.log('getTripId function in service', selectedDriver);
    // console.log('getTripId function in service', selectedStatus);
    // console.log('getTripId function in service', selectedRoute);

      let status;
      let route;

      if (selectedStatus === 'All Status') {
        status = {like: '%'};
      } else {
        status = selectedStatus;
      }
      if (+selectedRoute === 9999) {
        route = [{routes_id: {gt: 0}} , {routes_id: null}];
      } else {
        route = [{routes_id: selectedRoute}];
      }
      const whereClause = {
        // timestamp:{gt:selectedDate}

          and: [
            {timestamp : {gte : moment(selectedDate).startOf('day').utc().format()}},
            {timestamp : {lte: moment(selectedDate).endOf('day').utc().format()}},
            {driverId : selectedDriver ? selectedDriver : {gt: 0}},
            {status : status},
            {or: route }
          ]
        };
    // console.log('whereClause', whereClause);

    return this._trip.find({
      where: whereClause,
      include: [
        {
          relation: 'm_users'
        },
      {
        relation: 'asset'
      },
      {
        relation: 'question_answers',
        scope: {
          include: {
            relation: 'questions'
          }

        }
      },
      {
        relation: 'journey_routes',
        scope: {
          include: {
            relation: 'job_questions',
            scope: {
              include: {
                relation: 'questions'
              }
            }
          }
        }
      }]

    }).pipe(map((_trip) => {
      // console.log(_trip);
      return _trip;
    }));
  }

  // fetch nurse & driver data

  getNurseDriverTripData(_tripObject) {
  //  console.log(_tripObject);
    return combineLatest([this._questionAnswers.find({
      where: {
            test_id: _tripObject['nurseQuestionnaireId'] || -99999
      },
      include: [
        {
        relation: 'm_users',
        scope: {
          include: {
            relation: 'job_function',
            scope: {
              include: {
                relation: 'job_questions',
                scope: {
                  include: {
                    relation: 'job_task'
                  }
                }
              }
            }
          }
        }
      },
      {
        relation: 'questions',
        scope: {
          include: {
            relation: 'question_group_header'
          }
        }
      },
  ],
    order: 'questionId'
    }),
    this._questionAnswers.find({
      where: {
            test_id: _tripObject['driverQuestionnaireId'] || -99999
      },
      include: [
        {
          relation: 'm_users',
          scope: {
            include: {
              relation: 'job_function',
              scope: {
                include: {
                  relation: 'job_questions',
                  scope: {
                    include: {
                      relation: 'job_task'
                    }
                  }
                }
              }
            }
          }
        },
      {
        relation: 'questions',
        scope: {
          include: {
            relation: 'question_group_header'
          }
        }
      }
      ]
    }),
    this._questionAnswers.find({
      where: {
            test_id: _tripObject['dispatcherQuestionnaireId'] || -99999
      },
      include: [
        {
          relation: 'm_users',
          scope: {
            include: {
              relation: 'job_function',
              scope: {
                include: {
                  relation: 'job_questions',
                  scope: {
                    include: {
                      relation: 'job_task'
                    }
                  }
                }
              }
            }
          }
        },
      {
        relation: 'questions',
        scope: {
          include: {
            relation: 'question_group_header'
          }
        }
      }
      ]
    })
    ]).toPromise().then((_nurseDriverResult) => {
      // console.log(_nurseDriverResult);
      return _nurseDriverResult;
    });
  }


  // fetch all trip id from the database
  getAllTrip(_date) {
    return this._trip.find({
      include: {
        relation: 'journey_routes'
      },
      where: {
        and: [
          {
            timestamp: { gte: _date }
          },
          {
            timestamp: { lt: moment(_date).add(1, 'days') }
          }
        ]
      }
    }).pipe(map((result) => {

      const tripsArray = [];

      result.forEach((tripElement: Trip) => {
        const trip = {
          tripId: 0,
          tripLocation: '',
          driverName: ''
        };
        if (tripElement.driverQuestionnaireId !== null) {
          this._questionAnswers.find({
            where: {
              test_id: tripElement.driverQuestionnaireId
            },
            include: {
              relation: 'm_users'
            }
          }).subscribe((questionData) => {
            trip.driverName = questionData[0]['m_users'].first_name;
            trip.tripId = tripElement.id;
            trip.tripLocation = tripElement.journey_routes.routes;
          });
          tripsArray.push(trip);
        }
      });
      return tripsArray;
    }));
  }

  // fetch checklist question for every checkpoint.
  getChecklistQuestionForCheckpoints(_typeOfInput) {
   return this._question.find({
      where: {
        type_of_input: _typeOfInput
      },
      include: [
        {
          relation: 'question_group_header'
        }
      ]
    }).toPromise();
  }
  getSessionData(tripId: number) {
    return this._session.find({
      where: {
        assetId: tripId
      },
      include: {
        relation: 'm_users'
      }
    }).pipe(map((result) => {
      // console.log('result', result);
      return result;
    }));
  }
  getDriverName() {
    return this._users.find({
      where: {
        jobFunction_id: 9
      }
    }).pipe(map((_driverName: M_users[]) => {
      return _driverName;
    }));
  }

  // getQuestionData(testId) {
  //   return this._questionAnswers.find({
  //     where: {
  //       test_id: testId
  //     },
  //     // },
  //     include: [{
  //       relation: 'questions',
  //       scope: {
  //         include: {
  //           relation: 'job_questions',
  //           scope: {
  //             include: {
  //               relation: 'route'
  //             }
  //           }
  //         }
  //       },
  //     }, {
  //       relation: 'm_users'
  //     }
  //     ]
  //   }).map((result) => {
  //     const temp = [];
  //     // console.log("Question Answer ****** ",JSON.stringify(result));
  //     result.forEach((questionElement) => {
  //       if (questionElement['questions']['job_questions'].name === 'driver journey') {
  //         temp.push(questionElement);
  //       }
  //     });
  //     return temp;
  //   });
  // }

  // get driver name when trip status is approved only
  getDriverNameForApprovedTrip(testId) {
    return this._questionAnswers.find({
      where: {
        test_id: testId
      },
      // },
      include: [{
        relation: 'questions',
        scope: {
          include: {
            relation: 'job_questions',
            scope: {
              include: {
                relation: 'route'
              }
            }
          }
        },
      }, {
        relation: 'm_users'
      }
      ]
    }).pipe(map((result) => {
      // console.log(result);
      return result;
    }));
  }

  checkTripType(groupId) {
    return this._question.find({
      include: {
        relation: 'job_questions',
        scope: {
          where: {
            question_group: groupId
          }
        }
      }
    }).pipe(map((data) => {
      return data;
    }));
  }

  getNurseName(nurseTestId) {
    return this._questionAnswers.find({
      where: {
        test_id: nurseTestId
      },
      include: {
        relation: 'm_users'
      }
    }
    ).pipe(map((data) => {
        return data;
      }));
  }

  getAssetName(assetId) {
    return this._asset.find({
      where: {
        asset_id: assetId
      }
    }).pipe(map((data) => {
      return data;
    }));
  }


  getRoute() {
    return this._job_question.find({
      where: {
        name: 'driver journey'
      },
      include: {
        relation: 'route'
      }
    }).pipe(map((data) => {
        // console.log("Route ***** " + JSON.stringify(data));
        return data;

      }));
  }

  getCheckpoints(_routeId) {
    const checkpoints = [];
    return this._journey_checkpoints.find({
      where: {
        journeyRoutesId: _routeId
      },
      include: {
        relation: 'location'
      }
    }).pipe(map((data) => {
        return data;
      }));
  }

  getStartLocation(_startLocationId) {
    return this._location.find({
      where: {
        id: _startLocationId
      }
    }).pipe(map((data) => {
        return data;
      }));
  }

  getEndLocation(_endLocationId) {
    return this._location.find({
      where: {
        id: _endLocationId
      }
    }).pipe(map((data) => {
        return data;
      }));
  }

  // fetch question answer on destination check-in click
  getQuestionAnswers(_tripId) {
    const questionGroupId = [7, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20];
     return  this._questionAnswers.find({
        where: {
          tripId: _tripId
        },
        include: [
          {
            relation : 'questions'
          }
        ]
      }).toPromise().then((_answer: Question_answers[]) => {
          const filteredArray = [];

        _answer.forEach((_answerObject: Question_answers) => {
          filteredArray.push(_answerObject.questionId);
        });
         return this._question.find({
            where: {
             and: [
               {
                 or: [
                  {
                    type_of_input : 'C1'
                  },
                  {
                    type_of_input : 'C2'
                  }
                 ]
                //  type_of_input: 'C1' || 'C2'
               },
               {
                 questionId: {inq: filteredArray}
               },
               {
                 questgroup_id: {inq: questionGroupId}
               }
             ]
            }
          }).toPromise().then((_question) => {
            this._logger.log('questions in trip service', _question);
            return _question;
          });
        // });
      });
  }

  // get question for Override checklist question
    getOverrideChecklistQuestion() {
      return this._question.find({
        where: {
          type_of_input: 'OR'
        }
      }).toPromise();
    }

    // get answer for override checklist reason
    getOverrideChecklistReasonAnswer(_tripId) {
     return this._questionAnswers.find({
        where: {
          tripId : +_tripId
        },
        include: {
          relation: 'questions'
        }
      }).toPromise().then((_result) => {
        console.log(_result);
        return _result;
      });
    }



  // use for fetching trip id from trip table
  /*getTripId() {
    // console.log('service');
    // let id = 0;
    return new Promise((resolve, reject) => {
     this._trip.find({
      where: {
        timestamp: { gt: '2018-08-28 22:11:36' }
      }
    }).subscribe((_tripDetails: Trip[]) => {
      _tripDetails.map((_tempTrip) => {
        this.fetchTripId.push(_tempTrip.id);
        this.getTestId(_tempTrip.id).then((_getTestId) => {
          // console.log(_getTestId);
          resolve(_getTestId);
        });
        // this.fetchTripId.push(_tripDetails['id']);
        // return this.fetchTripId;
      }, (error) => {
        // console.log(error);
        reject(error);
      });

    });

  }

  // use for fetching test id from session table
  getTestId(_tripId: number, _nurseId: string , id: number) {
    // console.log('_tripId');
    return new Promise((resolve, reject) => {

      this._session.find({
        where: {
          assetID: _tripId.toString()
        },
        include: {
          relation: 'm_users'
        }
      }).subscribe((_sessionDetails: Sessions[]) => {
        // console.log('_sessionDetails', _sessionDetails);
        _sessionDetails.map((_tempSession) => {
          // console.log('getTestId');
          // console.log('');
          this.tripDetails.firstName = _tempSession.m_users.first_name;
          this.tripDetails.lastName = _tempSession.m_users.last_name;
          this.fetchTestId.push(_tempSession.test_id);
          this.getQuestionAnswer(_tempSession.test_id, _tripId, _nurseId, id).then((_getQuestionAnswer) => {
            // console.log(_getQuestionAnswer);
            resolve(_getQuestionAnswer);
          }).catch((error) => {
            reject(error);
          });
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  // getting question Id and given answer from the question_answer table
  getQuestionAnswer(_testId, _tripId, _nurseId , id) {
    // console.log('getQuestionAnswer');
    const inputArray = [_testId, _nurseId];
    // console.log(inputArray);
    return new Promise((resolve, reject) => {
      this._questionAnswers.find({
        where: {
          test_id: {inq: inputArray}
        },
        include: [{
          relation: 'questions',
          scope: {
            include: {
              relation: 'job_questions'
            }
          }
        },
      {
        relation: 'm_users',
      }]
      }).subscribe((_question_answer: Question_answers[]) => {
        // console.log('_question_answer', _question_answer);
        this.fetchQuestionId = [];
        _question_answer.map((tempQuestion) => {
          if (tempQuestion.questions.job_questions.question_group === 7) {
            this.fetchQuestionId.push({ questionId: tempQuestion.questionId, answerId: tempQuestion.answerId
               , clientDate: tempQuestion.clientDate});
          }
          if (tempQuestion.questions.job_questions.question_group === 3) {
            this.tripDetails.nurse = tempQuestion.m_users.first_name + ' ' + tempQuestion.m_users.last_name;
          }
        });
        // console.log(this.fetchQuestionId);

        this.checkStatus(this.fetchQuestionId, _tripId, id).then((_chk) => {
          // console.log(_chk);
          resolve(_chk);
        });
      });
    });
  }
  checkStatus(_fetchQustAns: Array<Object>, _tripId, id) {
    console.log('_fetchQustAns', _fetchQustAns);
    // let status: string;
    // const id = 0;
    this.resultArray = [];

    const typeInput = ['C1', 'C2', 'D'];

    return new Promise((resolve, reject) => {
      this._question.find({
        where: {
          type_of_input: { inq: typeInput }
        }
      }).subscribe((_question: Questions[]) => {
        // console.log('_question', _question);
        _question.map((tempQuestion) => {
          // console.log('tempQuestion', tempQuestion);
          this.fetchQuestion.push({
            questionId: tempQuestion.questionId, answerId: tempQuestion.question,
            typeOfInput: tempQuestion.type_of_input
          });
        });
        this.tripDetails = {
          firstName: '',
          lastName: '',
          vehicle: '',
          nurse: '',
          route: '',
          start: '',
          checkA: 'N/A',
          checkB: 'N/A',
          checkC: 'N/A',
          end: ''
        };
        for (let i = 0; i < _fetchQustAns.length; i++) {
          for (let j = 0; j < this.fetchQuestion.length; j++) {
            if (_fetchQustAns[i]['questionId'] === this.fetchQuestion[j]['questionId']) {
              if ( _fetchQustAns[i]['answerId'] === 0) {

                if (this.fetchQuestion[j]['typeOfInput'] === 'C1') {
                  this.tripDetails.start = _fetchQustAns[i]['clientDate'];
                  this.tripDetails.checkA = _fetchQustAns[i]['clientDate'];
                  this.resultArray[id] = { 'tripId': _tripId, status: 'In progress', 'trip': this.tripDetails};

                } else if (this.fetchQuestion[j]['typeOfInput'] === 'C2') {
                  this.tripDetails.checkB = _fetchQustAns[i]['clientDate'];
                  this.resultArray[id] = { 'tripId': _tripId, status: 'In progress', 'trip': this.tripDetails };

                } else if (this.fetchQuestion[j]['typeOfInput'] === 'D') {
                  this.tripDetails.end = _fetchQustAns[i]['clientDate'];
                  this.resultArray[id] = { 'tripId': _tripId, status: 'End journey', 'trip': this.tripDetails };

                }
              }

            }

    }
  }
  });
});
  }*/

}
