import { AssignTripComponent } from './AssignTrip/AssignTrip.component';
import { Journey_checkpointsApi } from './shared/sdk/services/custom/Journey_checkpoints';
import { Question_group_headerApi } from './shared/sdk/services/custom/Question_group_header';
import { Question_typeApi } from './shared/sdk/services/custom/Question_type';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';


import { environment } from './../environments/environment.prod';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule, } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminQuestionsComponent } from './admin-questions/admin-questions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportComponent } from './report/report.component';
import { MinetellComponent } from './mine-intel/mine-intel.component';

import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { QuestionServiceService } from './services/question-service.service';
import { QuestionSequenceManagerService } from './services/question-sequence-manager.service';
import { TimeSpentService } from './services/time-spent.service';
import { NotificationService } from './services/notification.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { QuestionViewComponent } from './question-view/question-view.component';
import {  SDKModels, LoopBackAuth, InternalStorage,
  ErrorHandler, QuestionsApi, Question_answersApi, Performance_requirementsApi, RisksApi, Critical_controlApi,
  Job_functionApi, Cc_nameApi, SessionsApi, UploadfileApi, Job_taskApi, Job_questionsApi, AssetApi, TripApi, LocationApi, FeedbackApi, EmailApi, Cc_averageApi, Risk_averageApi, Response_quality_indexApi} from './shared/sdk/index';
import { SocketConnection } from './shared/sdk/sockets/socket.connections';
import { SocketDriver } from './shared/sdk/sockets/socket.driver';

import { M_usersApi } from './shared/sdk/services/custom/M_users';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from './services/data.service';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {BreadcrumbsModule} from 'ng6-breadcrumbs';
import { JobandtaskpageComponent } from './jobandtaskpage/jobandtaskpage.component';
import { DashboardCriticalControlComponent } from './dashboard-critical-control/dashboard-critical-control.component';
import { DashboardPerformanceRequirementsComponent} from './dashboard-performance-requirements/dashboard-performance-requirements.component';
import { ChartsModule, BaseChartDirective } from 'ng2-charts';
import { RiskManagementComponent } from './risk-management/risk-management.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { InspectionReportComponent } from './inspection-report/inspection-report.component';
import { SignupComponent } from './signup/signup.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { InspectionListingComponent } from './inspection-listing/inspection-listing.component';
import { LoopBackConfig } from './shared/sdk/lb.config';
import {PapaParseModule} from 'ngx-papaparse';
import { IncidentComponent } from './incident/incident.component';
import { AgmCoreModule } from '@agm/core';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { MessagingService } from './services/messaging.service';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AdminAssetComponent } from './admin-asset/admin-asset.component';
import { ChecklistComponent } from './checklist/checklist.component';
import { ChecklistpagebypageComponent } from './checklistpagebypage/checklistpagebypage.component';
import { ChecklistreportComponent } from './checklistreport/checklistreport.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DispatchReportComponent } from './dispatch-report/dispatch-report.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Journey_routesApi } from './shared/sdk/services/custom/Journey_routes';
import { TripReportComponent } from './trip-report/trip-report.component';
import { TripReportService } from '../app/services/trip-report.service';
import { TripMapComponent } from './trip-map/trip-map.component';
import { TripDetailReportComponent } from './trip-detail-report/trip-detail-report.component';
import { ComponentReportsComponent } from './component-reports/component-reports.component';
import {MatDialogModule, MatButtonModule} from '@angular/material';
import { RoutesComponentComponent } from './routes-component/routes-component.component';
import { DialogComponent } from './dialog/dialog.component';

import {MatCardModule} from '@angular/material/card';
import { CloseJourneyComponent } from './close-journey/close-journey.component';

import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DateFormatPipe } from '../app/pipe/date.pipe';
import { DateTimeFormatPipe } from '../app/pipe/date-time-format.pipe';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { FeedbackComponent } from './feedback/feedback.component';
import { RefreshComponent } from './refresh/refresh.component';
@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    AppComponent,
    AdminQuestionsComponent,
    DashboardComponent,
    ReportComponent,
    MinetellComponent,
    LoginComponent,
    QuestionViewComponent,
    JobandtaskpageComponent,
    DashboardCriticalControlComponent,
    DashboardPerformanceRequirementsComponent,
    RiskManagementComponent,
    InspectionReportComponent,
    SignupComponent,
    BreadcrumbsComponent,
    InspectionListingComponent,
    IncidentComponent,
    AdminAssetComponent,
    ChecklistComponent,
    ChecklistpagebypageComponent,
    ChecklistreportComponent,
    DispatchReportComponent,
    TripReportComponent,
    TripMapComponent,
    TripDetailReportComponent,
    ComponentReportsComponent,
    RoutesComponentComponent,
    DialogComponent,
    CloseJourneyComponent,
    AssignTripComponent,

    ForgotPasswordComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    DateFormatPipe,
    DateTimeFormatPipe,

    FeedbackComponent,

    RefreshComponent

  ],
  entryComponents: [
    DialogComponent,
    CloseJourneyComponent,
    ChecklistreportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AgmCoreModule.forRoot(environment.googleMapApi),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    HttpModule,
    NgbModule.forRoot(),
    FormsModule,
    BreadcrumbsModule,
    ChartsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    LoggerModule.forRoot({ serverLoggingUrl: LoopBackConfig.getPath() + '/api/logs',
    level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR}),
    PapaParseModule,
    SimpleNotificationsModule.forRoot(),
    BrowserAnimationsModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    ServiceWorkerModule.register('../firebase-messaging-sw.js', { enabled: environment.production }),
    AngularFirestoreModule.enablePersistence(),
    MatDialogModule,
    MatCardModule,
    MatButtonModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSnackBarModule
  ],
  providers: [Critical_controlApi, RisksApi, Performance_requirementsApi,
    QuestionServiceService, QuestionSequenceManagerService, TimeSpentService,
    NotificationService, SocketConnection, SocketDriver, SDKModels, LoopBackAuth,
    InternalStorage, ErrorHandler, M_usersApi, QuestionsApi,
    DataService, Question_answersApi, Job_functionApi, Job_taskApi, Cc_nameApi, SessionsApi,
     Question_typeApi, UploadfileApi, MessagingService, Job_questionsApi, AssetApi,
     Question_group_headerApi, Journey_routesApi, TripApi, TripReportService, Journey_checkpointsApi, LocationApi,
     {provide: OWL_DATE_TIME_LOCALE, useValue: 'en-gb'}, 
     FeedbackApi, Cc_averageApi, Risk_averageApi, Response_quality_indexApi],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

