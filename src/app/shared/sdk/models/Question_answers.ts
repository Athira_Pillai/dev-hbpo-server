/* tslint:disable */
import {
  Critical_control,
  M_users,
  Questions,
  Sessions,
  Trip
} from '../index';

declare var Object: any;
export interface Question_answersInterface {
  "answerId"?: number;
  "clientDate"?: Date;
  "date"?: Date;
  "dontKnow"?: number;
  "secondsTracked"?: number;
  "skipped"?: number;
  "comments"?: string;
  "id"?: number;
  "cc_id"?: number;
  "userId"?: number;
  "questionId"?: string;
  "test_id"?: string;
  "tripId"?: number;
  critical_control?: Critical_control;
  m_users?: M_users;
  questions?: Questions;
  sessions?: Sessions;
  trip?: Trip;
}

export class Question_answers implements Question_answersInterface {
  "answerId": number;
  "clientDate": Date;
  "date": Date;
  "dontKnow": number;
  "secondsTracked": number;
  "skipped": number;
  "comments": string;
  "id": number;
  "cc_id": number;
  "userId": number;
  "questionId": string;
  "test_id": string;
  "tripId": number;
  critical_control: Critical_control;
  m_users: M_users;
  questions: Questions;
  sessions: Sessions;
  trip: Trip;
  constructor(data?: Question_answersInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Question_answers`.
   */
  public static getModelName() {
    return "Question_answers";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Question_answers for dynamic purposes.
  **/
  public static factory(data: Question_answersInterface): Question_answers{
    return new Question_answers(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Question_answers',
      plural: 'Question_answers',
      path: 'Question_answers',
      idName: 'id',
      properties: {
        "answerId": {
          name: 'answerId',
          type: 'number'
        },
        "clientDate": {
          name: 'clientDate',
          type: 'Date'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "dontKnow": {
          name: 'dontKnow',
          type: 'number'
        },
        "secondsTracked": {
          name: 'secondsTracked',
          type: 'number'
        },
        "skipped": {
          name: 'skipped',
          type: 'number'
        },
        "comments": {
          name: 'comments',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "cc_id": {
          name: 'cc_id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "questionId": {
          name: 'questionId',
          type: 'string'
        },
        "test_id": {
          name: 'test_id',
          type: 'string'
        },
        "tripId": {
          name: 'tripId',
          type: 'number'
        },
      },
      relations: {
        critical_control: {
          name: 'critical_control',
          type: 'Critical_control',
          model: 'Critical_control',
          relationType: 'belongsTo',
                  keyFrom: 'cc_id',
          keyTo: 'id'
        },
        m_users: {
          name: 'm_users',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'userId',
          keyTo: 'id'
        },
        questions: {
          name: 'questions',
          type: 'Questions',
          model: 'Questions',
          relationType: 'belongsTo',
                  keyFrom: 'questionId',
          keyTo: 'questionId'
        },
        sessions: {
          name: 'sessions',
          type: 'Sessions',
          model: 'Sessions',
          relationType: 'belongsTo',
                  keyFrom: 'test_id',
          keyTo: 'test_id'
        },
        trip: {
          name: 'trip',
          type: 'Trip',
          model: 'Trip',
          relationType: 'belongsTo',
                  keyFrom: 'tripId',
          keyTo: 'id'
        },
      }
    }
  }
}
