/* tslint:disable */
import {
  Job_function,
  Userrole
} from '../index';

declare var Object: any;
export interface M_usersInterface {
  "jobFunction"?: string;
  "jobTask"?: string;
  "phone"?: string;
  "first_name": string;
  "last_name"?: string;
  "licence_no"?: string;
  "torex_guid"?: string;
  "email"?: string;
  "language"?: string;
  "isPasswordChanged"?: boolean;
  "realm"?: string;
  "username"?: string;
  "emailVerified"?: boolean;
  "id"?: number;
  "createdAt": Date;
  "updatedAt": Date;
  "jobFunction_id"?: number;
  "password"?: string;
  accessTokens?: any[];
  job_function?: Job_function;
  userrole?: Userrole[];
}

export class M_users implements M_usersInterface {
  "jobFunction": string;
  "jobTask": string;
  "phone": string;
  "first_name": string;
  "last_name": string;
  "licence_no": string;
  "torex_guid": string;
  "email": string;
  "language": string;
  "isPasswordChanged": boolean;
  "realm": string;
  "username": string;
  "emailVerified": boolean;
  "id": number;
  "createdAt": Date;
  "updatedAt": Date;
  "jobFunction_id": number;
  "password": string;
  accessTokens: any[];
  job_function: Job_function;
  userrole: Userrole[];
  constructor(data?: M_usersInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `M_users`.
   */
  public static getModelName() {
    return "M_users";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of M_users for dynamic purposes.
  **/
  public static factory(data: M_usersInterface): M_users{
    return new M_users(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'M_users',
      plural: 'm_users',
      path: 'm_users',
      idName: 'id',
      properties: {
        "jobFunction": {
          name: 'jobFunction',
          type: 'string'
        },
        "jobTask": {
          name: 'jobTask',
          type: 'string'
        },
        "phone": {
          name: 'phone',
          type: 'string'
        },
        "first_name": {
          name: 'first_name',
          type: 'string'
        },
        "last_name": {
          name: 'last_name',
          type: 'string'
        },
        "licence_no": {
          name: 'licence_no',
          type: 'string'
        },
        "torex_guid": {
          name: 'torex_guid',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "language": {
          name: 'language',
          type: 'string'
        },
        "isPasswordChanged": {
          name: 'isPasswordChanged',
          type: 'boolean'
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
        "jobFunction_id": {
          name: 'jobFunction_id',
          type: 'number'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        job_function: {
          name: 'job_function',
          type: 'Job_function',
          model: 'Job_function',
          relationType: 'belongsTo',
                  keyFrom: 'jobFunction_id',
          keyTo: 'id'
        },
        userrole: {
          name: 'userrole',
          type: 'Userrole[]',
          model: 'Userrole',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
      }
    }
  }
}
