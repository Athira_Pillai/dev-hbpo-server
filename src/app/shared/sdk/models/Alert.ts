/* tslint:disable */

declare var Object: any;
export interface AlertInterface {
  "timestamp"?: Date;
  "rm_name"?: string;
  "riskAverage"?: number;
  "status"?: string;
  "id"?: number;
}

export class Alert implements AlertInterface {
  "timestamp": Date;
  "rm_name": string;
  "riskAverage": number;
  "status": string;
  "id": number;
  constructor(data?: AlertInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Alert`.
   */
  public static getModelName() {
    return "Alert";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Alert for dynamic purposes.
  **/
  public static factory(data: AlertInterface): Alert{
    return new Alert(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Alert',
      plural: 'Alerts',
      path: 'Alerts',
      idName: 'id',
      properties: {
        "timestamp": {
          name: 'timestamp',
          type: 'Date'
        },
        "rm_name": {
          name: 'rm_name',
          type: 'string'
        },
        "riskAverage": {
          name: 'riskAverage',
          type: 'number'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
