/* tslint:disable */
import {
  Performance_requirements
} from '../index';

declare var Object: any;
export interface Perf_averageInterface {
  "average"?: string;
  "date"?: Date;
  "moving_average"?: string;
  "id"?: number;
  "perf_id"?: number;
  performance_requirements?: Performance_requirements;
}

export class Perf_average implements Perf_averageInterface {
  "average": string;
  "date": Date;
  "moving_average": string;
  "id": number;
  "perf_id": number;
  performance_requirements: Performance_requirements;
  constructor(data?: Perf_averageInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Perf_average`.
   */
  public static getModelName() {
    return "Perf_average";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Perf_average for dynamic purposes.
  **/
  public static factory(data: Perf_averageInterface): Perf_average{
    return new Perf_average(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Perf_average',
      plural: 'Perf_averages',
      path: 'Perf_averages',
      idName: 'id',
      properties: {
        "average": {
          name: 'average',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "moving_average": {
          name: 'moving_average',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "perf_id": {
          name: 'perf_id',
          type: 'number'
        },
      },
      relations: {
        performance_requirements: {
          name: 'performance_requirements',
          type: 'Performance_requirements',
          model: 'Performance_requirements',
          relationType: 'belongsTo',
                  keyFrom: 'perf_id',
          keyTo: 'id'
        },
      }
    }
  }
}
