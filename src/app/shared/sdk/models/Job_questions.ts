/* tslint:disable */
import {
  Job_function,
  Job_task,
  Questions,
  Question_group_header,
  Journey_routes,
  Email,
  Installation
} from '../index';

declare var Object: any;
export interface Job_questionsInterface {
  "question_group"?: number;
  "type"?: string;
  "checklist_type"?: string;
  "name"?: string;
  "is_combine"?: boolean;
  "is_button_color"?: boolean;
  "is_risk_screen_enabled"?: boolean;
  "jobFunctionId"?: number;
  "jobTaskId"?: number;
  "question_group_header_id"?: number;
  "routeId"?: number;
  "assetTypeId"?: number;
  job_function?: Job_function;
  job_task?: Job_task;
  questions?: Questions[];
  question_group_header?: Question_group_header;
  route?: Journey_routes;
  email?: Email[];
  installation?: Installation[];
}

export class Job_questions implements Job_questionsInterface {
  "question_group": number;
  "type": string;
  "checklist_type": string;
  "name": string;
  "is_combine": boolean;
  "is_button_color": boolean;
  "is_risk_screen_enabled": boolean;
  "jobFunctionId": number;
  "jobTaskId": number;
  "question_group_header_id": number;
  "routeId": number;
  "assetTypeId": number;
  job_function: Job_function;
  job_task: Job_task;
  questions: Questions[];
  question_group_header: Question_group_header;
  route: Journey_routes;
  email: Email[];
  installation: Installation[];
  constructor(data?: Job_questionsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Job_questions`.
   */
  public static getModelName() {
    return "Job_questions";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Job_questions for dynamic purposes.
  **/
  public static factory(data: Job_questionsInterface): Job_questions{
    return new Job_questions(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Job_questions',
      plural: 'job_questions',
      path: 'job_questions',
      idName: 'question_group',
      properties: {
        "question_group": {
          name: 'question_group',
          type: 'number'
        },
        "type": {
          name: 'type',
          type: 'string'
        },
        "checklist_type": {
          name: 'checklist_type',
          type: 'string',
          default: 'checklist'
        },
        "name": {
          name: 'name',
          type: 'string',
          default: 'nurse'
        },
        "is_combine": {
          name: 'is_combine',
          type: 'boolean',
          default: false
        },
        "is_button_color": {
          name: 'is_button_color',
          type: 'boolean',
          default: true
        },
        "is_risk_screen_enabled": {
          name: 'is_risk_screen_enabled',
          type: 'boolean',
          default: false
        },
        "jobFunctionId": {
          name: 'jobFunctionId',
          type: 'number'
        },
        "jobTaskId": {
          name: 'jobTaskId',
          type: 'number'
        },
        "question_group_header_id": {
          name: 'question_group_header_id',
          type: 'number'
        },
        "routeId": {
          name: 'routeId',
          type: 'number'
        },
        "assetTypeId": {
          name: 'assetTypeId',
          type: 'number'
        },
      },
      relations: {
        job_function: {
          name: 'job_function',
          type: 'Job_function',
          model: 'Job_function',
          relationType: 'belongsTo',
                  keyFrom: 'jobFunctionId',
          keyTo: 'id'
        },
        job_task: {
          name: 'job_task',
          type: 'Job_task',
          model: 'Job_task',
          relationType: 'belongsTo',
                  keyFrom: 'jobTaskId',
          keyTo: 'id'
        },
        questions: {
          name: 'questions',
          type: 'Questions[]',
          model: 'Questions',
          relationType: 'hasMany',
                  keyFrom: 'question_group',
          keyTo: 'questgroup_id'
        },
        question_group_header: {
          name: 'question_group_header',
          type: 'Question_group_header',
          model: 'Question_group_header',
          relationType: 'belongsTo',
                  keyFrom: 'question_group_header_id',
          keyTo: 'id'
        },
        route: {
          name: 'route',
          type: 'Journey_routes',
          model: 'Journey_routes',
          relationType: 'belongsTo',
                  keyFrom: 'routeId',
          keyTo: 'id'
        },
        email: {
          name: 'email',
          type: 'Email[]',
          model: 'Email',
          relationType: 'hasMany',
                  keyFrom: 'question_group',
          keyTo: 'questgroup_id'
        },
        installation: {
          name: 'installation',
          type: 'Installation[]',
          model: 'Installation',
          relationType: 'hasMany',
                  keyFrom: 'question_group',
          keyTo: 'questgroup_id'
        },
      }
    }
  }
}
