/* tslint:disable */

declare var Object: any;
export interface Cc_averageInterface {
  "average"?: string;
  "rm_id"?: number;
  "rm_name"?: string;
  "date"?: Date;
  "moving_average"?: string;
  "id"?: number;
  "cc_id"?: number;
}

export class Cc_average implements Cc_averageInterface {
  "average": string;
  "rm_id": number;
  "rm_name": string;
  "date": Date;
  "moving_average": string;
  "id": number;
  "cc_id": number;
  constructor(data?: Cc_averageInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cc_average`.
   */
  public static getModelName() {
    return "Cc_average";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cc_average for dynamic purposes.
  **/
  public static factory(data: Cc_averageInterface): Cc_average{
    return new Cc_average(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cc_average',
      plural: 'Cc_averages',
      path: 'Cc_averages',
      idName: 'id',
      properties: {
        "average": {
          name: 'average',
          type: 'string'
        },
        "rm_id": {
          name: 'rm_id',
          type: 'number'
        },
        "rm_name": {
          name: 'rm_name',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "moving_average": {
          name: 'moving_average',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "cc_id": {
          name: 'cc_id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
