/* tslint:disable */
import {
  Critical_control,
  Risk_average
} from '../index';

declare var Object: any;
export interface RisksInterface {
  "rm_name": string;
  "rm_id": number;
  "sortId"?: number;
  critical_control?: Critical_control[];
  risk_average?: Risk_average[];
}

export class Risks implements RisksInterface {
  "rm_name": string;
  "rm_id": number;
  "sortId": number;
  critical_control: Critical_control[];
  risk_average: Risk_average[];
  constructor(data?: RisksInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Risks`.
   */
  public static getModelName() {
    return "Risks";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Risks for dynamic purposes.
  **/
  public static factory(data: RisksInterface): Risks{
    return new Risks(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Risks',
      plural: 'Risks',
      path: 'Risks',
      idName: 'rm_id',
      properties: {
        "rm_name": {
          name: 'rm_name',
          type: 'string'
        },
        "rm_id": {
          name: 'rm_id',
          type: 'number'
        },
        "sortId": {
          name: 'sortId',
          type: 'number'
        },
      },
      relations: {
        critical_control: {
          name: 'critical_control',
          type: 'Critical_control[]',
          model: 'Critical_control',
          relationType: 'hasMany',
                  keyFrom: 'rm_id',
          keyTo: 'rm_id'
        },
        risk_average: {
          name: 'risk_average',
          type: 'Risk_average[]',
          model: 'Risk_average',
          relationType: 'hasMany',
                  keyFrom: 'rm_id',
          keyTo: 'rm_id'
        },
      }
    }
  }
}
