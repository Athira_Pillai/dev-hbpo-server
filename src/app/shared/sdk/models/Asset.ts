/* tslint:disable */
import {
  Asset_type
} from '../index';

declare var Object: any;
export interface AssetInterface {
  "asset_id": string;
  "plate_number": string;
  "asset_model"?: string;
  "color"?: string;
  "model"?: number;
  "property"?: string;
  "vehicle_type"?: string;
  "assetTypeId"?: number;
  assetType?: Asset_type;
}

export class Asset implements AssetInterface {
  "asset_id": string;
  "plate_number": string;
  "asset_model": string;
  "color": string;
  "model": number;
  "property": string;
  "vehicle_type": string;
  "assetTypeId": number;
  assetType: Asset_type;
  constructor(data?: AssetInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Asset`.
   */
  public static getModelName() {
    return "Asset";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Asset for dynamic purposes.
  **/
  public static factory(data: AssetInterface): Asset{
    return new Asset(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Asset',
      plural: 'Assets',
      path: 'Assets',
      idName: 'asset_id',
      properties: {
        "asset_id": {
          name: 'asset_id',
          type: 'string'
        },
        "plate_number": {
          name: 'plate_number',
          type: 'string'
        },
        "asset_model": {
          name: 'asset_model',
          type: 'string'
        },
        "color": {
          name: 'color',
          type: 'string'
        },
        "model": {
          name: 'model',
          type: 'number'
        },
        "property": {
          name: 'property',
          type: 'string'
        },
        "vehicle_type": {
          name: 'vehicle_type',
          type: 'string'
        },
        "assetTypeId": {
          name: 'assetTypeId',
          type: 'number'
        },
      },
      relations: {
        assetType: {
          name: 'assetType',
          type: 'Asset_type',
          model: 'Asset_type',
          relationType: 'belongsTo',
                  keyFrom: 'assetTypeId',
          keyTo: 'id'
        },
      }
    }
  }
}
