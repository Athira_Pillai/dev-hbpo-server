/* tslint:disable */
import {
  Job_questions
} from '../index';

declare var Object: any;
export interface EmailInterface {
  "email_id": string;
  "sort_id"?: number;
  "id"?: number;
  "questgroup_id"?: number;
  "report_id"?: number;
  job_questions?: Job_questions;
}

export class Email implements EmailInterface {
  "email_id": string;
  "sort_id": number;
  "id": number;
  "questgroup_id": number;
  "report_id": number;
  job_questions: Job_questions;
  constructor(data?: EmailInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Email`.
   */
  public static getModelName() {
    return "Email";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Email for dynamic purposes.
  **/
  public static factory(data: EmailInterface): Email{
    return new Email(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Email',
      plural: 'Emails',
      path: 'Emails',
      idName: 'id',
      properties: {
        "email_id": {
          name: 'email_id',
          type: 'string'
        },
        "sort_id": {
          name: 'sort_id',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "questgroup_id": {
          name: 'questgroup_id',
          type: 'number'
        },
        "report_id": {
          name: 'report_id',
          type: 'number'
        },
      },
      relations: {
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions',
          model: 'Job_questions',
          relationType: 'belongsTo',
                  keyFrom: 'questgroup_id',
          keyTo: 'question_group'
        },
      }
    }
  }
}
