/* tslint:disable */
import {
  Job_questions
} from '../index';

declare var Object: any;
export interface InstallationInterface {
  "appId": string;
  "appVersion"?: string;
  "badge"?: number;
  "created"?: Date;
  "deviceToken": string;
  "deviceType": string;
  "modified"?: Date;
  "status"?: string;
  "subscriptions"?: Array<any>;
  "timeZone"?: string;
  "userId"?: string;
  "id"?: number;
  "questgroup_id"?: number;
  job_questions?: Job_questions;
}

export class Installation implements InstallationInterface {
  "appId": string;
  "appVersion": string;
  "badge": number;
  "created": Date;
  "deviceToken": string;
  "deviceType": string;
  "modified": Date;
  "status": string;
  "subscriptions": Array<any>;
  "timeZone": string;
  "userId": string;
  "id": number;
  "questgroup_id": number;
  job_questions: Job_questions;
  constructor(data?: InstallationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Installation`.
   */
  public static getModelName() {
    return "Installation";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Installation for dynamic purposes.
  **/
  public static factory(data: InstallationInterface): Installation{
    return new Installation(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Installation',
      plural: 'Installations',
      path: 'Installations',
      idName: 'id',
      properties: {
        "appId": {
          name: 'appId',
          type: 'string'
        },
        "appVersion": {
          name: 'appVersion',
          type: 'string'
        },
        "badge": {
          name: 'badge',
          type: 'number'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "deviceToken": {
          name: 'deviceToken',
          type: 'string'
        },
        "deviceType": {
          name: 'deviceType',
          type: 'string'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "subscriptions": {
          name: 'subscriptions',
          type: 'Array&lt;any&gt;'
        },
        "timeZone": {
          name: 'timeZone',
          type: 'string'
        },
        "userId": {
          name: 'userId',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "questgroup_id": {
          name: 'questgroup_id',
          type: 'number'
        },
      },
      relations: {
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions',
          model: 'Job_questions',
          relationType: 'belongsTo',
                  keyFrom: 'questgroup_id',
          keyTo: 'question_group'
        },
      }
    }
  }
}
