/* tslint:disable */

declare var Object: any;
export interface UploadfileInterface {
  "file_type"?: string;
  "file_format"?: string;
  "id"?: number;
}

export class Uploadfile implements UploadfileInterface {
  "file_type": string;
  "file_format": string;
  "id": number;
  constructor(data?: UploadfileInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Uploadfile`.
   */
  public static getModelName() {
    return "Uploadfile";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Uploadfile for dynamic purposes.
  **/
  public static factory(data: UploadfileInterface): Uploadfile{
    return new Uploadfile(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Uploadfile',
      plural: 'uploadfile',
      path: 'uploadfile',
      idName: 'id',
      properties: {
        "file_type": {
          name: 'file_type',
          type: 'string',
          default: 'Risk'
        },
        "file_format": {
          name: 'file_format',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
