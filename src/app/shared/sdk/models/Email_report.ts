/* tslint:disable */
import {
  M_users,
  Location,
  Sessions
} from '../index';

declare var Object: any;
export interface Email_reportInterface {
  "email": string;
  "date"?: Date;
  "comments"?: string;
  "id"?: number;
  "screenerId"?: number;
  "employeeId"?: number;
  "locationId"?: number;
  "testId"?: string;
  screener?: M_users;
  employee?: M_users;
  location?: Location;
  sessions?: Sessions;
}

export class Email_report implements Email_reportInterface {
  "email": string;
  "date": Date;
  "comments": string;
  "id": number;
  "screenerId": number;
  "employeeId": number;
  "locationId": number;
  "testId": string;
  screener: M_users;
  employee: M_users;
  location: Location;
  sessions: Sessions;
  constructor(data?: Email_reportInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Email_report`.
   */
  public static getModelName() {
    return "Email_report";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Email_report for dynamic purposes.
  **/
  public static factory(data: Email_reportInterface): Email_report{
    return new Email_report(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Email_report',
      plural: 'Email_reports',
      path: 'Email_reports',
      idName: 'id',
      properties: {
        "email": {
          name: 'email',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "comments": {
          name: 'comments',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "screenerId": {
          name: 'screenerId',
          type: 'number'
        },
        "employeeId": {
          name: 'employeeId',
          type: 'number'
        },
        "locationId": {
          name: 'locationId',
          type: 'number'
        },
        "testId": {
          name: 'testId',
          type: 'string'
        },
      },
      relations: {
        screener: {
          name: 'screener',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'screenerId',
          keyTo: 'id'
        },
        employee: {
          name: 'employee',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'employeeId',
          keyTo: 'id'
        },
        location: {
          name: 'location',
          type: 'Location',
          model: 'Location',
          relationType: 'belongsTo',
                  keyFrom: 'locationId',
          keyTo: 'id'
        },
        sessions: {
          name: 'sessions',
          type: 'Sessions',
          model: 'Sessions',
          relationType: 'belongsTo',
                  keyFrom: 'testId',
          keyTo: 'test_id'
        },
      }
    }
  }
}
