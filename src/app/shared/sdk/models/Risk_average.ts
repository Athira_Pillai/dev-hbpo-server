/* tslint:disable */

declare var Object: any;
export interface Risk_averageInterface {
  "average"?: string;
  "date"?: Date;
  "id"?: number;
  "rm_id"?: number;
}

export class Risk_average implements Risk_averageInterface {
  "average": string;
  "date": Date;
  "id": number;
  "rm_id": number;
  constructor(data?: Risk_averageInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Risk_average`.
   */
  public static getModelName() {
    return "Risk_average";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Risk_average for dynamic purposes.
  **/
  public static factory(data: Risk_averageInterface): Risk_average{
    return new Risk_average(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Risk_average',
      plural: 'Risk_averages',
      path: 'Risk_averages',
      idName: 'id',
      properties: {
        "average": {
          name: 'average',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "rm_id": {
          name: 'rm_id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
