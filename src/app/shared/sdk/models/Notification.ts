/* tslint:disable */
import {
  M_users,
  Job_questions
} from '../index';

declare var Object: any;
export interface NotificationInterface {
  "id"?: number;
  "userId"?: number;
  "jobQuestionId"?: number;
  m_users?: M_users;
  jobQuestions?: Job_questions;
}

export class Notification implements NotificationInterface {
  "id": number;
  "userId": number;
  "jobQuestionId": number;
  m_users: M_users;
  jobQuestions: Job_questions;
  constructor(data?: NotificationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Notification`.
   */
  public static getModelName() {
    return "Notification";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Notification for dynamic purposes.
  **/
  public static factory(data: NotificationInterface): Notification{
    return new Notification(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Notification',
      plural: 'Notifications',
      path: 'Notifications',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "jobQuestionId": {
          name: 'jobQuestionId',
          type: 'number'
        },
      },
      relations: {
        m_users: {
          name: 'm_users',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'userId',
          keyTo: 'id'
        },
        jobQuestions: {
          name: 'jobQuestions',
          type: 'Job_questions',
          model: 'Job_questions',
          relationType: 'belongsTo',
                  keyFrom: 'jobQuestionId',
          keyTo: 'question_group'
        },
      }
    }
  }
}
