/* tslint:disable */
import {
  Location
} from '../index';

declare var Object: any;
export interface Journey_checkpointsInterface {
  "id"?: number;
  "locationId"?: number;
  "journeyRoutesId"?: number;
  location?: Location;
}

export class Journey_checkpoints implements Journey_checkpointsInterface {
  "id": number;
  "locationId": number;
  "journeyRoutesId": number;
  location: Location;
  constructor(data?: Journey_checkpointsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Journey_checkpoints`.
   */
  public static getModelName() {
    return "Journey_checkpoints";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Journey_checkpoints for dynamic purposes.
  **/
  public static factory(data: Journey_checkpointsInterface): Journey_checkpoints{
    return new Journey_checkpoints(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Journey_checkpoints',
      plural: 'Journey_checkpoints',
      path: 'Journey_checkpoints',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "locationId": {
          name: 'locationId',
          type: 'number'
        },
        "journeyRoutesId": {
          name: 'journeyRoutesId',
          type: 'number'
        },
      },
      relations: {
        location: {
          name: 'location',
          type: 'Location',
          model: 'Location',
          relationType: 'belongsTo',
                  keyFrom: 'locationId',
          keyTo: 'id'
        },
      }
    }
  }
}
