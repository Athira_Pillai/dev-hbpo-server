/* tslint:disable */
import {
  Asset,
  M_users,
  Question_answers,
  Sessions,
  Journey_routes,
  Trip_checkpoint
} from '../index';

declare var Object: any;
export interface TripInterface {
  "driverSelfFit"?: string;
  "driverFit"?: string;
  "vehicleFit"?: string;
  "routeFit"?: string;
  "timestamp"?: Date;
  "status"?: string;
  "id"?: number;
  "assetId"?: string;
  "driverId"?: number;
  "nurseId"?: number;
  "driverQuestionnaireId"?: string;
  "nurseQuestionnaireId"?: string;
  "driverSelfQuestionnaireId"?: string;
  "dispatcherQuestionnaireId"?: string;
  "routes_id"?: number;
  asset?: Asset;
  m_users?: M_users;
  users?: M_users;
  question_answers?: Question_answers[];
  driverQuestionnaire?: Sessions;
  nurseQuestionnaire?: Sessions;
  driverSelfQuestionnaire?: Sessions;
  dispatcherQuestionnaire?: Sessions;
  journey_routes?: Journey_routes;
  trip_checkpoint?: Trip_checkpoint[];
}

export class Trip implements TripInterface {
  "driverSelfFit": string;
  "driverFit": string;
  "vehicleFit": string;
  "routeFit": string;
  "timestamp": Date;
  "status": string;
  "id": number;
  "assetId": string;
  "driverId": number;
  "nurseId": number;
  "driverQuestionnaireId": string;
  "nurseQuestionnaireId": string;
  "driverSelfQuestionnaireId": string;
  "dispatcherQuestionnaireId": string;
  "routes_id": number;
  asset: Asset;
  m_users: M_users;
  users: M_users;
  question_answers: Question_answers[];
  driverQuestionnaire: Sessions;
  nurseQuestionnaire: Sessions;
  driverSelfQuestionnaire: Sessions;
  dispatcherQuestionnaire: Sessions;
  journey_routes: Journey_routes;
  trip_checkpoint: Trip_checkpoint[];
  constructor(data?: TripInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Trip`.
   */
  public static getModelName() {
    return "Trip";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Trip for dynamic purposes.
  **/
  public static factory(data: TripInterface): Trip{
    return new Trip(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Trip',
      plural: 'Trips',
      path: 'Trips',
      idName: 'id',
      properties: {
        "driverSelfFit": {
          name: 'driverSelfFit',
          type: 'string'
        },
        "driverFit": {
          name: 'driverFit',
          type: 'string'
        },
        "vehicleFit": {
          name: 'vehicleFit',
          type: 'string'
        },
        "routeFit": {
          name: 'routeFit',
          type: 'string'
        },
        "timestamp": {
          name: 'timestamp',
          type: 'Date'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "assetId": {
          name: 'assetId',
          type: 'string'
        },
        "driverId": {
          name: 'driverId',
          type: 'number'
        },
        "nurseId": {
          name: 'nurseId',
          type: 'number'
        },
        "driverQuestionnaireId": {
          name: 'driverQuestionnaireId',
          type: 'string'
        },
        "nurseQuestionnaireId": {
          name: 'nurseQuestionnaireId',
          type: 'string'
        },
        "driverSelfQuestionnaireId": {
          name: 'driverSelfQuestionnaireId',
          type: 'string'
        },
        "dispatcherQuestionnaireId": {
          name: 'dispatcherQuestionnaireId',
          type: 'string'
        },
        "routes_id": {
          name: 'routes_id',
          type: 'number'
        },
      },
      relations: {
        asset: {
          name: 'asset',
          type: 'Asset',
          model: 'Asset',
          relationType: 'belongsTo',
                  keyFrom: 'assetId',
          keyTo: 'asset_id'
        },
        m_users: {
          name: 'm_users',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'driverId',
          keyTo: 'id'
        },
        users: {
          name: 'users',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'nurseId',
          keyTo: 'id'
        },
        question_answers: {
          name: 'question_answers',
          type: 'Question_answers[]',
          model: 'Question_answers',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'tripId'
        },
        driverQuestionnaire: {
          name: 'driverQuestionnaire',
          type: 'Sessions',
          model: 'Sessions',
          relationType: 'belongsTo',
                  keyFrom: 'driverQuestionnaireId',
          keyTo: 'test_id'
        },
        nurseQuestionnaire: {
          name: 'nurseQuestionnaire',
          type: 'Sessions',
          model: 'Sessions',
          relationType: 'belongsTo',
                  keyFrom: 'nurseQuestionnaireId',
          keyTo: 'test_id'
        },
        driverSelfQuestionnaire: {
          name: 'driverSelfQuestionnaire',
          type: 'Sessions',
          model: 'Sessions',
          relationType: 'belongsTo',
                  keyFrom: 'driverSelfQuestionnaireId',
          keyTo: 'test_id'
        },
        dispatcherQuestionnaire: {
          name: 'dispatcherQuestionnaire',
          type: 'Sessions',
          model: 'Sessions',
          relationType: 'belongsTo',
                  keyFrom: 'dispatcherQuestionnaireId',
          keyTo: 'test_id'
        },
        journey_routes: {
          name: 'journey_routes',
          type: 'Journey_routes',
          model: 'Journey_routes',
          relationType: 'belongsTo',
                  keyFrom: 'routes_id',
          keyTo: 'id'
        },
        trip_checkpoint: {
          name: 'trip_checkpoint',
          type: 'Trip_checkpoint[]',
          model: 'Trip_checkpoint',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'tripId'
        },
      }
    }
  }
}
