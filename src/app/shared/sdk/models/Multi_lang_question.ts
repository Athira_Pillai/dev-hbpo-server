/* tslint:disable */
import {
  Questions
} from '../index';

declare var Object: any;
export interface Multi_lang_questionInterface {
  "question"?: string;
  "help_text"?: string;
  "option1"?: string;
  "option2"?: string;
  "option3"?: string;
  "option4"?: string;
  "option5"?: string;
  "language"?: string;
  "id"?: number;
  "main_question_id"?: string;
  questions?: Questions;
}

export class Multi_lang_question implements Multi_lang_questionInterface {
  "question": string;
  "help_text": string;
  "option1": string;
  "option2": string;
  "option3": string;
  "option4": string;
  "option5": string;
  "language": string;
  "id": number;
  "main_question_id": string;
  questions: Questions;
  constructor(data?: Multi_lang_questionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Multi_lang_question`.
   */
  public static getModelName() {
    return "Multi_lang_question";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Multi_lang_question for dynamic purposes.
  **/
  public static factory(data: Multi_lang_questionInterface): Multi_lang_question{
    return new Multi_lang_question(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Multi_lang_question',
      plural: 'Multi_lang_questions',
      path: 'Multi_lang_questions',
      idName: 'id',
      properties: {
        "question": {
          name: 'question',
          type: 'string'
        },
        "help_text": {
          name: 'help_text',
          type: 'string'
        },
        "option1": {
          name: 'option1',
          type: 'string'
        },
        "option2": {
          name: 'option2',
          type: 'string'
        },
        "option3": {
          name: 'option3',
          type: 'string'
        },
        "option4": {
          name: 'option4',
          type: 'string'
        },
        "option5": {
          name: 'option5',
          type: 'string'
        },
        "language": {
          name: 'language',
          type: 'string',
          default: 'en-US'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "main_question_id": {
          name: 'main_question_id',
          type: 'string'
        },
      },
      relations: {
        questions: {
          name: 'questions',
          type: 'Questions',
          model: 'Questions',
          relationType: 'belongsTo',
                  keyFrom: 'main_question_id',
          keyTo: 'questionId'
        },
      }
    }
  }
}
