/* tslint:disable */
import {
  M_users,
  Question_answers,
  Job_function,
  Job_task,
  Asset,
  Trip,
  Location
} from '../index';

declare var Object: any;
export interface SessionsInterface {
  "sessionID"?: string;
  "clientDate"?: Date;
  "session_weight"?: number;
  "speed_index"?: number;
  "pattern_index"?: number;
  "time_index"?: number;
  "consistency_index"?: number;
  "comments"?: string;
  "test_id": string;
  "status": number;
  "lat"?: string;
  "lng"?: string;
  "user_id"?: number;
  "job_function_id"?: number;
  "job_task_id"?: number;
  "assetId"?: string;
  "locationId"?: number;
  m_users?: M_users;
  question_answers?: Question_answers[];
  job_function?: Job_function;
  job_task?: Job_task;
  asset?: Asset;
  trip_driver_questionnaire?: Trip[];
  trip_nurse_questionnaire?: Trip[];
  trip_driverself_questionnaire?: Trip[];
  trip_dispatch_questionnaire?: Trip[];
  location?: Location;
}

export class Sessions implements SessionsInterface {
  "sessionID": string;
  "clientDate": Date;
  "session_weight": number;
  "speed_index": number;
  "pattern_index": number;
  "time_index": number;
  "consistency_index": number;
  "comments": string;
  "test_id": string;
  "status": number;
  "lat": string;
  "lng": string;
  "user_id": number;
  "job_function_id": number;
  "job_task_id": number;
  "assetId": string;
  "locationId": number;
  m_users: M_users;
  question_answers: Question_answers[];
  job_function: Job_function;
  job_task: Job_task;
  asset: Asset;
  trip_driver_questionnaire: Trip[];
  trip_nurse_questionnaire: Trip[];
  trip_driverself_questionnaire: Trip[];
  trip_dispatch_questionnaire: Trip[];
  location: Location;
  constructor(data?: SessionsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Sessions`.
   */
  public static getModelName() {
    return "Sessions";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Sessions for dynamic purposes.
  **/
  public static factory(data: SessionsInterface): Sessions{
    return new Sessions(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Sessions',
      plural: 'sessions',
      path: 'sessions',
      idName: 'test_id',
      properties: {
        "sessionID": {
          name: 'sessionID',
          type: 'string'
        },
        "clientDate": {
          name: 'clientDate',
          type: 'Date'
        },
        "session_weight": {
          name: 'session_weight',
          type: 'number',
          default: 0
        },
        "speed_index": {
          name: 'speed_index',
          type: 'number'
        },
        "pattern_index": {
          name: 'pattern_index',
          type: 'number'
        },
        "time_index": {
          name: 'time_index',
          type: 'number'
        },
        "consistency_index": {
          name: 'consistency_index',
          type: 'number'
        },
        "comments": {
          name: 'comments',
          type: 'string'
        },
        "test_id": {
          name: 'test_id',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'number'
        },
        "lat": {
          name: 'lat',
          type: 'string'
        },
        "lng": {
          name: 'lng',
          type: 'string'
        },
        "user_id": {
          name: 'user_id',
          type: 'number'
        },
        "job_function_id": {
          name: 'job_function_id',
          type: 'number'
        },
        "job_task_id": {
          name: 'job_task_id',
          type: 'number'
        },
        "assetId": {
          name: 'assetId',
          type: 'string'
        },
        "locationId": {
          name: 'locationId',
          type: 'number'
        },
      },
      relations: {
        m_users: {
          name: 'm_users',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'user_id',
          keyTo: 'id'
        },
        question_answers: {
          name: 'question_answers',
          type: 'Question_answers[]',
          model: 'Question_answers',
          relationType: 'hasMany',
                  keyFrom: 'test_id',
          keyTo: 'test_id'
        },
        job_function: {
          name: 'job_function',
          type: 'Job_function',
          model: 'Job_function',
          relationType: 'belongsTo',
                  keyFrom: 'job_function_id',
          keyTo: 'id'
        },
        job_task: {
          name: 'job_task',
          type: 'Job_task',
          model: 'Job_task',
          relationType: 'belongsTo',
                  keyFrom: 'job_task_id',
          keyTo: 'id'
        },
        asset: {
          name: 'asset',
          type: 'Asset',
          model: 'Asset',
          relationType: 'belongsTo',
                  keyFrom: 'assetId',
          keyTo: 'asset_id'
        },
        trip_driver_questionnaire: {
          name: 'trip_driver_questionnaire',
          type: 'Trip[]',
          model: 'Trip',
          relationType: 'hasMany',
                  keyFrom: 'test_id',
          keyTo: 'driverQuestionnaireId'
        },
        trip_nurse_questionnaire: {
          name: 'trip_nurse_questionnaire',
          type: 'Trip[]',
          model: 'Trip',
          relationType: 'hasMany',
                  keyFrom: 'test_id',
          keyTo: 'nurseQuestionnaireId'
        },
        trip_driverself_questionnaire: {
          name: 'trip_driverself_questionnaire',
          type: 'Trip[]',
          model: 'Trip',
          relationType: 'hasMany',
                  keyFrom: 'test_id',
          keyTo: 'driverSelfQuestionnaireId'
        },
        trip_dispatch_questionnaire: {
          name: 'trip_dispatch_questionnaire',
          type: 'Trip[]',
          model: 'Trip',
          relationType: 'hasMany',
                  keyFrom: 'test_id',
          keyTo: 'dispatcherQuestionnaireId'
        },
        location: {
          name: 'location',
          type: 'Location',
          model: 'Location',
          relationType: 'belongsTo',
                  keyFrom: 'locationId',
          keyTo: 'id'
        },
      }
    }
  }
}
