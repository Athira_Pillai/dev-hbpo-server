/* tslint:disable */

declare var Object: any;
export interface Response_quality_indexInterface {
  "rqi"?: string;
  "id"?: number;
}

export class Response_quality_index implements Response_quality_indexInterface {
  "rqi": string;
  "id": number;
  constructor(data?: Response_quality_indexInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Response_quality_index`.
   */
  public static getModelName() {
    return "Response_quality_index";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Response_quality_index for dynamic purposes.
  **/
  public static factory(data: Response_quality_indexInterface): Response_quality_index{
    return new Response_quality_index(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Response_quality_index',
      plural: 'Response_quality_indices',
      path: 'Response_quality_indices',
      idName: 'id',
      properties: {
        "rqi": {
          name: 'rqi',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
