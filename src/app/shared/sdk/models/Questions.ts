/* tslint:disable */
import {
  Job_function,
  Critical_control,
  Risks,
  Performance_requirements,
  Question_answers,
  Question_type,
  Job_questions,
  Question_group_header,
  Multi_lang_question
} from '../index';

declare var Object: any;
export interface QuestionsInterface {
  "questionId"?: string;
  "question"?: string;
  "help_text"?: string;
  "option1"?: string;
  "option2"?: string;
  "option3"?: string;
  "option4"?: string;
  "option5"?: string;
  "correct_answer"?: string;
  "frequency"?: string;
  "score"?: string;
  "type_of_input"?: string;
  "sortId"?: number;
  "language"?: string;
  "group_id"?: number;
  "cc_id"?: number;
  "rm_id"?: number;
  "perf_id"?: number;
  "type_id"?: string;
  "questgroup_id"?: number;
  "priority_id"?: number;
  job_function?: Job_function;
  critical_control?: Critical_control;
  risks?: Risks;
  performance_requirements?: Performance_requirements;
  question_answers?: Question_answers[];
  question_type?: Question_type;
  job_questions?: Job_questions;
  question_group_header?: Question_group_header;
  multi_lang_question?: Multi_lang_question[];
}

export class Questions implements QuestionsInterface {
  "questionId": string;
  "question": string;
  "help_text": string;
  "option1": string;
  "option2": string;
  "option3": string;
  "option4": string;
  "option5": string;
  "correct_answer": string;
  "frequency": string;
  "score": string;
  "type_of_input": string;
  "sortId": number;
  "language": string;
  "group_id": number;
  "cc_id": number;
  "rm_id": number;
  "perf_id": number;
  "type_id": string;
  "questgroup_id": number;
  "priority_id": number;
  job_function: Job_function;
  critical_control: Critical_control;
  risks: Risks;
  performance_requirements: Performance_requirements;
  question_answers: Question_answers[];
  question_type: Question_type;
  job_questions: Job_questions;
  question_group_header: Question_group_header;
  multi_lang_question: Multi_lang_question[];
  constructor(data?: QuestionsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Questions`.
   */
  public static getModelName() {
    return "Questions";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Questions for dynamic purposes.
  **/
  public static factory(data: QuestionsInterface): Questions{
    return new Questions(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Questions',
      plural: 'Questions',
      path: 'Questions',
      idName: 'questionId',
      properties: {
        "questionId": {
          name: 'questionId',
          type: 'string'
        },
        "question": {
          name: 'question',
          type: 'string'
        },
        "help_text": {
          name: 'help_text',
          type: 'string'
        },
        "option1": {
          name: 'option1',
          type: 'string'
        },
        "option2": {
          name: 'option2',
          type: 'string'
        },
        "option3": {
          name: 'option3',
          type: 'string'
        },
        "option4": {
          name: 'option4',
          type: 'string'
        },
        "option5": {
          name: 'option5',
          type: 'string'
        },
        "correct_answer": {
          name: 'correct_answer',
          type: 'string'
        },
        "frequency": {
          name: 'frequency',
          type: 'string'
        },
        "score": {
          name: 'score',
          type: 'string'
        },
        "type_of_input": {
          name: 'type_of_input',
          type: 'string'
        },
        "sortId": {
          name: 'sortId',
          type: 'number'
        },
        "language": {
          name: 'language',
          type: 'string'
        },
        "group_id": {
          name: 'group_id',
          type: 'number'
        },
        "cc_id": {
          name: 'cc_id',
          type: 'number'
        },
        "rm_id": {
          name: 'rm_id',
          type: 'number'
        },
        "perf_id": {
          name: 'perf_id',
          type: 'number'
        },
        "type_id": {
          name: 'type_id',
          type: 'string'
        },
        "questgroup_id": {
          name: 'questgroup_id',
          type: 'number'
        },
        "priority_id": {
          name: 'priority_id',
          type: 'number'
        },
      },
      relations: {
        job_function: {
          name: 'job_function',
          type: 'Job_function',
          model: 'Job_function',
          relationType: 'belongsTo',
                  keyFrom: 'group_id',
          keyTo: 'id'
        },
        critical_control: {
          name: 'critical_control',
          type: 'Critical_control',
          model: 'Critical_control',
          relationType: 'belongsTo',
                  keyFrom: 'cc_id',
          keyTo: 'id'
        },
        risks: {
          name: 'risks',
          type: 'Risks',
          model: 'Risks',
          relationType: 'belongsTo',
                  keyFrom: 'rm_id',
          keyTo: 'rm_id'
        },
        performance_requirements: {
          name: 'performance_requirements',
          type: 'Performance_requirements',
          model: 'Performance_requirements',
          relationType: 'belongsTo',
                  keyFrom: 'perf_id',
          keyTo: 'id'
        },
        question_answers: {
          name: 'question_answers',
          type: 'Question_answers[]',
          model: 'Question_answers',
          relationType: 'hasMany',
                  keyFrom: 'questionId',
          keyTo: 'questionId'
        },
        question_type: {
          name: 'question_type',
          type: 'Question_type',
          model: 'Question_type',
          relationType: 'belongsTo',
                  keyFrom: 'type_id',
          keyTo: 'id'
        },
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions',
          model: 'Job_questions',
          relationType: 'belongsTo',
                  keyFrom: 'questgroup_id',
          keyTo: 'question_group'
        },
        question_group_header: {
          name: 'question_group_header',
          type: 'Question_group_header',
          model: 'Question_group_header',
          relationType: 'belongsTo',
                  keyFrom: 'priority_id',
          keyTo: 'id'
        },
        multi_lang_question: {
          name: 'multi_lang_question',
          type: 'Multi_lang_question[]',
          model: 'Multi_lang_question',
          relationType: 'hasMany',
                  keyFrom: 'questionId',
          keyTo: 'main_question_id'
        },
      }
    }
  }
}
