/* tslint:disable */
import {
  Job_questions,
  Userrole
} from '../index';

declare var Object: any;
export interface Job_functionInterface {
  "id": number;
  "jobFunctionName": string;
  "sortId"?: number;
  job_questions?: Job_questions[];
  userrole?: Userrole[];
}

export class Job_function implements Job_functionInterface {
  "id": number;
  "jobFunctionName": string;
  "sortId": number;
  job_questions: Job_questions[];
  userrole: Userrole[];
  constructor(data?: Job_functionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Job_function`.
   */
  public static getModelName() {
    return "Job_function";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Job_function for dynamic purposes.
  **/
  public static factory(data: Job_functionInterface): Job_function{
    return new Job_function(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Job_function',
      plural: 'job_function',
      path: 'job_function',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "jobFunctionName": {
          name: 'jobFunctionName',
          type: 'string'
        },
        "sortId": {
          name: 'sortId',
          type: 'number'
        },
      },
      relations: {
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions[]',
          model: 'Job_questions',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'jobFunctionId'
        },
        userrole: {
          name: 'userrole',
          type: 'Userrole[]',
          model: 'Userrole',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'jobFunctionId'
        },
      }
    }
  }
}
