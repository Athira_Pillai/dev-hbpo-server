/* tslint:disable */
import {
  Asset,
  Job_questions
} from '../index';

declare var Object: any;
export interface Asset_typeInterface {
  "asset_name"?: string;
  "id"?: number;
  asset?: Asset[];
  job_questions?: Job_questions;
}

export class Asset_type implements Asset_typeInterface {
  "asset_name": string;
  "id": number;
  asset: Asset[];
  job_questions: Job_questions;
  constructor(data?: Asset_typeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Asset_type`.
   */
  public static getModelName() {
    return "Asset_type";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Asset_type for dynamic purposes.
  **/
  public static factory(data: Asset_typeInterface): Asset_type{
    return new Asset_type(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Asset_type',
      plural: 'Asset_types',
      path: 'Asset_types',
      idName: 'id',
      properties: {
        "asset_name": {
          name: 'asset_name',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
        asset: {
          name: 'asset',
          type: 'Asset[]',
          model: 'Asset',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'assetTypeId'
        },
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions',
          model: 'Job_questions',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'assetTypeId'
        },
      }
    }
  }
}
