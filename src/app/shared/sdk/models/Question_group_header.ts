/* tslint:disable */
import {
  Job_questions
} from '../index';

declare var Object: any;
export interface Question_group_headerInterface {
  "id"?: number;
  "priority"?: string;
  "header"?: string;
  "status"?: string;
  job_questions?: Job_questions[];
}

export class Question_group_header implements Question_group_headerInterface {
  "id": number;
  "priority": string;
  "header": string;
  "status": string;
  job_questions: Job_questions[];
  constructor(data?: Question_group_headerInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Question_group_header`.
   */
  public static getModelName() {
    return "Question_group_header";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Question_group_header for dynamic purposes.
  **/
  public static factory(data: Question_group_headerInterface): Question_group_header{
    return new Question_group_header(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Question_group_header',
      plural: 'Question_group_headers',
      path: 'Question_group_headers',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "priority": {
          name: 'priority',
          type: 'string'
        },
        "header": {
          name: 'header',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
      },
      relations: {
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions[]',
          model: 'Job_questions',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'question_group_header_id'
        },
      }
    }
  }
}
