/* tslint:disable */
import {
  Risks,
  Cc_average
} from '../index';

declare var Object: any;
export interface Critical_controlInterface {
  "id": number;
  "cc_name": string;
  "sortId"?: number;
  "rm_id"?: number;
  risks?: Risks;
  cc_average?: Cc_average[];
}

export class Critical_control implements Critical_controlInterface {
  "id": number;
  "cc_name": string;
  "sortId": number;
  "rm_id": number;
  risks: Risks;
  cc_average: Cc_average[];
  constructor(data?: Critical_controlInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Critical_control`.
   */
  public static getModelName() {
    return "Critical_control";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Critical_control for dynamic purposes.
  **/
  public static factory(data: Critical_controlInterface): Critical_control{
    return new Critical_control(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Critical_control',
      plural: 'critical_control',
      path: 'critical_control',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "cc_name": {
          name: 'cc_name',
          type: 'string'
        },
        "sortId": {
          name: 'sortId',
          type: 'number'
        },
        "rm_id": {
          name: 'rm_id',
          type: 'number'
        },
      },
      relations: {
        risks: {
          name: 'risks',
          type: 'Risks',
          model: 'Risks',
          relationType: 'belongsTo',
                  keyFrom: 'rm_id',
          keyTo: 'rm_id'
        },
        cc_average: {
          name: 'cc_average',
          type: 'Cc_average[]',
          model: 'Cc_average',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'cc_id'
        },
      }
    }
  }
}
