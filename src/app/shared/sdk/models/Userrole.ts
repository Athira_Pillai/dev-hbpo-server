/* tslint:disable */
import {
  M_users,
  Job_function
} from '../index';

declare var Object: any;
export interface UserroleInterface {
  "id"?: number;
  "userId"?: number;
  "jobFunctionId"?: number;
  m_users?: M_users;
  job_function?: Job_function;
}

export class Userrole implements UserroleInterface {
  "id": number;
  "userId": number;
  "jobFunctionId": number;
  m_users: M_users;
  job_function: Job_function;
  constructor(data?: UserroleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Userrole`.
   */
  public static getModelName() {
    return "Userrole";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Userrole for dynamic purposes.
  **/
  public static factory(data: UserroleInterface): Userrole{
    return new Userrole(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Userrole',
      plural: 'Userroles',
      path: 'Userroles',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "jobFunctionId": {
          name: 'jobFunctionId',
          type: 'number'
        },
      },
      relations: {
        m_users: {
          name: 'm_users',
          type: 'M_users',
          model: 'M_users',
          relationType: 'belongsTo',
                  keyFrom: 'userId',
          keyTo: 'id'
        },
        job_function: {
          name: 'job_function',
          type: 'Job_function',
          model: 'Job_function',
          relationType: 'belongsTo',
                  keyFrom: 'jobFunctionId',
          keyTo: 'id'
        },
      }
    }
  }
}
