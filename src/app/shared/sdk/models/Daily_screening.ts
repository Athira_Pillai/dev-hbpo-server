/* tslint:disable */
import {
  Report
} from '../index';

declare var Object: any;
export interface Daily_screeningInterface {
  "date"?: Date;
  "pass"?: number;
  "fail"?: number;
  "medium_priority"?: number;
  "low_priority"?: number;
  "total_screening"?: number;
  "id"?: number;
  "report_id"?: number;
  report?: Report;
}

export class Daily_screening implements Daily_screeningInterface {
  "date": Date;
  "pass": number;
  "fail": number;
  "medium_priority": number;
  "low_priority": number;
  "total_screening": number;
  "id": number;
  "report_id": number;
  report: Report;
  constructor(data?: Daily_screeningInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Daily_screening`.
   */
  public static getModelName() {
    return "Daily_screening";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Daily_screening for dynamic purposes.
  **/
  public static factory(data: Daily_screeningInterface): Daily_screening{
    return new Daily_screening(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Daily_screening',
      plural: 'Daily_screenings',
      path: 'Daily_screenings',
      idName: 'id',
      properties: {
        "date": {
          name: 'date',
          type: 'Date'
        },
        "pass": {
          name: 'pass',
          type: 'number'
        },
        "fail": {
          name: 'fail',
          type: 'number'
        },
        "medium_priority": {
          name: 'medium_priority',
          type: 'number'
        },
        "low_priority": {
          name: 'low_priority',
          type: 'number'
        },
        "total_screening": {
          name: 'total_screening',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "report_id": {
          name: 'report_id',
          type: 'number'
        },
      },
      relations: {
        report: {
          name: 'report',
          type: 'Report',
          model: 'Report',
          relationType: 'belongsTo',
                  keyFrom: 'report_id',
          keyTo: 'id'
        },
      }
    }
  }
}
