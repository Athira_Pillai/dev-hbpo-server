/* tslint:disable */
import {
  Critical_control,
  Questions,
  Perf_average
} from '../index';

declare var Object: any;
export interface Performance_requirementsInterface {
  "id": number;
  "perf_name": string;
  "cc_id"?: number;
  critical_control?: Critical_control;
  questions?: Questions;
  perf_average?: Perf_average[];
}

export class Performance_requirements implements Performance_requirementsInterface {
  "id": number;
  "perf_name": string;
  "cc_id": number;
  critical_control: Critical_control;
  questions: Questions;
  perf_average: Perf_average[];
  constructor(data?: Performance_requirementsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Performance_requirements`.
   */
  public static getModelName() {
    return "Performance_requirements";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Performance_requirements for dynamic purposes.
  **/
  public static factory(data: Performance_requirementsInterface): Performance_requirements{
    return new Performance_requirements(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Performance_requirements',
      plural: 'Performance_requirements',
      path: 'Performance_requirements',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "perf_name": {
          name: 'perf_name',
          type: 'string'
        },
        "cc_id": {
          name: 'cc_id',
          type: 'number'
        },
      },
      relations: {
        critical_control: {
          name: 'critical_control',
          type: 'Critical_control',
          model: 'Critical_control',
          relationType: 'belongsTo',
                  keyFrom: 'cc_id',
          keyTo: 'id'
        },
        questions: {
          name: 'questions',
          type: 'Questions',
          model: 'Questions',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'perf_id'
        },
        perf_average: {
          name: 'perf_average',
          type: 'Perf_average[]',
          model: 'Perf_average',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'perf_id'
        },
      }
    }
  }
}
