/* tslint:disable */
import {
  Report
} from '../index';

declare var Object: any;
export interface Daily_reportInterface {
  "date"?: Date;
  "medicalchecklist_pass"?: number;
  "medicalchecklist_fail"?: number;
  "vehiclechecklist_pass"?: number;
  "vehiclechecklist_fail"?: number;
  "medicalchecklist_total"?: number;
  "vehiclechecklist_total"?: number;
  "journey_start"?: number;
  "journey_incomplete"?: number;
  "journey_complete"?: number;
  "id"?: number;
  "report_id"?: number;
  report?: Report;
}

export class Daily_report implements Daily_reportInterface {
  "date": Date;
  "medicalchecklist_pass": number;
  "medicalchecklist_fail": number;
  "vehiclechecklist_pass": number;
  "vehiclechecklist_fail": number;
  "medicalchecklist_total": number;
  "vehiclechecklist_total": number;
  "journey_start": number;
  "journey_incomplete": number;
  "journey_complete": number;
  "id": number;
  "report_id": number;
  report: Report;
  constructor(data?: Daily_reportInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Daily_report`.
   */
  public static getModelName() {
    return "Daily_report";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Daily_report for dynamic purposes.
  **/
  public static factory(data: Daily_reportInterface): Daily_report{
    return new Daily_report(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Daily_report',
      plural: 'Daily_reports',
      path: 'Daily_reports',
      idName: 'id',
      properties: {
        "date": {
          name: 'date',
          type: 'Date'
        },
        "medicalchecklist_pass": {
          name: 'medicalchecklist_pass',
          type: 'number'
        },
        "medicalchecklist_fail": {
          name: 'medicalchecklist_fail',
          type: 'number'
        },
        "vehiclechecklist_pass": {
          name: 'vehiclechecklist_pass',
          type: 'number'
        },
        "vehiclechecklist_fail": {
          name: 'vehiclechecklist_fail',
          type: 'number'
        },
        "medicalchecklist_total": {
          name: 'medicalchecklist_total',
          type: 'number'
        },
        "vehiclechecklist_total": {
          name: 'vehiclechecklist_total',
          type: 'number'
        },
        "journey_start": {
          name: 'journey_start',
          type: 'number'
        },
        "journey_incomplete": {
          name: 'journey_incomplete',
          type: 'number'
        },
        "journey_complete": {
          name: 'journey_complete',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "report_id": {
          name: 'report_id',
          type: 'number'
        },
      },
      relations: {
        report: {
          name: 'report',
          type: 'Report',
          model: 'Report',
          relationType: 'belongsTo',
                  keyFrom: 'report_id',
          keyTo: 'id'
        },
      }
    }
  }
}
