/* tslint:disable */

declare var Object: any;
export interface FeedbackInterface {
  "name": string;
  "email": string;
  "feedback": string;
  "id"?: number;
  "createdAt": Date;
  "updatedAt": Date;
}

export class Feedback implements FeedbackInterface {
  "name": string;
  "email": string;
  "feedback": string;
  "id": number;
  "createdAt": Date;
  "updatedAt": Date;
  constructor(data?: FeedbackInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Feedback`.
   */
  public static getModelName() {
    return "Feedback";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Feedback for dynamic purposes.
  **/
  public static factory(data: FeedbackInterface): Feedback{
    return new Feedback(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Feedback',
      plural: 'Feedbacks',
      path: 'Feedbacks',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "feedback": {
          name: 'feedback',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
