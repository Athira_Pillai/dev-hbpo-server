/* tslint:disable */

declare var Object: any;
export interface Job_taskInterface {
  "jobTaskName"?: string;
  "sortId"?: number;
  "id"?: number;
}

export class Job_task implements Job_taskInterface {
  "jobTaskName": string;
  "sortId": number;
  "id": number;
  constructor(data?: Job_taskInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Job_task`.
   */
  public static getModelName() {
    return "Job_task";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Job_task for dynamic purposes.
  **/
  public static factory(data: Job_taskInterface): Job_task{
    return new Job_task(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Job_task',
      plural: 'job_task',
      path: 'job_task',
      idName: 'id',
      properties: {
        "jobTaskName": {
          name: 'jobTaskName',
          type: 'string'
        },
        "sortId": {
          name: 'sortId',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
