/* tslint:disable */
import {
  Email,
  Daily_report
} from '../index';

declare var Object: any;
export interface ReportInterface {
  "subject"?: string;
  "time"?: string;
  "last_run_status"?: string;
  "id"?: number;
  email?: Email[];
  daily_report?: Daily_report[];
}

export class Report implements ReportInterface {
  "subject": string;
  "time": string;
  "last_run_status": string;
  "id": number;
  email: Email[];
  daily_report: Daily_report[];
  constructor(data?: ReportInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Report`.
   */
  public static getModelName() {
    return "Report";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Report for dynamic purposes.
  **/
  public static factory(data: ReportInterface): Report{
    return new Report(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Report',
      plural: 'Reports',
      path: 'Reports',
      idName: 'id',
      properties: {
        "subject": {
          name: 'subject',
          type: 'string'
        },
        "time": {
          name: 'time',
          type: 'string'
        },
        "last_run_status": {
          name: 'last_run_status',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
        email: {
          name: 'email',
          type: 'Email[]',
          model: 'Email',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'report_id'
        },
        daily_report: {
          name: 'daily_report',
          type: 'Daily_report[]',
          model: 'Daily_report',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'id'
        },
      }
    }
  }
}
