/* tslint:disable */
import {
  Job_function
} from '../index';

declare var Object: any;
export interface Report_typeInterface {
  "name"?: string;
  "id"?: number;
  "jobFunctionId"?: number;
  job_function?: Job_function;
}

export class Report_type implements Report_typeInterface {
  "name": string;
  "id": number;
  "jobFunctionId": number;
  job_function: Job_function;
  constructor(data?: Report_typeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Report_type`.
   */
  public static getModelName() {
    return "Report_type";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Report_type for dynamic purposes.
  **/
  public static factory(data: Report_typeInterface): Report_type{
    return new Report_type(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Report_type',
      plural: 'Report_types',
      path: 'Report_types',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "jobFunctionId": {
          name: 'jobFunctionId',
          type: 'number'
        },
      },
      relations: {
        job_function: {
          name: 'job_function',
          type: 'Job_function',
          model: 'Job_function',
          relationType: 'belongsTo',
                  keyFrom: 'jobFunctionId',
          keyTo: 'id'
        },
      }
    }
  }
}
