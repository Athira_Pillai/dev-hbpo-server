/* tslint:disable */

declare var Object: any;
export interface Question_typeInterface {
  "id": string;
  "type_name": string;
}

export class Question_type implements Question_typeInterface {
  "id": string;
  "type_name": string;
  constructor(data?: Question_typeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Question_type`.
   */
  public static getModelName() {
    return "Question_type";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Question_type for dynamic purposes.
  **/
  public static factory(data: Question_typeInterface): Question_type{
    return new Question_type(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Question_type',
      plural: 'Question_types',
      path: 'Question_types',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "type_name": {
          name: 'type_name',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
