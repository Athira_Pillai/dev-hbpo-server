/* tslint:disable */
import {
  Job_questions,
  Location,
  Journey_checkpoints
} from '../index';

declare var Object: any;
export interface Journey_routesInterface {
  "routes"?: string;
  "id"?: number;
  "location_startId"?: number;
  "location_endId"?: number;
  job_questions?: Job_questions;
  location_start?: Location;
  location_end?: Location;
  journey_checkpoint?: Journey_checkpoints[];
}

export class Journey_routes implements Journey_routesInterface {
  "routes": string;
  "id": number;
  "location_startId": number;
  "location_endId": number;
  job_questions: Job_questions;
  location_start: Location;
  location_end: Location;
  journey_checkpoint: Journey_checkpoints[];
  constructor(data?: Journey_routesInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Journey_routes`.
   */
  public static getModelName() {
    return "Journey_routes";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Journey_routes for dynamic purposes.
  **/
  public static factory(data: Journey_routesInterface): Journey_routes{
    return new Journey_routes(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Journey_routes',
      plural: 'Journey_routes',
      path: 'Journey_routes',
      idName: 'id',
      properties: {
        "routes": {
          name: 'routes',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "location_startId": {
          name: 'location_startId',
          type: 'number'
        },
        "location_endId": {
          name: 'location_endId',
          type: 'number'
        },
      },
      relations: {
        job_questions: {
          name: 'job_questions',
          type: 'Job_questions',
          model: 'Job_questions',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'routeId'
        },
        location_start: {
          name: 'location_start',
          type: 'Location',
          model: 'Location',
          relationType: 'belongsTo',
                  keyFrom: 'location_startId',
          keyTo: 'id'
        },
        location_end: {
          name: 'location_end',
          type: 'Location',
          model: 'Location',
          relationType: 'belongsTo',
                  keyFrom: 'location_endId',
          keyTo: 'id'
        },
        journey_checkpoint: {
          name: 'journey_checkpoint',
          type: 'Journey_checkpoints[]',
          model: 'Journey_checkpoints',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'journeyRoutesId'
        },
      }
    }
  }
}
