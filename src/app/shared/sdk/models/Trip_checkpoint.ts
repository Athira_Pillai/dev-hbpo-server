/* tslint:disable */

declare var Object: any;
export interface Trip_checkpointInterface {
  "checkpoint"?: string;
  "timestamp"?: Date;
  "tripId"?: number;
  "testId"?: string;
  "id"?: number;
}

export class Trip_checkpoint implements Trip_checkpointInterface {
  "checkpoint": string;
  "timestamp": Date;
  "tripId": number;
  "testId": string;
  "id": number;
  constructor(data?: Trip_checkpointInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Trip_checkpoint`.
   */
  public static getModelName() {
    return "Trip_checkpoint";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Trip_checkpoint for dynamic purposes.
  **/
  public static factory(data: Trip_checkpointInterface): Trip_checkpoint{
    return new Trip_checkpoint(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Trip_checkpoint',
      plural: 'Trip_checkpoints',
      path: 'Trip_checkpoints',
      idName: 'id',
      properties: {
        "checkpoint": {
          name: 'checkpoint',
          type: 'string'
        },
        "timestamp": {
          name: 'timestamp',
          type: 'Date'
        },
        "tripId": {
          name: 'tripId',
          type: 'number'
        },
        "testId": {
          name: 'testId',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
