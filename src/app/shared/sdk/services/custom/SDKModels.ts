/* tslint:disable */
import { Injectable } from '@angular/core';
import { M_users } from '../../models/M_users';
import { Job_function } from '../../models/Job_function';
import { Critical_control } from '../../models/Critical_control';
import { Risks } from '../../models/Risks';
import { Performance_requirements } from '../../models/Performance_requirements';
import { Question_answers } from '../../models/Question_answers';
import { Questions } from '../../models/Questions';
import { Question_type } from '../../models/Question_type';
import { Sessions } from '../../models/Sessions';
import { Uploadfile } from '../../models/Uploadfile';
import { Job_task } from '../../models/Job_task';
import { Job_questions } from '../../models/Job_questions';
import { Asset } from '../../models/Asset';
import { Question_group_header } from '../../models/Question_group_header';
import { Trip } from '../../models/Trip';
import { Journey_routes } from '../../models/Journey_routes';
import { Location } from '../../models/Location';
import { Journey_checkpoints } from '../../models/Journey_checkpoints';
import { Feedback } from '../../models/Feedback';
import { Email } from '../../models/Email';
import { Report } from '../../models/Report';
import { Daily_report } from '../../models/Daily_report';
import { Cc_average } from '../../models/Cc_average';
import { Risk_average } from '../../models/Risk_average';
import { Response_quality_index } from '../../models/Response_quality_index';
import { Trip_checkpoint } from '../../models/Trip_checkpoint';
import { Alert } from '../../models/Alert';
import { Asset_type } from '../../models/Asset_type';
import { Push } from '../../models/Push';
import { Application } from '../../models/Application';
import { Installation } from '../../models/Installation';
import { Notification } from '../../models/Notification';
import { Multi_lang_question } from '../../models/Multi_lang_question';
import { Perf_average } from '../../models/Perf_average';
import { Userrole } from '../../models/Userrole';
import { Report_type } from '../../models/Report_type';
import { Email_report } from '../../models/Email_report';
import { Daily_screening } from '../../models/Daily_screening';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    M_users: M_users,
    Job_function: Job_function,
    Critical_control: Critical_control,
    Risks: Risks,
    Performance_requirements: Performance_requirements,
    Question_answers: Question_answers,
    Questions: Questions,
    Question_type: Question_type,
    Sessions: Sessions,
    Uploadfile: Uploadfile,
    Job_task: Job_task,
    Job_questions: Job_questions,
    Asset: Asset,
    Question_group_header: Question_group_header,
    Trip: Trip,
    Journey_routes: Journey_routes,
    Location: Location,
    Journey_checkpoints: Journey_checkpoints,
    Feedback: Feedback,
    Email: Email,
    Report: Report,
    Daily_report: Daily_report,
    Cc_average: Cc_average,
    Risk_average: Risk_average,
    Response_quality_index: Response_quality_index,
    Trip_checkpoint: Trip_checkpoint,
    Alert: Alert,
    Asset_type: Asset_type,
    Push: Push,
    Application: Application,
    Installation: Installation,
    Notification: Notification,
    Multi_lang_question: Multi_lang_question,
    Perf_average: Perf_average,
    Userrole: Userrole,
    Report_type: Report_type,
    Email_report: Email_report,
    Daily_screening: Daily_screening,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
