/* tslint:disable */
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root 
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { SDKModels } from './services/custom/SDKModels';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CookieBrowser } from './storage/cookie.browser';
import { StorageBrowser } from './storage/storage.browser';
import { SocketBrowser } from './sockets/socket.browser';
import { SocketDriver } from './sockets/socket.driver';
import { SocketConnection } from './sockets/socket.connections';
import { RealTime } from './services/core/real.time';
import { M_usersApi } from './services/custom/M_users';
import { Job_functionApi } from './services/custom/Job_function';
import { Critical_controlApi } from './services/custom/Critical_control';
import { RisksApi } from './services/custom/Risks';
import { Performance_requirementsApi } from './services/custom/Performance_requirements';
import { Question_answersApi } from './services/custom/Question_answers';
import { QuestionsApi } from './services/custom/Questions';
import { Question_typeApi } from './services/custom/Question_type';
import { SessionsApi } from './services/custom/Sessions';
import { UploadfileApi } from './services/custom/Uploadfile';
import { Job_taskApi } from './services/custom/Job_task';
import { Job_questionsApi } from './services/custom/Job_questions';
import { AssetApi } from './services/custom/Asset';
import { Question_group_headerApi } from './services/custom/Question_group_header';
import { TripApi } from './services/custom/Trip';
import { Journey_routesApi } from './services/custom/Journey_routes';
import { LocationApi } from './services/custom/Location';
import { Journey_checkpointsApi } from './services/custom/Journey_checkpoints';
import { FeedbackApi } from './services/custom/Feedback';
import { EmailApi } from './services/custom/Email';
import { ReportApi } from './services/custom/Report';
import { Daily_reportApi } from './services/custom/Daily_report';
import { Cc_averageApi } from './services/custom/Cc_average';
import { Risk_averageApi } from './services/custom/Risk_average';
import { Response_quality_indexApi } from './services/custom/Response_quality_index';
import { Trip_checkpointApi } from './services/custom/Trip_checkpoint';
import { AlertApi } from './services/custom/Alert';
import { Asset_typeApi } from './services/custom/Asset_type';
import { PushApi } from './services/custom/Push';
import { ApplicationApi } from './services/custom/Application';
import { InstallationApi } from './services/custom/Installation';
import { NotificationApi } from './services/custom/Notification';
import { Multi_lang_questionApi } from './services/custom/Multi_lang_question';
import { Perf_averageApi } from './services/custom/Perf_average';
import { UserroleApi } from './services/custom/Userrole';
import { Report_typeApi } from './services/custom/Report_type';
import { Email_reportApi } from './services/custom/Email_report';
import { Daily_screeningApi } from './services/custom/Daily_screening';
/**
* @module SDKBrowserModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
@NgModule({
  imports:      [ CommonModule, HttpClientModule ],
  declarations: [ ],
  exports:      [ ],
  providers:    [
    ErrorHandler,
    SocketConnection
  ]
})
export class SDKBrowserModule {
  static forRoot(internalStorageProvider: any = {
    provide: InternalStorage,
    useClass: CookieBrowser
  }): ModuleWithProviders {
    return {
      ngModule  : SDKBrowserModule,
      providers : [
        LoopBackAuth,
        LoggerService,
        SDKModels,
        RealTime,
        M_usersApi,
        Job_functionApi,
        Critical_controlApi,
        RisksApi,
        Performance_requirementsApi,
        Question_answersApi,
        QuestionsApi,
        Question_typeApi,
        SessionsApi,
        UploadfileApi,
        Job_taskApi,
        Job_questionsApi,
        AssetApi,
        Question_group_headerApi,
        TripApi,
        Journey_routesApi,
        LocationApi,
        Journey_checkpointsApi,
        FeedbackApi,
        EmailApi,
        ReportApi,
        Daily_reportApi,
        Cc_averageApi,
        Risk_averageApi,
        Response_quality_indexApi,
        Trip_checkpointApi,
        AlertApi,
        Asset_typeApi,
        PushApi,
        ApplicationApi,
        InstallationApi,
        NotificationApi,
        Multi_lang_questionApi,
        Perf_averageApi,
        UserroleApi,
        Report_typeApi,
        Email_reportApi,
        Daily_screeningApi,
        internalStorageProvider,
        { provide: SDKStorage, useClass: StorageBrowser },
        { provide: SocketDriver, useClass: SocketBrowser }
      ]
    };
  }
}
/**
* Have Fun!!!
* - Jon
**/
export * from './models/index';
export * from './services/index';
export * from './lb.config';
export * from './storage/storage.swaps';
export { CookieBrowser } from './storage/cookie.browser';
export { StorageBrowser } from './storage/storage.browser';

