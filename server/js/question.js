const moment = require('moment')
const dateIterator = require('./date_iterator')
const answerModel = require('./answer')
const timePeriod = require('./time_period')

const Question = (app, data) => {
  let questionCache
  let fetchError

  const fetch = (callback) => {
    if (questionCache !== undefined || fetchError !== undefined) {
      callback(fetchError, questionCache)
      return
    }

    // con.query("select question_id,question,option1,option2,option3,option4,option5,help_text,perf_name,cc_name from question_view1 where cc_name='" + data + "'order by question_id", (err, result) => {
    //   questionCache = result
    //   fetchError = err
    //   callback(fetchError, questionCache)
    // })
    // console.log(data);
    var Question = app.models.questions
    var result = [];
    Question.find({
      where:{
        type_of_input: {neq: 'ACTI'}
      },
      include: [
        {
          relation: 'critical_control'
        },
        {
          relation: 'performance_requirements'
        },
        {
          relation: 'risks'
        },
        {
          relation: 'question_group_header',
          scope:{
            where:{
              priority: {neq: 'G'}
            }
          }
        }
      ]

    }).then((_question) => {
      //  console.log("before for loop"); 
        //  console.log('data in question js',data);
        // console.log("question",_question);
        // console.log("question",_question[0].critical_control());
      for (i = 0; i < _question.length; i++) {
        //  console.log(typeof data)
        if (_question[i].critical_control()) {
          if (_question[i].critical_control().id === +data) {
            //  console.log("in for loop");
            //  console.log(_question[i]);
            var output = {
              "question_id": _question[i].questionId,
              "question": _question[i].question,
              "option1": _question[i].option1, "option2": _question[i].option2,
              "option3": _question[i].option3, "option4": _question[i].option4,
              "option5": _question[i].option5, "help_text": _question[i].help_text,
              "perf_name": _question[i].performance_requirements().perf_name,
              "perf_id": _question[i].performance_requirements().id,
              "cc_name": _question[i].critical_control().cc_name,
              "rm_id": _question[i].critical_control().rm_id,
              "cc_id": _question[i].critical_control().id,
              "rm_name": _question[i].risks().rm_name,
              "priority":_question[i].question_group_header().priority
            };

            result.push(output)

          }
        }

      }
      //  console.log("after for loop");
      //      console.log(result);
      questionCache = result;
      callback(fetchError, questionCache);

    }, (err) => {
      fetchError = err;
      console.log(fetchError);
      callback(fetchError, questionCache);
    })

  }

  const averagePointsPerQuestion = (questionsOut, answers) => {
    // console.log("in averagePoints");
    // console.log(questionsOut);
    const questionMap = {}
    questionsOut.forEach(({ id }) => {
      questionMap[id] = {
        points: 0,
        responseCount: 0
      }
    })

    answers.forEach(({ questionId, answerId, skipped, dontKnow, session_weight }) => {

      const data = questionMap[questionId]
      // console.log("in question averagePointsPerQuestion")
      // console.log(data)
      if (data && !skipped && !dontKnow) {
        // data.points += answerModel().pointsPerAnswer(answerId)
        // data.responseCount++
        data.points += answerModel().pointsPerAnswer(answerId) * session_weight
        data.responseCount += session_weight
      }
    })
    // console.log("questionsOut",questionsOut)
    questionsOut.forEach((question) => {
      const { points, responseCount } = questionMap[question.id]
      if (responseCount > 0) {
        question.averagePoints = points / responseCount
      } else {
        question.averagePoints = null
      }
    })
  }

  // Returns summarized performance requirement data for the given period.
  const performanceRequirements = ({ start, end }, data, alignToShift, callback) => {
    // console.log("in process");
    // console.log("start",start);
    // console.log("end",end);
    // console.log("alignToShift",alignToShift);
    if (typeof alignToShift !== 'boolean') {
      alignToShift = true
    }
    // console.log("in process after if");
    let questions
    let answers

    const processPerformanceRequirements = () => {

      // console.log("in processPerformanceRequirements");

      if (answers !== undefined && questions !== undefined) {

        if (
          !(questions instanceof Array) ||
          !(answers instanceof Array)
        ) {
          console.log('Error: unable to process performance requirements.')
          callback([])
          return
        }
        // console.log("before averagePoints");
        // Note: the questions array is modified by averagePointsPerQuestion().
        averagePointsPerQuestion(questions, answers)
        // console.log("in processPR");
        // console.log(questions);
        // console.log(answers);
        callback(questions)
      }
      // console.log(questions);
    }

    if (alignToShift) {
      start = moment(start).hours(6).toDate();
      end = moment(end).add(1, 'day').hours(6).toDate();
    }
    // console.log("start",start);
    // console.log("end",end);
    // const endDate = end.add(1,'day');
    // console.log("endDate",endDate);
    answerModel({ start, end }, data, app).fetch((err, result) => {

      answers = result
      // console.log("in answerModel");
      // console.log(answers)
      processPerformanceRequirements()
    })

    fetch((err, result) => {
      if (result.length === 0) {
        // console.log("result length is 0");
        callback([])
        return
      }
      // console.log("result",result);
      questions = result.map(({ question_id, perf_name, cc_name, rm_id, cc_id, rm_name,perf_id,priority }) => {
        return {
          id: question_id,
          name: perf_name,
          ccName: cc_name,
          averagePoints: undefined,
          rm_id: rm_id,
          cc_id: cc_id,
          rm_name: rm_name,
          perf_id: perf_id,
          priority: priority
        }
      })
      // console.log("question model");
      // console.log(questions);
      processPerformanceRequirements()
    })
  }

  const getByID = (id, callback) => {
    // console.log('questionId in getByID',id);

    var Question = app.models.questions

    Question.find({
      include: [
        {
          relation: 'critical_control'
        },
        {
          relation: 'performance_requirements'
        }
      ]

    }).then((_question) => {
      // console.log("before for loop");
      //  console.log(_question);
      //  console.log('questionId in question.js',id);
      for (i = 0; i < _question.length; i++) {
        if (_question[i].questionId == id) {
          // console.log("for getById");
          //   console.log(_question[i]);
          var format = { 'question_id': _question[i].questionId, 'question': _question[i].question,
           'answers': [_question[i].option1, _question[i].option2, _question[i].option3, _question[i].option4, _question[i].option5],
            'help_text': _question[i].help_text, 'perf_name': _question[i].performance_requirements().perf_name, 
            'perf_id': _question[i].performance_requirements().id,
          'type_name': _question[i].type_name, 'cc_name': _question[i].critical_control().cc_name }

        }
      }
      // console.log("in getByID function");
      // console.log(format)
      questionCache = format;
      // console.log("in getByID function",questionCache );

      callback(fetchError, questionCache);
    }, (err) => {
      fetchError = err;
      callback(fetchError, questionCache);
    })
  }
  const performance = (questionID, { start, end }, callback) => {
    console.log('start date', start);
    console.log('end date', end);
    const i = dateIterator(start, end, false, 6)
    const result = []


    if (!i.isValid()) {

      callback(result)
      return
    }
    let daysToProcess = 0
    let daysProcessed = 0

    i.forEach((date, index) => {
      daysToProcess++

      const start = new Date(date.valueOf())
      const end = moment(date).add(1, 'days').toDate()
      // console.log(start);
      // console.log(end);


      // console.log("Model" + model);
      // console.log(data);
      const model = answerModel({ start, end }, data, app)
      // console.log('performance...........',questionID);

      model.fetch(questionID, (error, answers) => {

        //  console.log(answers);
        if (!error) {
          const performance = model.performance(answers)
          // console.log(performance);
          result[index] = performance
          daysProcessed++
        }
        if (daysToProcess === daysProcessed) {
          // console.log("in performance function");

          callback(result)
        }
      })
    })
  }

  const colourSummary = (performance) => {
    const result = {
      green: ['-', '-', '-', '-', '-', '-'],
      red: ['-', '-', '-', '-', '-', '-'],
      amber: ['-', '-', '-', '-', '-', '-']
    }

    const removeDayPeriod = true
    const periods = timePeriod(removeDayPeriod)

    let periodIndex = 0
    let performanceIndex = 0
    let green = 0
    let red = 0
    let amber = 0

    performance.slice(0).reverse().forEach((value) => {
      performanceIndex++
      const currentPeriod = periods[periodIndex]
      if (performanceIndex > currentPeriod.days) {
        periodIndex++
      }

      if (value !== null) {
        if (value > 0.75) {
          green++
        }
        else if (value > 0.5) {
          amber++
        }
        else if (value <= 0.5) {
          red++
        }
      }

      if (performanceIndex === currentPeriod.days) {
        result.green[periodIndex] = green
        result.amber[periodIndex] = amber
        result.red[periodIndex] = red
      }
    })

    const currentPeriod = periods[periodIndex]
    if (performanceIndex < currentPeriod.days) {
      result.green[periodIndex] = green
      result.amber[periodIndex] = amber
      result.red[periodIndex] = red
    }
    // console.log("result of amber " + result.amber);
    return result
  }

  return {
    fetch,
    getByID,
    performance,
    performanceRequirements,
    colourSummary
  }
}

module.exports = exports = Question
