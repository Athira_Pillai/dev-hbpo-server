const timePeriod = (removeDay) => {
  if (removeDay === undefined) {
    removeDay = false
  }

  const result = [
    {
      id: 'day',
      label: '1 day',
      short: '1d',
      days: 1
    },
    {
      id: 'week',
      label: '7 days',
      short: '7',
      days: 7
    },
    {
      id: 'halfmonth',
      label: '15 days',
      short: '15',
      days: 15
    },
    {
      id: 'month',
      label: '1 month',
      short: '1m',
      days: 31
    },
    {
      id: 'quarter',
      label: '3 months',
      short: '3m',
      days: 92
    },
    {
      id: 'halfyear',
      label: '6 months',
      short: '6m',
      days: 183
    },
    {
      id: 'year',
      label: '1 year',
      short: '1y',
      days: 365
    }
  ]

  if (removeDay) {
    result.shift()
  }

  return result
}
module.exports = exports = timePeriod
// exports.timePeriod = timePeriod 

