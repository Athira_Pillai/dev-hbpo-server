const moment = require('moment')
const dateIterator = require('./date_iterator')
const answerModel = require('./answer')

const CriticalControl = (criticalControl, dateRange, app) => {
  var critical = criticalControl
  // console.log(critical);
  // console.log(dateRange);
  const calculateDailyPerformance = (averagePoints) => {
    // console.log("averagePoints", averagePoints);
    // console.log(averagePoints);
    if (!averagePoints.isValid()) {
      return undefined
    }

    if (averagePoints.responseCount() === 0) {
      return null
    }

    const dayPoints = averagePoints.day.points.concat(averagePoints.night.points)
    // console.log("dayPoints");
    // console.log(dayPoints);
    const sum = dayPoints.reduce((accumulator, points) => {
      if (points !== null) {
        return accumulator + points
      }
      return accumulator
    })
    // console.log("Sum ", sum);
    return sum / averagePoints.responseCount()
  }

  const performance = (callback) => {
    // console.log("startDate",dateRange.start);
    //   console.log("endDate",dateRange.end);
    const i = dateIterator(dateRange.start, dateRange.end, false, 6)
    
    const result = []

    if (!i.isValid()) {
      callback(result)
      return
    }

    let daysToProcess = 0
    let daysProcessed = 0

    i.forEach((date, index) => {
     
      daysToProcess++

      let totalPoints = 0
      let operatorsAnswered = 0
      const averagePoints = {
        day: null,
        night: null,

        isValid: function () {
          return (this.day !== null && this.night !== null)
        },
        responseCount: function () {
         
          return this.day.responseCount + this.night.responseCount
        }
      }

      const processAveragePoints = () => {

        const performance = calculateDailyPerformance(averagePoints)
        // console.log("pppppppp")
        // console.log(performance)
        if (performance !== undefined) {
          result[index] = performance
          daysProcessed++

          if (daysToProcess === daysProcessed) {
            // console.log(result);
            callback(result)
          }
        }
      }
    
      const start = new Date(date.valueOf())
      const end = moment(date).hours(18).toDate()
   
     
      let model = answerModel({start, end}, critical, app)
      model.fetch((error, answers) => {
        // console.log("answers");
        //   console.log(answers);
        if (!error) {
          averagePoints.day = model.averagePointsPerOperator(answers)
          // console.log("averagePoints.day");
          // console.log(averagePoints.day); 
          processAveragePoints()
        }
      })

      const nightShiftDateRange = {
        start: moment(date).hours(18).toDate(),
        end: moment(date).add(1, 'days').toDate()
      }
      model = answerModel(nightShiftDateRange, critical, app)
      model.fetch((error, answers) => {
        if (!error) {
          averagePoints.night = model.averagePointsPerOperator(answers)
          processAveragePoints()
        }
      })
    })
  }

  const calculateResponseRate = (responseRateInfo, questionCount) => {
    // console.log("Response info", responseRateInfo.responseCount())
    if (!responseRateInfo.isValid()) {
      return undefined
    }
    const day = responseRateInfo.day
    const night = responseRateInfo.night
    const operatorCount = day.operatorCount + night.operatorCount

    if (operatorCount === 0 || questionCount === 0) {
      return null
    }
    // console.log("response accurate", responseRateInfo.responseCount() / (operatorCount * questionCount))
    // if(responseRateInfo.responseCount() / (operatorCount * questionCount) > 1){
    //   console.log('error response exceeds 1', responseRateInfo.responseCount() / (operatorCount * questionCount));
    // }else{
    //   console.log("in else does not exceed by 1", responseRateInfo.responseCount() / (operatorCount * questionCount))
    // }
    return responseRateInfo.responseCount() / (operatorCount * questionCount)
  }

  const responseRate = (questions, callback) => {

  
    const i = dateIterator(dateRange.start, dateRange.end, false, 6)
    const result = []

    if (!i.isValid()) {
      callback(result)
      return
    }

    let daysToProcess = 0
    let daysProcessed = 0

    i.forEach((date, index) => {
      daysToProcess++

      const responseRateInfo = {
        day: null,
        night: null,

        isValid: function () {
          return (this.day !== null && this.night !== null)
        },
        responseCount: function () {
          return this.day.questionsAnswered + this.night.questionsAnswered
        }
      }

      const processResponseRate = () => {
        const responseRate = calculateResponseRate(responseRateInfo, questions.length)
        if (responseRate !== undefined) {
          result[index] = responseRate
          daysProcessed++

          if (daysToProcess === daysProcessed) {
            const {sum, count, day} = result.reduce((acc, value) => {
              if (value !== null && value > 0) {
                acc.sum += value
                acc.count++
                acc.day = value
              }
              return acc
            }, {sum: 0, count: 0, day: null})

            if (count === 0) {
              
              callback({day: day, average: null})
            } else {
              callback({day: day, average: (sum / count)})
            }
          }
        }
      }

      const processAnswers = (period, error, answers) => {
         
        // console.log("answers")
        // console.log(answers)
        const defaultValue = {questionsAnswered: 0, operatorCount: 0}

        if (error || answers.length === 0) {
          responseRateInfo[period] = defaultValue
          processResponseRate()
          return
        }

        let currentOperatorId
        let lastQuestionId
        responseRateInfo[period] = answers.reduce((acc, {questionId, skipped, dontKnow, test_id}) => {

          const questionIndex = questions.findIndex((question) =>  question.question_id === questionId)
          
       
          if (questionIndex === -1) {
            return acc
          }

          if (test_id !== currentOperatorId) {
            currentOperatorId = test_id
            lastQuestionId = null
            acc.operatorCount++
          }

          if (lastQuestionId === questionId) {
            // console.log("Error: duplicate data found");
          } else if (!skipped && !dontKnow) {
            acc.questionsAnswered++
          }
          lastQuestionId = questionId

          return acc
        }, defaultValue)

        processResponseRate()
      }

      const start = new Date(date.valueOf())
      const end = moment(date).hours(18).toDate()

      let model = answerModel({start, end}, critical, app)
      model.fetch((error, answers) => {
      //   console.log("answer in critical_control");
      //  console.log(answers);
        processAnswers('day', error, answers)
      })

      const nightShiftDateRange = {
        start: moment(date).hours(18).toDate(),
        end: moment(date).add(1, 'days').toDate()
      }
      model = answerModel(nightShiftDateRange, critical, app)
      model.fetch((error, answers) => {
        
        processAnswers('night', error, answers)
      })
    })
  }
 
  return {
    performance,
    responseRate
  }
}

module.exports = exports = CriticalControl
