const average = (data) => {
  const {sum, count} = data.reduce((accumulator, value) => {
    if (value !== null && !Number.isNaN(value)) {
      accumulator.sum += value
      accumulator.count++
    }
    return accumulator
  }, {sum: 0, count: 0})

  const result = sum / count
  return Number.isNaN(result) ? undefined : result
}

const simpleMovingAverage = (data, period, truncateLeadingValues) => {
//  console.log("data",data);
//  console.log("period",period);

  if (truncateLeadingValues === undefined) {
    truncateLeadingValues = true
  }
  if (period === undefined) {
    period = 7
  }
  if (
    !(data instanceof Array) ||
    !Number.isInteger(period) ||
    period <= 0 ||
    data.length < period
  ) {
    return undefined
  }

  const {result} = data.reduce((accumulator, value) => {
   
    const {periodData, result} = accumulator
    if (periodData.length < period) {
      periodData.push(value)
    }
    if (periodData.length === period) {
      result.push(average(periodData) || null)
      periodData.shift()
    } else {
      result.push(null)
    }

    return accumulator
  }, {periodData: [], result: []})

  if (truncateLeadingValues) {
    result.splice(0, period - 1)
  }
  
  return result
}

module.exports = exports = simpleMovingAverage
