const moment = require('moment')

const OperatorShift = () => {
  const dateRange = (clientDate) => {
    const date = moment(clientDate)

    let start = moment(date).hours(6).startOf('hour')
    let end = moment(date).hours(18).startOf('hour')

    if (date.hours() < 6) {
      start.subtract(12, 'hours')
      end.subtract(12, 'hours')
    } else if (date.hours() >= 18) {
      start.add(12, 'hours')
      end.add(12, 'hours')
    }

    return {
      start: start.toDate(),
      end: end.toDate()
    }
  }

  return {
    dateRange
  }
}

module.exports = exports = OperatorShift
