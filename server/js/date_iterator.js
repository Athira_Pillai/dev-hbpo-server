const moment = require('moment')

const DateIterator = (start, end, twelveHourInterval, startHour) => {

  const startDate = moment(start, 'YYYY-MM-DD', true).add(1,'day')
  const endDate = moment(end, 'YYYY-MM-DD', true).add(1,'day')

  if (startHour > 0) {
    startDate.hours(startHour)
    endDate.hours(startHour) 
  
  }

  if (twelveHourInterval === true) {
    startDate.hours(6)
    endDate.hours(6)
  }

  isValid = () => {
    return (
      startDate.isValid() &&
      endDate.isValid() &&
      startDate.isBefore(endDate)
    )
  }

  isLast = (date) => {
    return moment(date).add(1, 'days').isSame(endDate)
  }

  forEach = (callback) => {
    let dayIndex = twelveHourInterval ? 0 : undefined
    for (let i = startDate.clone(), index = 0; i.isBefore(endDate); i.add(1, 'days'), index++, dayIndex++) {

    
      callback(i.toDate(), index, dayIndex)

      if (twelveHourInterval) {
        index++
        callback(i.clone().hours(18).toDate(), index, dayIndex)
      }
    }
  }

  return {
    forEach,
    isValid,
    isLast
  }
}

module.exports = exports = DateIterator
