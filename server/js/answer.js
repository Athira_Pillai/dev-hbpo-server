const operatorShift = require('./operator_shift')()
var createWhere = require('mysql-where')
let fetchError;
// var question = require('');
const Answer = (dateRange, data,app) => {
  //   console.log("dateRange in answer")
  //  console.log(dateRange.start)
  //  console.log(dateRange.end)
  // console.log("dateRange in answer",dateRange)
  const fetch = (questionID, callback) => {
    if (typeof questionID === 'function') {
      callback = questionID
      questionID = null
    }
    let searchParameter = {
      clientDate: {
        $gte: dateRange.start,
        $lte: dateRange.end
      }
    }

    if (questionID) {
      searchParameter['questionId'] = questionID
    }

var answer = app.models.question_answers
var result = []
answer.find(
    {
      where: 
      { 
      
        clientDate:{
            between:[dateRange.start,dateRange.end]
        }
      },
      include:[
        {
          relation:'critical_control'
        },
        {
          relation:'sessions'
        } 
      ]   
    } 
    
).then((_answer)=>{ 
  // console.log(data)
        // console.log("QuestAnswers Test", _answer)
    // console.log("SessionDataTest", _answer[0].sessions().session_weight )
    if(searchParameter['questionId']) {
  
      //  console.log("in else")
      for(var i=0;i<_answer.length;i++){
    
          if((_answer[i].questionId == questionID)  && (_answer[i].test_id != null) && (_answer[i].sessions())){
      
            var output = {'primary_key': _answer[i].id, 'dontKnow': _answer[i].dontKnow, 'questionId': _answer[i].questionId, 'skipped': _answer[i].skipped, 'secondsTracked': _answer[i].secondsTracked, 'answerId': _answer[i].answerId, 'date': _answer[i].date, 'clientDate': _answer[i].clientDate, 'test_id': _answer[i].test_id, 'session_weight': _answer[i].sessions().session_weight}
            result.push(output);
          }
          else{
           
          }
      } //end for in if
    }else{
      //  console.log("in if else")
      //  console.log("in else")
      for(var i=0;i<_answer.length;i++){
          if(_answer[i].critical_control()) {
            if((_answer[i].critical_control().id === data) && (_answer[i].test_id != null) && (_answer[i].sessions())){
              var output = {'primary_key': _answer[i].id, 'dontKnow': _answer[i].dontKnow, 'questionId': _answer[i].questionId, 'skipped': _answer[i].skipped, 'secondsTracked': _answer[i].secondsTracked, 'answerId': _answer[i].answerId, 'date': _answer[i].date, 'clientDate': _answer[i].clientDate, 'test_id': _answer[i].test_id, 'session_weight': _answer[i].sessions().session_weight}
               result.push(output);
         }
          }
         
     } //end for in else
    }
        

 questionCache = result;
//  console.log("questionCache Test", questionCache)
 callback(fetchError,questionCache); 
 
},(err)=>{
  fetchError = err
  callback(fetchError,questionCache);  
})

  }

  const pointsPerAnswer = (answerID) => {
    switch (answerID) {
      case 0: return 1.00 // Compliant
      case 1: return 0.66 // Mostly compliant
      case 2: return 0.33 // Somewhat compliant
      case 3: return 0.01
      case 4: return 0.00 // Not compliant
    }
    // console.log('Error: We have an invalid answer ID: ' + answerID)
  }

  const averagePointsPerOperator = (answers) => {
    // console.log("in averagePointsPerOperator",answers);
    const result = {
      responseCount: 0,
      points: []
    }

    if (answers.length === 0) {
      return result
    }

    let operatorPoints = 0
    let questionsAnswered = 0
    let currentOperatorId = answers[0].test_id

    // Calculate the current operator's average and reset the counts
    const storeAndResetPoints = (answer) => {
      if(answer.answerId !== null){
        // console.log('operatorPoints', operatorPoints)
        // console.log('questionsAnswered', questionsAnswered)
  
        if (questionsAnswered > 0) {
          result.points.push(operatorPoints / questionsAnswered)
          result.responseCount++
        } else {
          result.points.push(null)
        }
        operatorPoints = 0
        questionsAnswered = 0
        currentOperatorId = answer.test_id
      }
    }
    // console.log('storeAndResetPoints', result)
    answers.forEach((answer, index) => {
      if (answer.test_id !== currentOperatorId) {
        storeAndResetPoints(answer)
      }

      if (!answer.skipped && !answer.dontKnow) {
        operatorPoints += pointsPerAnswer(answer.answerId)
        questionsAnswered++
      }

      if (index === answers.length - 1) {
        storeAndResetPoints(answer)
      }
    })
    // console.log("averagePointsPerOperator",result);
    return result
  }

  const performance = (answers) => {
    const {sum, questionsAnswered} = answers.reduce((accumulator, {skipped, dontKnow, answerId}) => {
      if (!skipped && !dontKnow) {
        accumulator.sum += pointsPerAnswer(answerId)
        accumulator.questionsAnswered++
      }
      return accumulator
    }, {sum: 0, questionsAnswered: 0})

    if (questionsAnswered > 0) {
      return sum / questionsAnswered
    }
    return null
  }
  
  // const removeDuplicates = (answers) => {
  //   answers.forEach(answer => {
  //     const dateRange = operatorShift.dateRange(answer.clientDate)

  //     // Delete all answers within the current shift with the same
  //     // question ID and session ID, except the most recent answer.
  //     con.query('delete from question_answers where' + createWhere({
  //       questionId: answer.questionId,
  //       sessionID: answer.sessionID,
  //       clientDate: {
  //         $gte: dateRange.start,
  //         $lte: dateRange.end
  //       }
  //     }), (error, result) => {
  //       if (!error && result.deletedCount > 0) {
  //         console.log('Duplicate answers found. Number of answers deleted: ' + result.deletedCount)
  //       }
  //     })
  //   })
  // }

  return {
    fetch,
    // removeDuplicates,
    pointsPerAnswer,
    averagePointsPerOperator,
    performance
  }
}

module.exports = exports = Answer
