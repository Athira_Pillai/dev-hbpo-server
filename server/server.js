'use strict'

var loopback = require('loopback')
var boot = require('loopback-boot')
var path = require('path')
var datasources = require('./datasources')
var models = require(path.resolve('./server/model-config.json'))
var app = module.exports = loopback()
const EventEmitter = require('events');
const bodyParser = require('body-parser');
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.

app.start = function () { 
  // start the web server
  return app.listen(function () { 
    app.emit('started')
   var baseUrl = app.get('url').replace(/\/$/, '')
    console.log('Web server listening at: %s', baseUrl)
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath)
    }
  })
} 

// will change event stream alive time to 4 seconds which is 60 seconds by default.
// app.middleware('routes:before', function (req, res, next) {
//   if (req.path.indexOf('change-stream') !== -1) {
//     res.set('X-Accel-Buffering', 'no');
//     // res.send('no data')
//     res.setTimeout(30000);

//     // console.log('change stream',res )
//     return next();
//   } else {
//     return next();
//   }
// });

app.use(bodyParser.json()); 
boot(app, __dirname, function (err) {
  if (err) throw err

            // start the server if `$ node server.js`
  if (require.main === module) {
        //  app.updateModel();    //to update database
     app.start(); 
    }
}) 
/* Script to update the models to the database uncomment from the boot method */
app.updateModel = function () {
  Object.keys(models).forEach(function (key) {
    if (typeof models[key].dataSource != 'undefined') {
      if (typeof datasources[models[key].dataSource] != 'undefined') {
        // console.log("DSDSDSD"+key)
        if (key == "daily_screening") {
          try {
            app.dataSources[models[key].dataSource].autoupdate(key, function (err) {
              console.log(key)
              if (err) {
                console.log("Error found in column " + err)
              }
              else { 
                console.log('Model ' + key + ' updated');
              }
            });
          } catch (error) {
            console.log("Error in table " + key)
          }
        }
      }
    }
  });
}

// app.use(loopback.static(path.resolve(__dirname,'/')));
// app.use(loopback.static(path.resolve(__dirname,'/mobile')));




