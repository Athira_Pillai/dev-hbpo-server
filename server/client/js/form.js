
    function validation() {
        var newPassword = document.forms["resetForm"]["newPassword"].value;
        var confirmPassword = document.forms["resetForm"]["confirmPassword"].value;
        if(newPassword == confirmPassword) {
          window.alert("Your password has been reset.");
          return true;
        } else {
          window.alert("Passwords entered are not the same.");
          return false;
        }
    }
    function onNewPasswordViewClicked() {
      var newPassword = document.getElementById("newPassword");
      console.log(newPassword);
      if(newPassword.type === "password") {
        newPassword.type = "text";
      } else {
        newPassword.type = "password";
      }
    }
    function onConfirmPasswordViewClicked() {
      var confirmPassword = document.getElementById("confirmPassword");
      if(confirmPassword.type === "password") {
        confirmPassword.type = "text";
      } else {
        confirmPassword.type = "password";
      }
    }
    
