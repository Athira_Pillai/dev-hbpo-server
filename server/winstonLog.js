const {winston,createLogger,transports }= require('winston')
 
var logger = createLogger({
  level: 'debug',
  transports: [
    new transports.Console(),
    new transports.File ({filename: 'logger.log'})
  ]
})

module.exports = logger
