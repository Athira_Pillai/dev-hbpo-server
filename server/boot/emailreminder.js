'use strict'
var schedule = require('node-schedule');
const nodemailer = require('nodemailer');
var logger = require('../winstonLog');
const moment = require('moment');
const bodyParser = require('body-parser')
var checklistArray = [];
const accountSid = 'AC6a7f0ea12491fe8abfde3cdcb2da705c';
const authToken = '66d71feec8700053adb201b55e220ff9';
const client = require('twilio')(accountSid, authToken);
const config = require('./config');
var totalDriverObject = {
    'user': 'Vehicle Inspections',
    'checklistno': 0
};
var totalNurseObject = {
    'user': 'Medical Inspections',
    'checklistno': 0
};
var nursePassObject = {
    'user': 'Medical Inspections',
    'checklistno': 0,
    'result': 'PASS'
}
var nurseFailObject = {
    'user': 'Medical Inspections',
    'checklistno': 0,
    'result': 'FAIL'
}
var driverPassObject = {
    'user': 'Vehicle Inspections',
    'checklistno': 0,
    'result': 'PASS'
}
var driverFailObject = {
    'user': 'Vehicle Inspections',
    'checklistno': 0,
    'result': 'FAIL'
}
var screenerObject  = {
    pass: 0,
    fail:0,
    medium_priority:0,
    low_priority:0,
    total: 0
}
module.exports = function(app) {
    // console.log('starttime', moment(new Date).startOf('day').subtract(1,'days').add(18,'hours').utc().format());
    // console.log('endtime', moment(new Date).endOf('day').subtract(6,'hours').utc().format());
  
    let report = app.models.report;
    report.find({
    }).then((_report) => {
        // console.log('report', _report); 
        _report.forEach((_reportObject) => {
            schedule.scheduleJob(`${_reportObject.time}`, function () {
                // logger.info('Daily CheckList Report')
                // console.log('Send Email For the Day');
                module.exports.email(app,_reportObject.id, null,null);
            })  
        })
    })
    // let report = app.models.report;

     app.post('/checklistFail', async(req, res) => {
        console.log('req body', req.body);
        // console.log('req req.body.jobTask', req.body.jobTask);
        // console.log('req',  req.body.jobFunction.id);
        // console.log('req', req);
        // console.log('checklist api called ....')
        const details = req.body;
        let jobQuestions = app.models.job_questions;
        let flag;
        if ( req.body.jobFunction.id === 9 && req.body.jobTask !== 8) {
             flag = 'drivers';
        } else if ( req.body.jobFunction.id === 19 || req.body.jobFunction.id === 10 || (req.body.jobFunction.id === 9 && req.body.jobTask === 8)) {
             flag = 'nurse'
        }
        // console.log('flag in email reminder..........',flag);
      let jobQuestionsArray =  await jobQuestions.find({
            where:{
              jobFunctionId : req.body.jobFunction.id,
              jobTaskId: req.body.jobTask
            },
              include:{
                  relation: 'email',
                  scope:{
                      where:{
                          sort_id: {neq: -1}
                      }
                  }
              }
          })
          jobQuestionsArray.forEach((_jobQuestion) => {
              console.log('jobQuestions ............', _jobQuestion);
            if(_jobQuestion.email().length > 0){
            module.exports.email(app,null,details,flag, _jobQuestion)
            }
          })
        //   console.log('jobQuestions in email reminder..........',jobQuestionsArray);
        // console.log("checklistFail is called");
        res.send('checklistFail is called');
    });
    
}

module.exports.email = function (app, report_id,details,flag, jobQuestion) {
    var mysqldb = app.dataSources.mysqlDb;
    const session = app.models.sessions;
    const questionAnswers = app.models.question_answers;
    let report = app.models.report;
    let daily_screening = app.models.daily_screening;
    let screenerReportArray = [];
    // console.log('starttime',  moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format());
    // console.log('endtime', moment(new Date).utc().endOf('day').subtract(6,'hours').format());
    // fetchData();
    console.log('starttime',moment(new Date).startOf('day').subtract(1,'day').add(17,'h').utc().format())
    console.log('endtime',moment(new Date).endOf('day').subtract(1,'day').add(17,'h').utc().format())
    app.use(bodyParser.json());
    // sendMessage(); 

    function fetchData(_reportId) {
        console.log('reportId', _reportId);
        if(!details) {
            report.find({
                where: {
                    id: +_reportId
                },
                include: {
                    relation: 'email'
                }
            }).then((_report) => {
                // console.log('report', _report[0]);
                console.log('report', _report[0].email());
                if(report_id == 1){
                driverPass(_report[0]);
                }
                if(_report[0].email().length > 0 && report_id == 2){
                const flag = 'screening'   
                    screeningPass(_report[0], flag);
                } 
            })
        }else {
            sendEmail(null,null,null,null,null,null,null,null,null,jobQuestion,details,flag,null);
        }
     
    }
    function driverPass(_emailObject) {
        mysqldb.connector.execute(`select test_id, count(*) from questions q, question_answers qa
    where q.questionId = qa.questionId and
    questgroup_id = 1 and priority_id = 1 and (answerId = 0 or answerId = 4) 
    and clientDate between '${ moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format()}' and '${moment(new Date).utc().endOf('day').subtract(6,'hours').format()}' group by test_id 
    having count(*) =11;`, (err, _passChecklist) => {
                // console.log('driverPass', _passChecklist)
                // console.log('driverPass', _passChecklist.length);
                if (err) {
                    console.log('err', err);
                } else {
                    // console.log('passChecklist', JSON.stringify(_passChecklist));
                    if (_passChecklist.length > 0) {
                        driverPassObject = {
                            user: 'Vehicle Inspections',
                            checklistno: _passChecklist.length,
                            result: 'PASS'
                        }
                    } else {
                        driverPassObject = {
                            user: 'Vehicle Inspections',
                            checklistno: 0,
                            result: 'PASS'
                        }
                    }

                }
                driverFail(driverPassObject, _emailObject);
            })
    }
    function driverFail(_driverPassObject, _emailObject) {
        mysqldb.connector.execute(`select test_id, count(*) from questions q, question_answers qa
    where q.questionId = qa.questionId and
    questgroup_id =1 and priority_id =1 and (answerId=0 or answerId =4) 
    and clientDate between '${ moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format()}' and '${moment(new Date).utc().endOf('day').subtract(6,'hours').format()}' group by test_id 
    having count(*) <11;`, (err, _failChecklist) => {
                // console.log('driverFail', _failChecklist)
                // console.log('driverFail', _failChecklist.length);
                if (err) {
                    console.log('err', err);
                } else {
                    if (_failChecklist.length > 0) {
                        // console.log('failChecklist', _failChecklist);
                        driverFailObject = {
                            user: 'Vehicle Inspections',
                            checklistno: _failChecklist.length,
                            result: 'FAIL'
                        }
                    } else {
                        // console.log('failChecklist', _failChecklist);
                        driverFailObject = {
                            user: 'Vehicle Inspections',
                            checklistno: 0,
                            result: 'FAIL'
                        }
                    }

                }
                driverTotal(_driverPassObject, driverFailObject, _emailObject);
            })
    }
    function driverTotal(_driverPassObject, _driverFailObject, _emailObject) {
        mysqldb.connector.execute(`select test_id, count(*) from questions q, question_answers qa
    where q.questionId = qa.questionId and
    questgroup_id =1
    and clientDate between '${ moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format()}' and '${moment(new Date).utc().endOf('day').subtract(6,'hours').format()}' group by test_id; `, (err, _totalChecklist) => {
                // console.log('driverTotal', _totalChecklist)
                // console.log('driverTotal', _totalChecklist.length);
                if (err) {
                    console.log('err', err);
                } else {
                    if (_totalChecklist.length > 0) {
                        totalDriverObject = {
                            user: 'Vehicle Inspections',
                            checklistno: _totalChecklist.length
                        }
                    } else {
                        totalDriverObject = {
                            user: 'Vehicle Inspections',
                            checklistno: 0
                        }
                    }
                }
                nursePass(_driverPassObject, _driverFailObject, totalDriverObject, _emailObject);
            })
    }
    function nursePass(_driverPassObject, _driverFailObject, _totalDriverObject, _emailObject) {
        let query = `select test_id, count(*) from question_answers 
        where questionId = 238 and answerId = 0
        and clientDate between '${ moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format()}' and '${moment(new Date).utc().endOf('day').subtract(6,'hours').format()}' group by test_id;`
        console.log('nurse pass',query); 
        mysqldb.connector.execute(query, (err, _passChecklist) => {
                // console.log('nursePass', JSON.stringify(_passChecklist))
                // console.log('nursePass', typeof JSON.stringify(_passChecklist))
                // console.log('nursePass', JSON.stringify(_passChecklist.length));
                if (err) {
                    console.log('err', err);
                } else {
                    if (_passChecklist.length > 0) {
                        // console.log('passChecklist', _passChecklist);
                        nursePassObject = {
                            user: 'Medical Inspections',
                            checklistno: _passChecklist.length,
                            result: 'PASS'
                        }
                    } else {
                        // console.log('passChecklist', _passChecklist);
                        nursePassObject = {
                            user: 'Medical Inspections',
                            checklistno: 0,
                            result: 'PASS'
                        }
                    }
                }
                nurseFail(_driverPassObject, _driverFailObject, _totalDriverObject, nursePassObject, _emailObject);
            })
    }
    function nurseFail(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _emailObject) {
        let query = `select test_id, count(*) from question_answers
        where questionId = 238 and answerId = 1
        and clientDate between '${ moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format()}' and '${moment(new Date).utc().endOf('day').subtract(6,'hours').format()}' group by test_id;`
        console.log('nurse fail',query);
        mysqldb.connector.execute(query, (err, _failChecklist) => {
                // console.log('nurseFail', _failChecklist)
                // console.log('nurseFail', _failChecklist.length);
                if (err) {
                    console.log('err', err);
                } else {
                    if (_failChecklist.length > 0) {
                        nurseFailObject = {
                            user: 'Medical Inspections',
                            checklistno: _failChecklist.length,
                            result: 'FAIL'
                        }
                    } else {
                        nurseFailObject = {
                            user: 'Medical Inspections',
                            checklistno: 0,
                            result: 'FAIL'
                        }
                    }
                }
                nurseTotal(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, nurseFailObject, _emailObject);
            })
    }
    function nurseTotal(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, _emailObject) {
        let query = `select test_id, count(*) from question_answers
        where questionId = 238
        and clientDate between '${ moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format()}' and '${moment(new Date).utc().endOf('day').subtract(6,'hours').format()}' group by test_id; `;
        console.log('nurse total',query);
        mysqldb.connector.execute(query, (err, _totalChecklist) => {
                // console.log('nurseTotal', _totalChecklist)
                // console.log('nurseTotal', _totalChecklist.length);
                if (err) {
                    console.log('err', err);
                } else {
                    if (_totalChecklist.length > 0) {
                        totalNurseObject = {
                            user: 'Medical Inspections',
                            checklistno: _totalChecklist.length
                        }
                    } else {
                        totalNurseObject = {
                            user: 'Medical Inspections',
                            checklistno: 0
                        }
                    } 
                }
                const flag = 'checklistReport'
                getTrips(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, totalNurseObject, _emailObject, flag);
            })
    }

    function getTrips(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, _totalNurseObject, _emailObject, _flag) {
        var trip = app.models.trip;
        trip.find({
            where: {
                and: [
                    { timestamp: { gte:  moment(new Date).utc().startOf('day').subtract(1,'days').add(18,'hours').format() } },
                    { timestamp: { lte: moment(new Date).utc().endOf('day').subtract(6,'hours').format() } }
                ]
            }
        }).then((_trip) => {
            // console.log(_trip);
            // console.log(_trip.length);
            var i = 0;
            var j = 0;
            var k = 0;
            // var scheduleJob = 0;
            var startedTrip = 0;
            var completedTrip = 0;
            var incompleteTrip = 0;
            _trip.forEach((_tripObject) => {
                if (_tripObject.status === "Pending") {
                    i++;
                    startedTrip = i;
                }
                if (_tripObject.status === "Closed") {
                    j++;
                    completedTrip = j;
                }
                if (_tripObject.status === "Open") {
                    k++;
                    incompleteTrip = k;
                }



            })



            storeDailyReportToDB(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, _totalNurseObject, startedTrip,
                incompleteTrip, completedTrip, _emailObject)




            sendEmail(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, _totalNurseObject, startedTrip,
                incompleteTrip, completedTrip, _emailObject, _flag);

        })

    }
    function storeDailyReportToDB(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, _totalNurseObject, startedTrip,
        incompleteTrip, completedTrip, _emailObject) {
        var dailyReport = app.models.daily_report;
        // console.log(_nursePassObject.checklistno)
        dailyReport.create({
            date: new Date(),
            medicalchecklist_pass: _nursePassObject.checklistno,
            medicalchecklist_fail: _nurseFailObject.checklistno,
            vehiclechecklist_pass: _driverPassObject.checklistno,
            vehiclechecklist_fail: _driverFailObject.checklistno,
            medicalchecklist_total: _totalNurseObject.checklistno,
            vehiclechecklist_total: _totalDriverObject.checklistno,
            journey_start: startedTrip,
            journey_incomplete: incompleteTrip,
            journey_complete: completedTrip,
            report_id: _emailObject.id
        }).then((_dailyReport) => {
            // console.log("dailyReport",_dailyReport);
        })
    }

  async function screeningPass(_emailObject, _flag){
    
 
    let sessionArray =  await session.find({
           where:{
               and:[
                   {
                       clientDate: {
                           between:[ moment(new Date).startOf('day').subtract(1,'day').add(17,'h').utc().format(),
                           moment(new Date).endOf('day').subtract(1,'day').add(17,'h').utc().format()
                        ]
                       }
                   }
               ]
           }
       })
       console.log('sessionArray in screeningPass', sessionArray);
       screenerObject.pass = 0; 
       screenerObject.fail = 0;
       screenerObject.medium_priority = 0;
       screenerObject.low_priority = 0;
       screenerObject.total = 0;
    screenerObject.total = sessionArray.length 
    sessionArray.forEach((_session) => {
        if(_session.status === 0){
            screenerObject.fail = screenerObject.fail+1;
        }else if(_session.status === 1){
            screenerObject.pass = screenerObject.pass+1;
        } else if(_session.status === 2) {
            screenerObject.medium_priority = screenerObject.medium_priority + 1;
        }   else if(_session.status === 3) {
            screenerObject.low_priority = screenerObject.low_priority + 1;
        }
    }) 
    // screenerReportArray.push(screenerObject)
      
       console.log('screenerObject in screeningPass', screenerObject);
    //    console.log('screenerReportArray in screeningPass', screenerReportArray);
       sendEmail(null,null,null,null,null,null,null,null,null,_emailObject,null,_flag,screenerObject);
    }
    function sendEmail(_driverPassObject, _driverFailObject, _totalDriverObject, _nursePassObject, _nurseFailObject, _totalNurseObject, _startedTrip,
        _incompleteTrip, _completedTrip, _emailObject, _details,_flag, screenerReport) {
            // console.log('_emailObject............',_emailObject)

        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        _emailObject.email().forEach((_emailId) => {
            nodemailer.createTestAccount((err, account) => {
                // create reusable transporter object using the default SMTP transport
                let transporter = nodemailer.createTransport({
                    // host: 'smtp.ethereal.email',
                    // port: 587,
                    secure: true,
                    service: 'gmail', // true for 465, false for other ports
                    auth: {
                        user: 'hacksstable@gmail.com',
                        pass: 'H@ck$table123'
                    }
                });
                let mailOptions;
         

                if (_flag === 'nurse') {
                    
                    if(_details.jobTask === 8){
                        mailOptions = {
                            from: '"Minetell" <hacksstable@gmail.com>', // sender address
                            to: `${_emailId.email_id}`, // list of receivers
                            subject: 'C19 Self Screening Fail'+' - '+_details.location.location_name, // Subject line
                            text: `C19 Self Screening Fail at ${_details.location.location_name}:   
                                Employee Id : ${_details.nurseId},
                                Employee Name : ${_details.nurseName},
                                Screening Time: ${_details.timestamp},
                                Screening Location: ${_details.location.location_name}` // plain text body
                            // html: '<b>Hello world?</b>' // html body
                        };
                    }else if(_details.jobTask !== 8) {
                        mailOptions = {
                            from: '"Minetell" <hacksstable@gmail.com>', // sender address
                            to: `${_emailId.email_id}`, // list of receivers
                            subject: 'C19 Group Screening Fail'+' - '+_details.location.location_name, // Subject line
                            text: `C19 Group Screening Fail at ${_details.location.location_name}:
                                Employee Id : ${_details.driverId},
                                Employee Name: ${_details.driverName},   
                                Screener Id : ${_details.nurseId},
                                Screener Name : ${_details.nurseName},
                                Screening Time: ${_details.timestamp},
                                Screening Location: ${_details.location.location_name},
                                Additional Comments: ${_details.comment}` // plain text body
                            // html: '<b>Hello world?</b>' // html body
                        };
                    }
                  
                } else if (_flag === 'checklistReport') {

                    if (_emailId.sort_id > 0) {
                        mailOptions = {
                            from: '"Minetell" <hacksstable@gmail.com>', // sender address
                            to: `${_emailId.email_id}`, // list of receivers
                            subject: `${_emailObject.subject} ${moment().format("MMM Do YYYY")}`, // Subject line
                            text: `
                            Date : ${moment().format("MMM Do YYYY")}
         
                            ${_nursePassObject.user}  ${_nursePassObject.result} :  ${_nursePassObject.checklistno}
                            ${_nurseFailObject.user} ${_nurseFailObject.result} :  ${_nurseFailObject.checklistno}
                            ${_totalNurseObject.user} TOTAL :  ${_totalNurseObject.checklistno}
                            
                            ${_driverPassObject.user} ${_driverPassObject.result} :  ${_driverPassObject.checklistno}
                            ${_driverFailObject.user} ${_driverFailObject.result} :  ${_driverFailObject.checklistno}
                            ${_totalDriverObject.user} TOTAL :  ${_totalDriverObject.checklistno}
                            Journeys PENDING : ${_incompleteTrip}
                            Journeys START : ${_startedTrip}
                            Journeys CLOSE : ${_completedTrip}`
                            //   Total number of checklist for today of ${_combineDispatchArray.user} is:  ${_combineDispatchArray.checklistno}`, // plain text body
                            // html: '<b>Hello world?</b>' // html body
                        };
                    }
                }
                else if (_flag === 'drivers') {
                  
                    mailOptions = {
                        from: '"Minetell" <hacksstable@gmail.com>', // sender address
                        to: `${_emailId.email_id}`, // list of receivers
                        subject: 'Checklist Fail', // Subject line
                        text: `Checklist fail details:
                            Driver Id : ${_details.driverId},
                            Driver Name : ${_details.driverName},
                            Checklist Time: ${_details.timestamp},
                            Equipment Id : ${_details.equipmentId},
                            Equipment Name: ${_details.equipmentName}` // plain text body
                        // html: '<b>Hello world?</b>' // html body
                    };
                } else if(_flag === 'screening') {
                    mailOptions = {
                        from: '"Minetell" <hacksstable@gmail.com>', // sender address
                        to: `${_emailId.email_id}`, // list of receivers
                        subject: `${_emailObject.subject} ${moment().format("MMM Do YYYY")}`, // Subject line
                        text: `Daily Screening details:
                        Employee is Fit to Work Screening : ${screenerReport.pass},
                        Self Isolate for 14 Days Screening : ${screenerReport.fail},
                        Self Isolate for 72 Hours : ${screenerReport.medium_priority},
                        Contact HR Manager to Discuss: ${screenerReport.low_priority},
                        Total Screening : ${screenerReport.total}` // plain text body
                        // html: '<b>Hello world?</b>' // html body
                    };
                }
                // setup email data with unicode symbols


                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log("email sent error",error);
                    }
                    console.log('Message sent: %s', info.messageId);
                    // Preview only available when sending through an Ethereal account
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
                });
                // })
                // })
            });
            console.log('flag in sendemail func', _flag)
            if(_flag === 'nurse'){
                saveEmailReport(details,_emailId.email_id)
            }else if (_flag === 'screening'){
                saveDailyScreening(screenerReport, _emailObject)
            }
        })
    }
  
    function saveEmailReport(details, emailId){
        var emailApp = app.models.email_report;
        emailApp.create({
            email: emailId,
            date: new Date(),
            screenerId: details.nurseId,
            employeeId: details.driverId,
            locationId: details.location.id,
            comments: details.comment,
            testId: details.testId
        }).then((_emailReport) => {
            console.log('new email report created.....',_emailReport)
        })
    }
    function saveDailyScreening(_screenerReport, _emailObject){
        var dailyScreening = app.models.daily_screening;
        dailyScreening.create({
            date: new Date(),
            pass: _screenerReport.pass,
            fail: _screenerReport.fail,
            medium_priority: _screenerReport.medium_priority,
            low_priority: _screenerReport.low_priority,
            total_screening: _screenerReport.total, 
            report_id: _emailObject.id
        }).then((_dailyScreening) => { 
            console.log('new daily screening report created', _dailyScreening)
        })
    }
    // function sendMessage() {
    //     client.messages
    //         .create({
    //             body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
    //             from: '+16473608244',
    //             to: '+14167283501'
    //         })
    //         .then(message => console.log(message.sid))
    //         .done();
    // }
        return fetchData(report_id)
}


