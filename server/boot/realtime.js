var es = require('event-stream');
module.exports = function (app) {
  var tripModel = app.models.trip;
  var ccAverage = app.models.cc_average;
  var rqi = app.models.response_quality_index;
  var alt = app.models.alert;
  var riskAverage = app.models.risk_average;

  /*will trigger the event stream and send new updated/created data to client for particular model assigned*/


  tripModel.createChangeStream(function (err, changes) {
    if (err) {
      throw err;
    }
    changes.pipe(es.stringify()).pipe(process.stdout);
  });

  ccAverage.createChangeStream(function (err, changes) {
    if (err) {
      throw err;
    }

    changes.pipe(es.stringify()).pipe(process.stdout);
  });
  // rqi.createChangeStream(function(err, changes) {
  //   changes.pipe(es.stringify()).pipe(process.stdout);
  // });
  alt.createChangeStream(function (err, changes) {
    if (err) {
      throw err;
    }
    changes.pipe(es.stringify()).pipe(process.stdout);
  });

  // riskAverage.createChangeStream(function(err, changes) {
  //   changes.pipe(es.stringify()).pipe(process.stdout);
  // });
}