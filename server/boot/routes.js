

'use strict'
function assert(condition, message) {
  if (!condition) {
    message = message || "Assertion failed";
    if (typeof Error !== "undefined") {
      throw new Error(message);
    }
    throw message; // Fallback 
  }
}
var alerts = false
const bodyParser = require('body-parser')
// var circularJson = require('circular-json')
var _ = require('lodash')
const criticalControl = require('../js/critical_control')
const question = require('../js/question')
const simpleMovingAverage = require('../js/moving_average')
const XLSX = require('xlsx')
const moment = require('moment-timezone')
const config = require('../../config')
var winstonLogger = require('../winstonLog')
const periods = require('../js/time_period')()
const request = require('request-promise');
const admin = require('firebase-admin');
// var db = admin.initializeApp();
const firebaseConfig = require('../../configFirestore')
admin.initializeApp({
  credential: admin.credential.cert(firebaseConfig)
})
const nodemailer = require('nodemailer');
var path = require('path');
var schedule = require('node-schedule');
var url = require('../url');
// db.firestore().settings({ timestampsInSnapshots: true });
var emailReminder = require('./emailreminder');
module.exports = function (app) {
  const accountSid = 'AC6a7f0ea12491fe8abfde3cdcb2da705c';
  const authToken = '66d71feec8700053adb201b55e220ff9';
  // require the Twilio module and create a REST client   
  const client = require('twilio')(accountSid, authToken);
  app.admin = admin;
  // console.log('minetellLink',url.url);
  var minetellLink = url.url;
  var Ccaverage = app.models.cc_average;
  var Riskaverage = app.models.risk_average;
  var Risk = app.models.risks;
  var PerfAverage = app.models.perf_average;
  var cc = app.models.critical_control;
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Header", "Origin, X-Requested-With, Content-Type, Accept");
    // if(!req.secure) { 
    //   console.log("got insecure");
    //   return res.redirect(minetellLink);
    // }
    next();
  });
  app.use(bodyParser.json());

  schedule.scheduleJob('00 58 04 * * *', function () {
    // console.log("node schedular");
    // console.log(moment().format('YYYY-MM-DD'));
    var trip = app.models.trip;
    trip.find({
      where: {
        and: [
          {
            status: 'Pending'
          },
          {
            timestamp: { lte: moment().hours(23).minutes(58).format('YYYY-MM-DD HH:mm:ss') }
          }
        ]
      }
    }).then((_trip) => {
      for (var i = 0; i < _trip.length; i++) {
        trip.updateAll({ id: _trip[i].id }, { status: 'Timed out' }).then((_updatedTrip) => {
                  })
      }
    })
    cc.find({
      where:{
        sortId: {neq: -1}
      },
      fields:['id']
    }).then((_cc) => {
      // console.log('critical control in time scheduler', _cc)
      _cc.forEach(async (criticalControl) => {
      // console.log('criticalControlArray control in time dcheduler', criticalControl.id)
        const options = {
          method: 'GET',
          uri: `${minetellLink}/v1/api/critical-controls/performance/` + `${criticalControl.id}` + `?period=week`
        }
        try {
          await request(options)
        }
        catch (err) {
          console.log(err)
        }
      })
    })
  })
 
  //   const payload = {  
  //   notification: { 
  //     title: 'Risk Performance Alert', 
  //     body: ` Green to Yellow`, 
  //     website: 'minetellLink'  
  //   } 
  // };
  //       admin.messaging().sendToDevice('cUh7txoIQBUd1381KMHjaR:APA91bGauTdgDooF8xPbdjasiyv2KSRqieNzm_g96s3lFIxvrhHWXvQUzVL_pBW9wmrFCLth-TTMReiVehELcjJu2sQNbaziKN3nFKRM1JOSbW7v9TdI56L8BMQUC78cqe1NPSD5YvAB',payload).then(result => {
  //       console.log('push notification',result);  
  //       if(result.results[0].error){ 
  //       //  tokenHandler(change.data().token);  
  //         // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //         //   console.log(_tokenDeleted);
  //         // }) 
  //       } 
  //     }) .catch(reason => {
  //       console.log("reason",reason)  
  //     })
  //use for alert testing **************************   
  // const payload = {  
  //   notification: { 
  //     title: 'Risk Performance Alert', 
  //     body: ` Green to Yellow`, 
  //     website: 'minetellLink'  
  //   } 
  // };
  // pushNotificationToken.get().then((_token)=>{  
  //  _token.forEach((change)=>{   
  //     console.log('change',change.data().token); 
  //     admin.messaging().sendToDevice(`${change.data().token}`,payload).then(result => {
  //       console.log('push notification',result);  
  //       var notify = {'notification':payload,'timeStamp':new Date(),'token':`${change.data().token}`,'userId':change.data().userId,'markAsRead':false}
  //       db.collection("Alert").doc(`${change.data().userId}`).collection('alertPayload').doc(`${moment().format()}`).set(notify).then((_notify)=>{
  //           // console.log("Notification",_notify);       
  //       })
  //       if(result.results[0].error){ 
  //        tokenHandler(change.data().token);  
  //         // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //         //   console.log(_tokenDeleted);
  //         // }) 
  //       } 
  //     }) .catch(reason => {
  //       console.log("reason",reason)  
  //     })
  //   }); 
  // })
  // end code *************************


  // use for notification testing
  // const payload = {  
  //   notification: { 
  //     title: 'Critical Control Assessment', 
  //     body: `Light Vehicle Journey`,
  //     time:`${new Date()}`   
  //   } 
  // };
  // pushNotificationToken.get().then((_token)=>{  
  //   _token.forEach((change)=>{   
  //      console.log('change',change.data().token); 
  //      admin.messaging().sendToDevice(`${change.data().token}`,payload).then(result => {
  //        console.log('push notification',result);  
  //        var notify = {'notification':payload,'timeStamp':new Date(),'token':`${change.data().token}`,'userId':change.data().userId,'markAsRead':false}
  //        db.collection("Notification").doc(`${change.data().userId}`).collection('notificationPayload').doc(`${moment().format()}`).set(notify).then((_notify)=>{
  //            // console.log("Notification",_notify);       
  //        })
  //        if(result.results[0].error){ 
  //         tokenHandler(change.data().token);  
  //          // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //          //   console.log(_tokenDeleted);
  //          // }) 
  //        } 
  //      }) .catch(reason => {
  //        console.log("reason",reason)  
  //      })
  //    }); 
  //  }) 
  // end code **************************


  function tokenHandler(_token) {
    console.log("in tokenHandler");

  }

  app.get('/startreport/:reportId', (req, res) => {
    // console.log(req.params);

    emailReminder.email(app, req.params.reportId)
    // emailReminder;
    // let fetchEmail = email
    // console.log(email.fetchData.fetchData);
    //  var result = emailReminder.fetchData("a");
    //  console.log("result in routes.js", result)
  }, (error) => {

  })


  app.get('/tokenError', (req, res) => {

    console.log("something");
    res.send(_token);

  }, (error) => {

  })
  app.get('/logout/:accessToken', (req, res) => {
    // console.log('in logout');
    // console.log('req',JSON.parse(req.params.accessToken));
    // req.logout();
    var accessToken = JSON.parse(req.params.accessToken) || "";
    if (accessToken) {
      // console.log('found access tocken',  accessToken.id);
      var User = app.models.m_users;
      User.logout(accessToken.id, function (err) {
        if (err) {
          res.sendStatus(404);
        }
        // console.log(err || 'Logged out');
        res.sendStatus(200);
      });
    }
    else {
      // console.log("unable to find the access token");
    }
  })
  app.post('/resetPassword', (req, res) => {
    console.log('in resetPassword');
    console.log(req.body.email);
    var User = app.models.m_users;
    User.resetPassword({
      email: req.body.email
    }
      , (err) => {
        if (err) return res.status(401).send(err);
        res.sendStatus(200);
        // res.render('response', {
        //   title: 'Password reset requested',
        //   content: 'Check your email for further instructions',
        //   redirectTo: '/',
        //   redirectToLinkText: 'Log in'
        // })
      }
    )

  })
  app.post('/send-email', function (req, data, res) {
    console.log("sending email.....", req.body)
    console.log("sending email.....", JSON.stringify(req.body))
    // console.log("user.....", require('./module').user)
    // console.log("pass.....", require('./module').pass)
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'hacksstable@gmail.com',
        pass: 'StableHacks22'
      }
    });
    let mailOptions = {
      from: req.body.from,
      to: req.body.to,//,req.body.to, // list of receivers
      subject: req.body.subject,// req.body.subject, // Subject line
      text: req.body.content, // plain text body
      html: req.body.mytext// html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
      //res.render('index');
    });
  });

  var User = app.models.m_users;
  var accessToken;
  User.on('resetPasswordRequest', function (info) {
    // console.log(info.email);
    console.log(info.accessToken);
    accessToken = info.accessToken.userId;
    var url = `${minetellLink}` + '/reset-password';
    // var html = 'Click <a href="' + url + '?access_token=' +
    //     info.accessToken.id + '">here</a> to reset your password';
    var html = 'Click <a href="' + url + '?accessToken=' +
      info.accessToken.id + '">here</a> to reset your password.';
    nodemailer.createTestAccount(() => {
      let transporter = nodemailer.createTransport({
        // host: 'smtp.ethereal.email',
        // port: 587,
        secure: true,
        service: 'gmail', // true for 465, false for other ports
        auth: {
          user: 'hacksstable@gmail.com',
          pass: 'H@ck$table123'
        }
      });
      let mailOptions = {
        to: info.email,
        from: '"StableHack" <hacksstable@gmail.com>',
        subject: 'Password reset',
        html: html
      }
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      });
    })



  });
  var urlencodedParser = bodyParser.urlencoded({ extended: true });
  app.get('/reset-password', function (req, res) {
    // console.log('accessToken',accessToken);
    // res.writeHead(200, {'Content-Type': 'text/html'});
    res.sendFile(path.resolve('server/client/html/form.html'));

  })
  app.post('/saveNewPassword', urlencodedParser, (req, res) => {
    console.log(req.body)
    console.log('accessToken in saveNewPassword', accessToken)

    User.setPassword(accessToken, req.body.newPassword, (err) => {
      // console.log('set password', res)
      if (err) return res.status(401).send(err);
      // res.writeHead(200, { 'Content-Type': 'text/html' });
      // res.write(`<body><h3>Your password has been reset.<h3></body>`)
      // res.end()
      res.redirect(`${minetellLink}/login`)
    }
    )
  })


  app.get('/login', (req, res) => {
    res.sendFile(path.resolve('prod/index.html'));
  })



  // app.get('/task/:user', (req, res) => {
  //   console.log("in task");
  //   console.log('req', req.params.user);
  //   setTimeout(() => {
  //     console.log("countdown started");
  //     const payload = {
  //       notification: {
  //         title: 'Critical Control Assessment',
  //         body: `Light Vehicle Journey`,
  //         time: `${new Date()}`
  //       }
  //     };
  //     pushNotificationToken.get().then((_token) => {
  //       _token.forEach((change) => {
  //         console.log('change', change.data().token);
  //         if (change.data().userId === Number(req.params.user)) {
  //           admin.messaging().sendToDevice(`${change.data().token}`, payload).then(result => {
  //             console.log('push notification', result);
  //             var notify = { 'notification': payload, 'timeStamp': new Date(), 'token': `${change.data().token}`, 'userId': change.data().userId, 'markAsRead': false }
  //             db.collection("Notification").doc(`${change.data().userId}`).collection('notificationPayload').doc(`${moment().format()}`).set(notify).then((_notify) => {
  //               // console.log("Notification",_notify);       
  //             })
  //             if (result.results[0].error) {
  //               tokenHandler(change.data().token);
  //               // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //               //   console.log(_tokenDeleted);
  //               // }) 
  //             }
  //           }).catch(reason => {
  //             console.log("reason", reason)
  //           })
  //         } else {
  //           console.log('else');
  //         }
  //       });
  //     })
  //   }, 5000)
  //   res.send('time started');
  // })

  // app.get('/dispatch/:user', (req, res) => {
  //   console.log("in dispatch");
  //   console.log('req', JSON.parse(req.params.user));
  //   //  setTimeout(() => {
  //   var driver;
  //   driver = JSON.parse(req.params.user);
  //   console.log(driver.driverId);
  //   console.log("countdown started");
  //   const payload = {
  //     notification: {
  //       title: 'Dispatch',
  //       body: `Journey has been approved`,
  //       time: `${moment().format('LLL')}`
  //     },
  //     data: {
  //       tripId: `${driver.testId}`,
  //       assetId: `${driver.assetId}`,
  //       assetName: `${driver.assetName}`,
  //       route: `${driver.routes}`,
  //       link: `${minetellLink}`
  //     }
  //   };
  //   pushNotificationToken.get().then((_token) => {
  //     _token.forEach((change) => {
  //       //  console.log('change',change.data().token); 
  //       if (change.data().userId === Number(driver.driverId)) {
  //         console.log('if');
  //         admin.messaging().sendToDevice(`${change.data().token}`, payload).then(result => {
  //           console.log('push notification', result);
  //           var notify = { 'notification': payload, 'timeStamp': new Date(), 'token': `${change.data().token}`, 'userId': change.data().userId, 'markAsRead': false }
  //           db.collection("Notification").doc(`${change.data().userId}`).collection('notificationPayload').doc(`${moment().format()}`).set(notify).then((_notify) => {
  //             // console.log("Notification",_notify);       
  //           })
  //           //  if(result.results[0].error){ 
  //           //   tokenHandler(change.data().token);  
  //           //    // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //           //    //   console.log(_tokenDeleted);
  //           //    // }) 
  //           //  } 
  //         }).catch(reason => {
  //           console.log("reason", reason)
  //         })
  //       } else {
  //         console.log('else');
  //       }
  //     });
  //   })
  //   //  },5000)
  //   res.send('time started');
  // })
  const getPeriod = (periodID) => {
    const result = periods.find(({ id }) => (id === periodID))
    if (result === undefined) {
      return periods[0]
    }
    return result
  }
  const getTimezone = (timeZone) => {
    if (!timeZone) {
      winstonLogger.info('Warning: Time zone information is missing from the current session.')
      timeZone = config.defaultClientTimeZone || config.defaultTimeZone
    }
    return timeZone
  }
  var data = ''
  app.post('/api/logs', (req, res) => {
    // console.log("in /api/logs");
    // console.log(req.body.fileName);
    winstonLogger.error(req.body.message + "," + "'fileName': " + req.body.fileName + "," + " 'lineNumber': " + req.body.lineNumber);
  })
  var firebaseCC_nameAverage = [];
  app.get('/v1/api/storefb/critical-controls/:criticalControl', (req, res) => {
    // console.log(req.params.criticalControl);
    var store = true;
    // console.log('in storefb end point');
    criticalControlCal(store, req, res);
  })
  app.get('/v1/api/critical-controls/:criticalControl', (req, res) => {
    // console.log('in criticalControl')
    var store = true;
    criticalControlCal(store, req, res);
    // console.log(criticalControlArray);
  })
  function criticalControlCal(_store, req, res) {
    // console.log('req in criticalControlCal',req);
    var criticalArray = []
    var CriticalControl = app.models.critical_control

    CriticalControl.find().then((_criticalControl) => {
      for (var i = 0; i < _criticalControl.length; i++) {
        criticalArray.push(_criticalControl[i].id);
      }
      if (criticalArray.includes(parseInt(req.params.criticalControl))) {
        // console.log("in if /critical")
        const period = getPeriod(req.query.period)

        // console.log("period", period);
        let dateRange, alignToShift

        // console.log("req.query.period", req.query.period);
        if (req.query.period === 'current') {
          //  console.log("in if /critical current")

          // The current period means the last 24 hours, irrespective of
          // when shifts start and end.

          const end = moment().tz(getTimezone(config.defaultTimeZone)).endOf('day')
          const start = moment().tz(getTimezone(config.defaultTimeZone)).startOf('day')

          // old code ----------------------
          // const end = moment().tz(getTimezone(config.defaultTimeZone))
          // const start = moment(end).subtract(1, 'days')
          dateRange = {
            start: start.toDate(),
            end: end.toDate()
          }
          //--------------------------------
          // console.log('current dateRange',dateRange)
          alignToShift = false
        } else {
          dateRange = getDateRangeFromPeriod(period, config.defaultTimeZone)
          // console.log('dateRange if not current',dateRange)
          alignToShift = true
        }
        // TODO: move this method on to the CC model.
        data = parseInt(req.params.criticalControl)
        const model = question(app, data);
        // console.log(model);
        //  console.log("daterange in routes.js",dateRange)   
        model.performanceRequirements(dateRange, data, alignToShift, (result) => {
          // console.log("in criticalControl result");
          // console.log("result", result);
          calculatePerfAverage(result);

          if (_store == true) {
            winstonLogger.info("storing value to FB")
            var array = [];
            var data = result.filter(function (n) {
              return n.averagePoints != null && !isNaN(n.averagePoints)
            })
            // console.log("data");
            // console.log(data); 
            var sumResult = 0;

            var averageResult = [];

            var k = 0;
            var rawData = [];
            for (var i = 0; i < data.length; i++) {

              sumResult += data[i].averagePoints;
              rawData[k] = { 'cc_name': data[0].ccName, 'rm_id': data[0].rm_id, 'cc_id': data[0].cc_id, 'rm_name': data[0].rm_name }
              k++
              //  averageResult.push({'average':sumResult/data.length,'cc_name':data[i].ccName,'rm_id':data[i].rm_id,'cc_id':data[i].cc_id,'rm_name':data[i].rm_name});
            }
            // console.log(rawData);

            averageResult.push({ 'average': sumResult / data.length, 'ccName': rawData });
            // console.log("averageResult");
            //   console.log(averageResult);
            var averageFilter = averageResult.filter(function (n) {
              return n.average != null && !isNaN(n.average)
            })
            // console.log("averageFilter");
            //             console.log(averageFilter[0]);
            var riskArray = [];
            var dataLength = 0;



            var Sessions = app.models.sessions
            var rqi = app.models.response_quality_index;
            //  Sessions.find().then((_session)=>{
            //   console.log("_session");
            //   console.log(_session);
            //  })
            var dateRangeStart = new Date();
            dateRangeStart.setDate(dateRangeStart.getDate() - 7);
            // console.log("7 days ago:", dateRangeStart)
            Sessions.find({ where: { clientDate: { gt: dateRangeStart } } }).then((_sessions_result) => {
              //  console.log("Sessions_result", _sessions_result)
              var arrayResultSessions = _sessions_result;
              //  console.log("Header Session Data Search:", this.arrayResultSessions)
              var rqiSum = 0
              // var rqiRef = db.firestore().collection('Response Quality Index');
              for (var _i = 0; _i < arrayResultSessions.length; _i++) {
                // console.log("In Header Loop", rqiSum)
                rqiSum += arrayResultSessions[_i].speed_index + arrayResultSessions[_i].pattern_index + arrayResultSessions[_i].time_index + arrayResultSessions[_i].consistency_index;
              }
              var rqiAverage = { 'rqi': (rqiSum / (arrayResultSessions.length * 4)).toFixed(1) }; //4 datapoints per validElement (row) based on 4 dub-indices
              // console.log(rqiAverage)

              // rqiRef.doc('rqi').set(rqiAverage).then((_result) => {
              //   // console.log(_result);
              // })
              rqi.find().then((_rqi) => {
                if (_rqi.length <= 0) {
                  rqi.create({
                    rqi: rqiAverage.rqi
                  }).then((_createRQI) => {
                    res.send({
                      'id': req.params.criticalControl,
                      'performance_requirements': result
                    })
                    // console.log('_createRQI',_createRQI);
                  })
                } else {
                  rqi.upsertWithWhere({ id: _rqi.id }, {
                    rqi: rqiAverage.rqi
                  }).then((_updateRQI) => {
                    res.send({
                      'id': req.params.criticalControl,
                      'performance_requirements': result
                    })
                    // console.log('_updateRQI',_updateRQI);
                  })
                }
              })
            })
          } else {

            res.send({
              'id': req.params.criticalControl,
              'performance_requirements': result
            })
          }


        })

      } else {
        // console.log("in else /critical")
        // console.log('error')
        res.sendStatus(404)
        winstonLogger.error("Error:", res.sendStatus(404))
      }
    })
  }
  function calculatePerfAverage(result) {
    // console.log("result in calculatePerfAverage", result);
    // const end = moment().tz(getTimezone(config.defaultTimeZone))
    //       const start = moment(end).subtract(1, 'days')
    const end = moment().tz(getTimezone(config.defaultTimeZone)).endOf('day')
    const start = moment().tz(getTimezone(config.defaultTimeZone)).startOf('day')

    const dateRange = {
      start: start.toDate(),
      end: end.toDate()
    }
    // console.log('dateRange in calculatePerfAverage func', dateRange)
    result.forEach((_perf_average) => {
      if (_perf_average.averagePoints) {
        PerfAverage.upsertWithWhere({
          perf_id: _perf_average.perf_id,
          date: { between: [dateRange.start, dateRange.end] }
        }, {
          average: _perf_average.averagePoints,
          perf_id: _perf_average.perf_id,
          date: new Date()
        }).then((_currentPerfAvg) => {
          // console.log('current perf average.....', _currentPerfAvg);
        }, (err) => {
          console.log('current perf average error.....', err);
        })
      }

    })
    calculateCriticalAverage(result, dateRange)

  }
  function calculateCriticalAverage(result, dateRange) {
    var sumResult = 0;

    var averageResult = [];

    var k = 0;
    var rawData = [];
    for (var i = 0; i < result.length; i++) {

      sumResult += result[i].averagePoints;
      rawData[0] = { 'cc_name': result[0].ccName, 'rm_id': result[0].rm_id, 'cc_id': result[0].cc_id, 'rm_name': result[0].rm_name }
      k++
      //  averageResult.push({'average':sumResult/data.length,'cc_name':data[i].ccName,'rm_id':data[i].rm_id,'cc_id':data[i].cc_id,'rm_name':data[i].rm_name});
    }
    // console.log(rawData);
    // console.log('sumResult.....', sumResult);
    // console.log('result.length.....', result.length);

    averageResult.push({ 'average': sumResult / result.length, 'ccName': rawData });

    // console.log('averageResult.....', averageResult);
    // console.log('averageResult.....', averageResult[0].ccName[0]);
    if (averageResult[0].average) {
      Ccaverage.upsertWithWhere({
        cc_id: averageResult[0].ccName[0].cc_id,
        date: { between: [dateRange.start, dateRange.end] }
      }, {
        average: averageResult[0].average,
        rm_id: averageResult[0].ccName[0].rm_id,
        rm_name: averageResult[0].ccName[0].rm_name,
        cc_id: averageResult[0].ccName[0].cc_id,
        date: new Date()
      }).then((_currentCriticalAverage) => {
        // console.log('current critical average', _currentCriticalAverage);
        calculateRiskAverage(dateRange);
      })
    }

  }
  function calculateRiskAverage(dateRange) {
    Risk.find(
      {
        include: {
          relation: "critical_control",
          scope: {
            include: {
              relation: "cc_average",
              scope: {
                where: {
                  date: {
                    between: [dateRange.start, dateRange.end]
                  }
                }
              }
            }
          }
        }
      }).then((_risk) => {
        // console.log('risk in calculateRiskAverage func',_risk[1]);
        let riskAverageArray = []
        let criticalAverageArray = []
        let filterCriticalAverageArray = []
        let sum = 0;
        for (var i = 0; i < _risk.length; i++) {
          criticalAverageArray = []
          sum = 0
          // console.log('risk in calculateRiskAverage func',_risk[i]);
          for (var j = 0; j < _risk[i].critical_control().length; j++) {
            if (_risk[i].critical_control()[j].cc_average().length > 0) {
              for (var k = 0; k < _risk[i].critical_control()[j].cc_average().length; k++) {
                // console.log('risk in calculateRiskAverage func', _risk[i].critical_control()[j].cc_average()[k].average);
                criticalAverageArray[j] = parseFloat(_risk[i].critical_control()[j].cc_average()[k].average)
                // console.log('risk in calculateRiskAverage func', criticalAverageArray)
                filterCriticalAverageArray = criticalAverageArray.filter((_criticalAvg) => {
                  // console.log('_criticalAvg in calculateRiskAverage func', _criticalAvg)
                  return _criticalAvg !== null
                })
                // console.log('filterCriticalAverageArray in calculateRiskAverage func', filterCriticalAverageArray.length)
                // console.log('j in calculateRiskAverage func', j)
                filterCriticalAverageArray.forEach((_filterCCAvg) => {
                  sum = (sum + parseFloat(_filterCCAvg)) / filterCriticalAverageArray.length;
                })
                // console.log('sum in calculateRiskAverage func', sum)

                riskAverageArray[i] = ({
                  rm_id: _risk[i].rm_id,
                  average: sum,
                  rm_name: _risk[i].rm_name
                })
              }
            }
          }
        }
        // console.log('riskAverageArray in calculateRiskAverage func',riskAverageArray);
        riskAverageArray = riskAverageArray.filter((_riskAvg) => {
          return _riskAvg !== null;
        })
        Riskaverage.find({
          where: {
            rm_id: riskAverageArray[0].rm_id,
            date: {
              between: [dateRange.start, dateRange.end]
            }
          },
          order: 'date DESC'
        }).then((_latestRiskAverage) => {
          // console.log('latestRiskAverage........................', _latestRiskAverage)
          riskNotify(_latestRiskAverage, riskAverageArray)
        })
        // console.log('riskAverageArray in calculateRiskAverage func', riskAverageArray);

        riskAverageArray.forEach((_riskAverage) => {
          Riskaverage.upsertWithWhere({
            rm_id: _riskAverage.rm_id,
            date: {
              between: [dateRange.start, dateRange.end]
            }
          }, {
            rm_id: _riskAverage.rm_id,
            average: _riskAverage.average,
            date: new Date()
          }).then((_currentRiskAverage) => {
            // console.log('_currentRiskAverage............', _currentRiskAverage)
          })
        })
      })
  }

  function riskNotify(riskNotify, riskAvg) {
    // console.log("riskNotification", riskNotify);
    // console.log("riskAverage", riskAvg);
    // var i = 0;
    const ALert = app.models.alert;

    for (var _j = 0; _j < riskNotify.length; _j++) {

      // console.log("risk average", riskAverage[0].riskAverage); 
      if (+riskNotify[_j].average > 0.9 && (riskAvg[_j].average < 0.9 && riskAvg[_j].average > 0.75)) {
        const body = "Green to Yellow";
        ALert.create({
          timestamp: new Date(),
          rm_name: riskAvg[_j].rm_name,
          riskAverage: riskAvg[_j].average.toFixed(2),
          status: body
        }).then((_alert) => {
          console.log('alert created...............', _alert);
        })
        // sendPushNotification(riskAvg[i].rm_name, body, riskAvg[i].riskAverage.toFixed(2));

        if (alerts == true) {
          sendTextMessage(riskAvg[_j].rm_name, body, riskAvg[_j].average.toFixed(2));
        }

      } else if (+riskNotify[_j].average > 0.9 && riskAvg[_j].average < 0.75) {
        const body = "Green to Red";
        ALert.create({
          timestamp: new Date(),
          rm_name: riskAvg[_j].rm_name,
          riskAverage: riskAvg[_j].average.toFixed(2),
          status: body
        }).then((_alert) => {
          console.log('alert created...............', _alert);
        })
        // sendPushNotification(riskAvg[i].rm_name, body, riskAvg[i].riskAverage.toFixed(2));


        if (alerts == true) {
          sendTextMessage(riskAvg[_j].rm_name, body, riskAvg[_j].average.toFixed(2));
        }

      } else if ((+riskNotify[_j].average < 0.9 && +riskNotify[_j].average > 0.75) && riskAvg[_j].average < 0.75) {
        const body = "Yellow to Red";
        ALert.create({
          timestamp: new Date(),
          rm_name: riskAvg[_j].rm_name,
          riskAverage: riskAvg[_j].average.toFixed(2),
          status: body
        }).then((_alert) => {
          console.log('alert created...............', _alert);
        })
        // sendPushNotification(riskAvg[i].rm_name, body, riskAvg[i].riskAverage.toFixed(2));

        if (alerts == true) {
          sendTextMessage(riskAvg[_j].rm_name, body, riskAvg[_j].average.toFixed(2));
        }

      } else {
        // winstonLogger.info("No Changes")
      }
      // i++
    }

  }

  // function sendPushNotification(_riskName, body, _riskAverage) {
  //   const payload = {
  //     notification: {
  //       title: 'Risk Performance Alert',
  //       body: `${_riskName}: ${body} : ${_riskAverage}`,
  //       website: minetellLink
  //     }
  //   };

  //   pushNotificationToken.get().then((_token) => {
  //     _token.forEach((change) => {
  //       console.log('change', change.data().token);
  //       admin.messaging().sendToDevice(`${change.data().token}`, payload).then(result => {
  //         console.log('push notification', result);
  //         var notify = { 'notification': payload, 'timeStamp': new Date(), 'token': `${change.data().token}`, 'userId': change.data().userId, 'markAsRead': false }
  //         db.firestore().collection("Alert").doc(`${change.data().userId}`).collection('alertPayload').doc(`${moment().format()}`).set(notify).then((_notify) => {
  //           // console.log("Notification",_notify);       
  //         })
  //         if (result.results[0].error) {
  //           tokenHandler(change.data().token);
  //           // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //           //   console.log(_tokenDeleted);
  //           // }) 
  //         }
  //       }).catch(reason => {
  //         console.log("reason", reason)
  //       })
  //     });
  //   })

  //   // pushNotificationToken.get().then((_token)=>{
  //   //   _token.forEach((change)=>{
  //   //      console.log('change',change.data().token);
  //   //      admin.messaging().sendToDevice(`${change.data().token}`,payload).then(result => {

  //   //        console.log('push notification',result);  
  //   //        if(result.results[0].error){
  //   //          tokenHandler(change.data().token);
  //   //          // firebase.messaging().deleteToken(`${change.doc.data().token}`).then((_tokenDeleted) => {
  //   //          //   console.log(_tokenDeleted);
  //   //          // })
  //   //        }
  //   //      }) .catch(reason => {   
  //   //        console.log("reason",reason)  
  //   //      })
  //   //    });
  //   //  })
  // }
  function sendTextMessage(_riskName, body, _riskAverage) {
    client.messages.create(
      {
        to: '+16477702954',
        from: '+16479319542',
        body: `${_riskName}: ${body} : ${_riskAverage}, Go to : ${minetellLink} `,
      },
      (err, message) => {
        winstonLogger.info(message);
      }
    );
  }
  // Note: the use of the timeZone in the following date range functions is
  // important since we want them to be with respect to the client timeZone.
  const getDateRangeFromString = (start, end, timeZone) => {
    timeZone = getTimezone(timeZone);
    return {
      start: moment(start, 'YYYY-MM-DD', true).tz(timeZone).toDate(),
      end: moment(end, 'YYYY-MM-DD', true).tz(timeZone).toDate()
    };
  }
  const getDateRangeFromPeriod = (period, timeZone) => {
    // console.log('in getDateRangeFromPeriod', period)
    timeZone = getTimezone(timeZone)
    const end = moment().tz(timeZone).subtract(1,'day').startOf('day')
    const start = moment(end).subtract(period.days, 'days')
    // console.log('start in getDateRangeFromPeriod', start.toDate())
    return {
      start: start.toDate(),
      end: end.toDate()
    }
  }
  app.get('/v1/api/critical-controls/stats/:criticalControl', (req, res) => {
    // console.log("in stats");

    var criticalArray = []
    var CriticalControl = app.models.critical_control

    CriticalControl.find().then((_criticalControl) => {
      for (var i = 0; i < _criticalControl.length; i++) {
        criticalArray.push(_criticalControl[i].id)
      }
      // console.log(criticalArray);
      // console.log(req.params.criticalControl);

      if (criticalArray.includes(parseInt(req.params.criticalControl))) {
        // console.log("in if /stats")
        const questionModel = question(app, parseInt(req.params.criticalControl))
        questionModel.fetch((error, questions) => {
          if (error) {
            winstonLogger.error(error, res.sendStatus(404))
            res.sendStatus(404)
            return
          }
          const period = getPeriod(req.query.period)
          const dateRange = getDateRangeFromPeriod(period, config.defaultTimeZone)
          //    console.log("dateRange");
          //  console.log(dateRange.start);
          //  console.log(dateRange.end);

          const model = criticalControl(parseInt(req.params.criticalControl), dateRange, app)

          model.responseRate(questions, (result) => {
            // console.log(" in stats/criticalControl");
            //   console.log(result)
            res.send({
              'id': req.params.criticalControl,
              'stats': {
                'response_rates': {
                  'day': result.day,
                  'average': result.average
                }
              }
            })
          })
        })
      } else {
        // console.log("in else /stats")
        res.sendStatus(404)
        winstonLogger.error("Error:", res.sendStatus(404))
      }
    })
  })

  app.get('/v1/api/questions/:questionID', (req, res) => {
    // console.log('in v1/api/questions', req.params.questionID)
    const questionID = req.params.questionID
    // if (isNaN(questionID)) {
    //   res.sendStatus(400)
    //   winstonLogger.error(res.sendStatus(400));
    //   returnc
    // }
    const model = question(app, data)
    model.getByID(questionID, (error, question) => {
      // console.log('question in /api/questions/:questionID',question);
      if (!question) {
        res.sendStatus(404)
        winstonLogger.log("Error:", res.sendStatus(404))
        return
      }

      const period = getPeriod(req.query.period)
      const dateRange = getDateRangeFromPeriod(period, config.defaultTimeZone)

      const movingAveragePeriod = 7
      const extraDays = (movingAveragePeriod - 1)
      dateRange.start = moment(dateRange.start).subtract(extraDays, 'days').toDate()
      // console.log("questionID");
      // console.log(questionID);
      // console.log("dateRange", dateRange);
      model.performance(questionID, dateRange, async (result) => {
        // console.log("in questions/questionID");
        // console.log('performance in questionID',result); 
        question['moving_average'] = await simpleMovingAverage(result, movingAveragePeriod);
        // console.log("perf movingAverage",  question['moving_average']);
        result.splice(0, extraDays);
        question['performance'] = await result;
        // console.log('performance', result);
        question['colour_summary'] = await model.colourSummary(result);
        try {
          await saveCriticalControlMovingAverage(period, question['moving_average'], null, question.perf_id, null);
          res.send(question);
        } catch (err) {
          throw err;
        }
        // console.log("in questions/question",question);
      });
    });
  });

  app.get('/v1/api/critical-controls/performance/:criticalControl', (req, res) => {
    // console.log('in api/critical-controls/performance');
    var criticalArray = []
    var CriticalControl = app.models.critical_control
    CriticalControl.find({
      where:{
        sortId: {neq: -1}
      },
      include: {
        relation: 'risks'
      }
    }).then((_criticalControl) => {
      // console.log('/v1/api/critical-controls/performance/', _criticalControl);
      for (var i = 0; i < _criticalControl.length; i++) {
        criticalArray.push(_criticalControl[i].id)
      }
      // console.log(typeof criticalArray[0]);
      //  console.log('req.params.criticalControl',req.params.criticalControl);
      if (criticalArray.includes(parseInt(req.params.criticalControl))) {
        // console.log("in if /performance")
        const period = getPeriod(req.query.period)
        // console.log("period", period);
        const dateRange = getDateRangeFromPeriod(period, config.defaultTimeZone)
        // console.log("dddddddd");
        const movingAveragePeriod = 7
        const extraDays = (movingAveragePeriod - 1)
        dateRange.start = moment(dateRange.start).subtract(extraDays, 'days').toDate()
        const controller = criticalControl(parseInt(req.params.criticalControl), dateRange, app)
        controller.performance((result) => {
          // console.log("in performance/criticalControl");
          // console.log("result", result);
          const movingAverage = simpleMovingAverage(result, movingAveragePeriod)

          result.splice(0, extraDays)

          saveCriticalControlMovingAverage(period, movingAverage, req.params.criticalControl, null, _criticalControl.filter((_ccId) => {
            return _ccId.id == req.params.criticalControl
          }));
          console.log("movingAverage...........",req.params.criticalControl+' '+movingAverage);
          res.send({
            'id': req.params.criticalControl,
            'performance': result,
            'moving_average': movingAverage
          })
        })
      } else {
        // console.log("in else")
        res.sendStatus(404)
        winstonLogger.error("Error:", res.sendStatus(404));
      }
    })
  })

  // it will fetch 7 days critical average data from db then will add/update moving average for each day.
  function saveCriticalControlMovingAverage(_period, _movingAverage, _criticalControl, _perfId, _rmObject) {
    // console.log("_period in saveCriticalControlMovingAverage", _period);
    // console.log('_rmObject in saveCriticalControlMovingAverage', _rmObject);
    // console.log("_movingAverage..........", _movingAverage);
    // console.log("_criticalControl..........", _criticalControl);
    const end = moment().tz(config.defaultTimeZone).startOf('day')
    const periodArray = [] 

    for (let j = 0; j < _movingAverage.length; j++) {
      const start = moment(end).subtract(_movingAverage.length - j, 'days').format('YYYY-MM-DD')
      periodArray[j] = { date: start, movingAverage: _movingAverage[j] }
    }
    // console.log("periodArray in saveCriticalControlMovingAverage", periodArray);
    const dateRange = getDateRangeFromPeriod(_period, config.defaultTimeZone)
    // console.log("dateRange in saveCriticalControlMovingAverage", dateRange);
    if (_criticalControl) {
      Ccaverage.find({
        where: {
          date: {
            between: [dateRange.start, dateRange.end]
          }
        }
      }).then(async (_ccAvg) => {
        // console.log('ccavg in saveCriticalControlMovingAverage....', _ccAvg);
        await updateOrCreateCriticalMovingAverage(periodArray, _criticalControl, _ccAvg, dateRange, _rmObject)
        // if(_ccAvg[0].date)
      })
    } else {
      // console.log('saveCriticalControlMovingAverage else', dateRange)
      // console.log('_perfId else', _perfId)
      // console.log('_movingAverage else', _movingAverage)
      PerfAverage.find({
        where: {
          date: {
            between: [dateRange.start, dateRange.end]
          },
          perf_id: +_perfId
        }
      }).then(async (_perf) => {
        // console.log('_perf else', _perf)
        await updateOrCreatePerfMovingAverage(periodArray, _perfId, _perf, dateRange)
        // console.log('movingAverageArray else', movingAverageArray)
      })
    }

  }
  function updateOrCreateCriticalMovingAverage(_periodArray, _criticalControl, _ccAvg, _dateRange, _rmObject) {
    let movingAverageArray = []
    for (let i = 0; i < _periodArray.length; i++) {
      if (_ccAvg.length > 0) {
        for (let j = 0; j < _ccAvg.length; j++) {
          if (_periodArray[i].date === moment(_ccAvg[j].date).format('YYYY-MM-DD')) {
            // console.log('condition check in if',periodArray[i].date === moment(_ccAvg[j].date).format('DD-MM-YYYY'))
            // console.log('date in if',periodArray[i].date)
            // console.log('date in ccavg in if',moment(_ccAvg[j].date).format('YYYY-MM-DD') )
            movingAverageArray[i] = {
              cc_id: _criticalControl,
              moving_average: _periodArray[i].movingAverage,
              date: _ccAvg[j].date,
              rm_name: _rmObject[0].risks().rm_name,
              rm_id: _rmObject[0].rm_id
            }
          } else {
            movingAverageArray[i] = {
              cc_id: _criticalControl,
              moving_average: _periodArray[i].movingAverage,
              date: moment(_periodArray[i].date).add(1, 'minute').toDate(),
              rm_name: _rmObject[0].risks().rm_name,
              rm_id: _rmObject[0].rm_id
            }
          }
        }
      } else {
        movingAverageArray[i] = {
          cc_id: _criticalControl,
          moving_average: _periodArray[i].movingAverage,
          date: moment(_periodArray[i].date).add(1, 'minutes').toDate(),
          rm_name: _rmObject[0].risks().rm_name,
          rm_id: _rmObject[0].rm_id
        }
      }

    }
    // console.log('movingAverageArray...................', movingAverageArray)
    movingAverageArray.forEach((_movingAvg) => {
      // console.log(moment(_movingAvg.date).subtract(1,'day').endOf('day'));
      Ccaverage.upsertWithWhere({
        date: { between: [moment(_movingAvg.date).subtract(1, 'day').endOf('day'), moment(_movingAvg.date).endOf('day')] },
        cc_id: _movingAvg.cc_id
      }, {
        moving_average: _movingAvg.moving_average,
        cc_id: _criticalControl,
        date: _movingAvg.date,
        rm_name: _movingAvg.rm_name,
        rm_id: _movingAvg.rm_id
      }).then(() => {

      })
    })
  }
  function updateOrCreatePerfMovingAverage(_periodArray, _perfId, _perf, _dateRange) {
    // console.log('in updateOrCreatePerfMovingAverage');
    // console.log('in updateOrCreatePerfMovingAverage _periodArray',_periodArray);
    // console.log('in updateOrCreatePerfMovingAverage _perf',_perf);
    // console.log('in updateOrCreatePerfMovingAverage _perf',_periodArray[0].date === moment(_perf[0].date).format('YYYY-MM-DD'));

    let movingAverageArray = [];
    let finalArray = []
    for (let i = 0; i < _periodArray.length; i++) {
      if (_perf.length > 0) {
        for (let j = 0; j < _perf.length; j++) {
          // movingAverageArray = []
    // console.log('in updateOrCreatePerfMovingAverage _perf',_perf[i].date);
          if (_periodArray[i].date === moment(_perf[j].date).format('YYYY-MM-DD')) {
            // console.log('condition check in if',periodArray[i].date === moment(_ccAvg[j].date).format('DD-MM-YYYY'))
            // console.log('date in if', _perf[j].date);
            // console.log('date in ccavg in if',moment(_ccAvg[j].date).format('YYYY-MM-DD') )
            movingAverageArray[i] = {
              perf_id: _perfId,
              moving_average: _periodArray[i].movingAverage,
              date: _perf[j].date
            }
            // finalArray.push(movingAverageArray[j])
            // console.log('in if movingAverageArray', movingAverageArray)
          } else {
            // console.log('date in else');
          
          }
        }
        if(!movingAverageArray[i]){
          movingAverageArray[i] = {
            perf_id: _perfId,
            moving_average: _periodArray[i].movingAverage,
            date: moment(_periodArray[i].date).add(1, 'minutes').toDate()
          }
        }
      
        
      } else {
        // console.log('date in else else');
        movingAverageArray[i] = {
          perf_id: _perfId,
          moving_average: _periodArray[i].movingAverage,
          date: moment(new Date).subtract(i + 1, 'day').startOf('day').toDate()
        }
      }
    }
    // console.log('movingAverageArray', movingAverageArray)
    // console.log('finalArray', finalArray)
    movingAverageArray.forEach((_movingAvg) => {
      // console.log('_movingAvg', _movingAvg)
      PerfAverage.upsertWithWhere({
        date: {
          between: [moment(_movingAvg.date).subtract(1, 'days').endOf('day'),
          moment(_movingAvg.date).endOf('day')]
        },
        perf_id: _perfId
      }, {
        moving_average: _movingAvg.moving_average,
        perf_id: _perfId,
        date: _movingAvg.date
      }).then((_perfMovingAverage) => {
        // console.log('_perfMovingAverage response', _perfMovingAverage)
      }, (err) => {
        // console.log('_perfMovingAverage err', err)
      })
    })
  }
  function sheetFrom2DArray(data) {
    var ws = {};
    var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
    for (var R = 0; R != data.length; ++R) {
      for (var C = 0; C != data[R].length; ++C) {
        if (range.s.r > R) {
          range.s.r = R;
        }
        if (range.s.c > C) {
          range.s.c = C;
        }
        if (range.e.r < R) {
          range.e.r = R;
        }
        if (range.e.c < C) {
          range.e.c = C;
        }
        var cell = { v: data[R][C] };
        if (cell.v == null) {
          continue;
        }
        var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

        if (typeof cell.v === 'number') {
          cell.t = 'n';
        } else if (typeof cell.v === 'boolean') {
          cell.t = 'b';
        } else {
          cell.t = 's';
        }

        ws[cell_ref] = cell;
      }
    }
    if (range.s.c < 10000000) {
      ws['!ref'] = XLSX.utils.encode_range(range);
    }
    return ws;
  }
  app.get("/admin/report", function (req, res) {
    // console.log("in /admin/report");
    // console.log(req.query.startDate);
    // console.log(req.query.endDate);
    if (!req.query.startDate || !req.query.endDate) {
      res.sendFile(__dirname + "../../src/app/report/report.component.html");
      return;
    }
    // console.log("cccccccc")
    // console.log(req.query.startDate)
    // console.log(req.query.endDate)
    const startDate = moment(req.query.startDate, "YYYY-MM-DD", true);
    const endDate = moment(req.query.endDate, "YYYY-MM-DD", true);
    // console.log(startDate)
    // console.log(endDate)
    if (!startDate.isValid() || !endDate.isValid()) {
      res.status(400).send("Error: the provided start date or end date is invalid.");
      winstonLogger.error(res.status(400).send("Error: the provided start date or end date is invalid."));
      return;
    }

    if (startDate.isSame(endDate)) {
      res.status(400).send("Error: the start date is the same as the end date.");
      winstonLogger.error(res.status(400).send("Error: the start date is the same as the end date."))
      return;
    }

    if (startDate.isAfter(endDate)) {
      res.status(400).send("Error: the start date is after the end date.");
      winstonLogger.error(res.status(400).send("Error: the start date is after the end date."))
      return;
    }

    const delta = endDate.diff(startDate, "days");
    const allowableDays = 60;
    if (delta > allowableDays) {
      res.status(400).send("Error: the number of days between the start and end dates is too large. The current limit is " + allowableDays + " days.");
      winstonLogger.error(res.status(400).send("Error: the number of days between the start and end dates is too large. The current limit is " + allowableDays + " days."))
      return;
    }

    startDate.hours(6);
    endDate.hours(6);

    function Workbook() {
      if (!(this instanceof Workbook)) {
        return new Workbook();
      }
      this.SheetNames = [];
      this.Sheets = {};
    }
    var wb = new Workbook();

    const cache = {
      questions: [],
      descriptions: [],
      questionIDs: {}
    };

    var dayData = [
      ["Hemlo Traffic Management Plan Performance Analysis"],
      []
    ];

    const answeredText = "Answered";
    const skipText = "Skip";
    const dontKnowText = "IDN";
    const didNotSeeText = "DNS";

    var daysHeaderRow = [null],
      overallPerformanceRow = ["Overall Performance"],
      answeredSummaryRow = [answeredText],
      skippedSummaryRow = [skipText],
      dontKnowSummaryRow = [dontKnowText],
      didNotSeeSummaryRow = [didNotSeeText];

    function createQuestionInfo() {
      return {
        total: 0,
        days: 0
      };
    }
    var perQuestionInfo = [createQuestionInfo()];

    var questionSummaryRows = [["Performance Requirement Data"]];

    // const db = req.app.locals.db;
    var question = [];
    var Questions = app.models.questions;

    Questions.find().then((_questions) => {
      // console.log(_questions);
      for (var j = 0; j < _questions.length; j++) {
        var output = {
          "questionId": _questions[j].questionId,
          "question": _questions[j].question,
          "help_text": _questions[j].help_text,
          'answers': [_questions[j].option1, _questions[j].option2, _questions[j].option3, _questions[j].option4, _questions[j].option5],
          "frequency": _questions[j].frequency,
          "id": _questions[j].id,
          "group_id": _questions[j].group_id,
          "cc_id": _questions[j].cc_id,
          "rm_id": _questions[j].rm_id,
          "perf_id": _questions[j].perf_id
        }
        question.push(output);
      }
      // console.log("questionData", question);
      if (question.length === 0) {
        res.status(404).send("Error: no questions to report on.");
        winstonLogger.error(res.status(404).send("Error: no questions to report on."))
      }

      cache.questions = question;
      cache.descriptions = cache.questions.map((question, index) => {
        //  console.log(cache.questionIDs[question.questionId]);
        var description = question.question;
        cache.questionIDs[question.questionId] = index;
        questionSummaryRows.push([description]);
        perQuestionInfo.push(createQuestionInfo());
        return description;
      });
      // console.log("cache",cache)
      var descriptions = cache.descriptions.slice();
      descriptions.unshift("Operator");
      descriptions.push("Average Performance");

      dayData.push(descriptions);
      winstonLogger.info(" ");
      winstonLogger.info("-------- start report logging --------")
      winstonLogger.info("Timestamp: " + moment().local().format());


      exportDayWorksheets(dayData);
    }, (err) => {
      winstonLogger.error(err)
    })

    // con.query("select * from question_masters order by question_id ASC ",(err,result) =>{
    //   if (result.length === 0) {
    //     res.status(404).send("Error: no questions to report on.");

    //   }

    //   cache.questions = result;
    //   cache.descriptions = cache.questions.map((question, index) => {
    //     var description = question.question;
    //     cache.questionIDs[question.question_id] = index;
    //     questionSummaryRows.push([description]);
    //     perQuestionInfo.push(createQuestionInfo());
    //     return description;
    //   });

    //   var descriptions = cache.descriptions.slice();
    //   descriptions.unshift("Operator");
    //   descriptions.push("Average Performance");

    //   dayData.push(descriptions);
    //   console.log(" ");
    //   console.log("-------- start report logging --------")
    //   console.log("Timestamp: " + moment().local().format());
    //   exportDayWorksheets(dayData);
    // });
    function pointsPerAnswer(answerID) {
      switch (answerID) {
        case 0: return 1.00;   // Compliant
        case 1: return 0.66;   // Mostly compliant
        case 2: return 0.33;   // Somewhat compliant
        case 3: return 0.01;   // Not compliant
        case 4: return 0.00;
      }
      // console.log("Error: We have an invalid answer ID: " + answerID);
    }

    function calculateRowAverage(rowOut) {
      var dataPoints = 0;
      var total = 0;
      rowOut.forEach((value) => {
        if (!isNaN(value)) {
          dataPoints++;
          total += value;
        }
      });
      // console.log ("dataPoint", dataPoints)
      const result = (dataPoints > 0) ? (total / dataPoints) : "N/A";
      // console.log(result);
      rowOut.push(result);
      return result;
    }

    function calculateShiftOperatorData(dataOut, responseData, shiftChangeTime, day) {
      // console.log(typeof responseData, responseData.length );
      var operatorCount = 1;
      var operatorNumber = 1;

      winstonLogger.info(" ");
      winstonLogger.info("Day " + (day + 1));

      const sheetInfo = [];
      cache.questions.forEach((question, index) => {
        sheetInfo[index] = {
          didNotSee: 0,
          skipped: 0,
          dontKnow: 0,
          answered: 0,
          total: 0
        };
      });

      const operatorSheetInfo = {
        total: 0,
        answered: 0
      };

      function calculateDaySummary() {
        const dayNumber = day + 1;
        daysHeaderRow[dayNumber] = `Day ${dayNumber}`;

        // If there's no response data, then that day's report is empty,
        // and so are the summary rows.

        if (responseData.length === 0) {

          overallPerformanceRow[dayNumber] = "N/A";
          answeredSummaryRow[dayNumber] = "N/A";
          skippedSummaryRow[dayNumber] = "N/A";
          dontKnowSummaryRow[dayNumber] = "N/A";
          didNotSeeSummaryRow[dayNumber] = "N/A";

          cache.questions.forEach((question, index) => {
            questionSummaryRows[index + 1][dayNumber] = "N/A";
          });
          return;
        }

        const averageQuestionScoreRow = ["Operator Average"],
          responseRateRow = ["Response Rate"],
          answeredRow = [answeredText],
          skippedRow = [skipText],
          dontKnowRow = [dontKnowText],
          didNotSeeRow = [didNotSeeText];

        var totalAnsweredPercentage = 0;
        var totalSkippedPercentage = 0;
        var totalDontKnowPercentage = 0;
        var totalDidNotSeePercentage = 0;

        // For each question, calculate the average score and response rates.
        cache.questions.forEach((question, index) => {
          //  console.log("question");
          //    console.log(question);
          const info = sheetInfo[index];
          // console.log("info");
          // console.log(info);
          var questionPerf = "N/A";
          if (info.answered > 0) {
            const averageQuestionScore = info.total / info.answered;
            const questionInfo = perQuestionInfo[index + 1];
            questionInfo.total += averageQuestionScore;
            questionInfo.days++;
            questionPerf = averageQuestionScore;
          }
          questionSummaryRows[index + 1][dayNumber] = questionPerf;
          averageQuestionScoreRow.push(questionPerf);

          // Insert empty cells since there's a header cell at the end of the row.
          responseRateRow.push(null);

          const answeredPercentage = info.answered / operatorCount;
          totalAnsweredPercentage += answeredPercentage;
          answeredRow.push(answeredPercentage);

          const skippedPercentage = info.skipped / operatorCount;
          totalSkippedPercentage += skippedPercentage;
          skippedRow.push(skippedPercentage);

          const dontKnowPercentage = info.dontKnow / operatorCount;
          totalDontKnowPercentage += dontKnowPercentage;
          dontKnowRow.push(dontKnowPercentage);

          const didNotSeePercentage = info.didNotSee / operatorCount;
          totalDidNotSeePercentage += didNotSeePercentage;
          didNotSeeRow.push(didNotSeePercentage);
        });

        const info = operatorSheetInfo;
        var ccPerf = "N/A";
        if (info.answered > 0) {
          var ccScore = info.total / info.answered;
          perQuestionInfo[0].total += ccScore;
          perQuestionInfo[0].days++;
          ccPerf = ccScore;
        }
        console.log("ccPerf", ccPerf)

        var ccPerfRow = ["CC Perf", ccPerf];

        overallPerformanceRow[dayNumber] = ccPerf;

        dataOut.push([], averageQuestionScoreRow, [], ccPerfRow, []);

        responseRateRow.push("Daily Response Rates");
        dataOut.push(responseRateRow);

        const questionCount = cache.questions.length;

        // Calculate response rate percentages for the entire day and append to each row.
        const answeredPercentage = totalAnsweredPercentage / questionCount;
        answeredSummaryRow[dayNumber] = answeredPercentage;
        answeredRow.push(answeredPercentage);
        dataOut.push(answeredRow);

        const skippedPercentage = totalSkippedPercentage / questionCount;
        skippedSummaryRow[dayNumber] = skippedPercentage;
        skippedRow.push(skippedPercentage);
        dataOut.push(skippedRow);

        const dontKnowPercentage = totalDontKnowPercentage / questionCount;
        dontKnowSummaryRow[dayNumber] = dontKnowPercentage;
        dontKnowRow.push(dontKnowPercentage);
        dataOut.push(dontKnowRow);

        const didNotSeePercentage = totalDidNotSeePercentage / questionCount;
        didNotSeeSummaryRow[dayNumber] = didNotSeePercentage;
        didNotSeeRow.push(didNotSeePercentage);
        dataOut.push(didNotSeeRow);
      }

      if (responseData.length === 0) {
        calculateDaySummary();
        return;
      }


      const dayShiftOpLabel = "Day Shift Op";
      const nightShiftOpLabel = "Night Shift Op";
      var operatorLabel = dayShiftOpLabel;
      if (shiftChange(responseData[0].clientDate)) {
        operatorLabel = nightShiftOpLabel;
      }

      var sessionID = responseData[0].sessionID;
      var operatorRow = [];
      var questionIter = 0;
      var responseIter = 0;

      // Takes a JS date object and returns whether the date occurs after
      // the shift change time.
      function shiftChange(date) {
        if (moment.isDate(date)) {
          date = moment(date);
        }

        if (operatorLabel === dayShiftOpLabel) {
          const result = date.isSameOrAfter(shiftChangeTime);
          return result;
        }
        return false;
      }

      function incrementResponseIter() {
        const nextIter = responseIter + 1;
        responseIter = (nextIter < responseData.length) ? nextIter : -1;
        return responseIter;
      }

      while (true) {
        if (questionIter >= cache.questions.length) {
          operatorRow.unshift(operatorLabel + operatorNumber);
          const average = calculateRowAverage(operatorRow);
          if (!isNaN(average)) {
            operatorSheetInfo.total += average;
            operatorSheetInfo.answered++;
          }
          dataOut.push(operatorRow);
          // console.log(operatorLabel + operatorNumber, sessionID);

          // Switch to the next operator, and skip over any responses to
          // questions that no longer exist.
          while (responseIter != -1) {
            const response = responseData[responseIter];
            // If the next response's session ID is different from the current one
            // or if we have a shift change, update the current sessionID (i.e. operator).
            if (sessionID != response.sessionID || shiftChange(response.clientDate)) {
              sessionID = response.sessionID;
              break;
            }
            incrementResponseIter();
          }

          if (responseIter == -1) {
            break;
          }
          questionIter = 0;

          operatorCount++;
          operatorNumber++;

          // Contains the cells showing an operator's responses.
          operatorRow = []

          if (shiftChange(responseData[responseIter].clientDate)) {
            operatorLabel = nightShiftOpLabel;
            operatorNumber = 1;
          }
        }


        /// Checks if the question and answer associated with \p response exist.
        function isValidQuestionResponse(response) {
          // console.log("response", response)
          assert(response);

          const questionIndex = cache.questionIDs[response.questionId];
          // console.log ("questionIndex", questionIndex)
          if (questionIndex === undefined) {
            return false;
          }

          const answerID = response.answerId;
          // console.log("answerId",answerID);
          if (answerID === -1) {
            return true;
          }

          const question = cache.questions[questionIndex];
          // console.log("question",question);
          const result = (answerID < question.answers.length);

          return result;
        }

        var question = cache.questions[questionIter];
        var response = (responseIter > -1) ? responseData[responseIter] : null;
        // console.log("response1",response)
        if (response && !isValidQuestionResponse(response)) {
          // Either the question or the answer associated with this response no longer exists,
          // so there's no need to report on it.
          incrementResponseIter();
          continue;
        } else if (!response || question.questionId != response.questionId || response.sessionID != sessionID || shiftChange(response.clientDate)) {
          operatorRow.push(didNotSeeText);
          sheetInfo[questionIter].didNotSee++;
        } else {
          const answerID = response.answerId;
          //  console.log(answerID);
          if (answerID === -1) {
            // The user either skipped or didn't know the answer.
            if (response.skipped) {
              operatorRow.push(skipText);
              sheetInfo[questionIter].skipped++;
            } else {
              console.log("Error: Answer data malformed.");
              // console.log(response);
            }
          } else if (response.dontKnow) {
            operatorRow.push(dontKnowText);
            sheetInfo[questionIter].dontKnow++;
          } else {
            //  console.log(answerID);
            // Write the point value for the given answer.
            var points = pointsPerAnswer(answerID);
            operatorRow.push(points);
            sheetInfo[questionIter].total += points;
            sheetInfo[questionIter].answered++;
          }
          incrementResponseIter();
        }
        questionIter++;
      }

      calculateDaySummary();
    }

    function exportSummaryWorksheet() {
      daysHeaderRow.push("Average Performance");

      // For each question, write out the daily overall score and question score.
      perQuestionInfo.forEach((info, index) => {
        // console.log ("info", info)
        var averageScore = (info.days > 0) ? (info.total / info.days) : "N/A";
        if (index > 0) {
          questionSummaryRows[index].push(averageScore);
        } else {
          overallPerformanceRow.push(averageScore);
        }
      });

      calculateRowAverage(answeredSummaryRow);
      calculateRowAverage(skippedSummaryRow);
      calculateRowAverage(dontKnowSummaryRow);
      calculateRowAverage(didNotSeeSummaryRow);

      var summaryData = [
        ["Summary"],
        daysHeaderRow,
        overallPerformanceRow,
        []
      ];

      summaryData = summaryData.concat(questionSummaryRows);
      summaryData.push([], ["Response Rate"], answeredSummaryRow, skippedSummaryRow, dontKnowSummaryRow, didNotSeeSummaryRow);

      const worksheetName = "Summary";
      wb.SheetNames.push(worksheetName);
      wb.Sheets[worksheetName] = sheetFrom2DArray(summaryData);
    }
    var result = [];
    function getAnswerData(start, end, clientData, callback) {

      // console.log("getAnswerData")
      // console.log(clientData);
      // console.log(start.toDate())
      // console.log(end.toDate())
      var QuestionAnswers = app.models.question_answers;
      QuestionAnswers.find({
        where: {
          clientDate:
          {
            // between:["2018-01-10T00:00:00.000Z","2018-01-15T00:00:00.000Z"]
            gt: start.toDate(), lt: end.toDate()
          }
          // ,
          // and:
          //   {
          //     order:['sessionID','questionId']
          //   }

        }
      }).then((_result) => {
        // console.log("query complete")
        //  console.log(_result)

        for (var i = 0; i < _result.length; i++) {
          result.push({ 'clientDate': _result[i].clientDate, 'answerId': _result[i].answerId, 'questionId': _result[i].questionId })
        }
        //  console.log(result)
        // console.log(clientData)
        callback(clientData, null, result)
      }, (err) => {
        // console.log("in error")
        callback(clientData, err, null);
        winstonLogger.error(callback(clientData, err, null))
      })


      // con.query("select clientDate from question_answers order by sessionID,questionId  where"+createWhere({
      //   $gte : start.toDate(),
      //   $lte : end.toDate()
      // }),(err,result) =>{
      //   callback(clientData,err,result);
      //  // console.log(clientData);
      // });
    }
    function exportDayWorksheets(data) {
      var dayFetchesInProgress = true;
      var allDaysData = [];
      var daysToFetch = 0;
      var daysFetched = 0;
      function saveDayData(dayData, date, day) {
        daysFetched++;
        var dayTemplate = data.slice();
        dayTemplate.splice(1, 0, [`Date: ${date.format("LLL")}`]);
        allDaysData[day] = dayTemplate.concat(dayData);

        if (!dayFetchesInProgress && daysFetched == daysToFetch) {

          exportSummaryWorksheet();
          // All data has been fetched and processed.
          allDaysData.forEach((dayData, index) => {
            var worksheetName = `Day ${index + 1}`;
            wb.SheetNames.push(worksheetName);
            wb.Sheets[worksheetName] = sheetFrom2DArray(dayData);
          });
          const excelFile = "report.xlsx";
          XLSX.writeFile(wb, excelFile);
          // res.send(allDaysData)
          res.download(excelFile);
          winstonLogger.info("--------- end report logging ---------")
          winstonLogger.info(" ");
        }
      }

      for (var i = startDate.clone(), day = 0; i.isBefore(endDate); i.add(1, "days"), day++) {
        daysToFetch++;

        // Day shift
        var shiftStart = i.clone();
        var shiftEnd = i.clone().hours(18);

        var shiftData = { start: shiftStart.clone(), changeTime: shiftEnd.clone() };

        getAnswerData(shiftStart, shiftEnd, { shift: shiftData, day: day }, (clientData, _err, answerData) => {
          if (_err == null) {


            // console.log("clientData.shift");
            // console.log(answerData)
            if (clientData.shift.answerData) {
              answerData = answerData.concat(clientData.shift.answerData);
              // console.log("in if part");
              //  console.log(answerData);
              var dayData = [];
              calculateShiftOperatorData(dayData, answerData, clientData.shift.changeTime, clientData.day);
              saveDayData(dayData, clientData.shift.start, clientData.day);
            } else {
              // console.log("in else part");
              // console.log(answerData);
              clientData.shift.answerData = answerData;

            }
          }
        }
        );

        // Night shift
        shiftStart = shiftEnd.clone();
        shiftEnd = i.clone().add(1, "days");

        getAnswerData(shiftStart, shiftEnd, { shift: shiftData, day: day }, (clientData, _err, answerData) => {
          if (_err == null) {
            if (clientData.shift.answerData) {
              answerData = clientData.shift.answerData.concat(answerData);
              // console.log("in night shift if part");
              // console.log(answerData);
              var dayData = [];
              calculateShiftOperatorData(dayData, answerData, clientData.shift.changeTime, clientData.day);
              saveDayData(dayData, clientData.shift.start, clientData.day);
            } else {
              // console.log("in night shift else part");
              // console.log(answerData);
              clientData.shift.answerData = answerData;
            }
          }
        });
      }
      dayFetchesInProgress = false;
    }
  });
}