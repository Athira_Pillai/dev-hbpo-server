'use strict';

module.exports = function(Jobfunjobtask) {



    Jobfunjobtask.getQuestionBasedOnFunction=async function(JobFunctionId,JobTaskId){

        

    }

    Jobfunjobtask.remoteMethod("getQuestionBasedOnFunction",{
        
        "description":"(custom) method to get question based on job function",
        "createdOn":"1st November",
        "updatedOn":"1st November",
        accepts: [
            {
                arg: 'limit',
                type: 'number'
            },
        ],
        http: {
            path: '/getQuestionBasedOnFunction',
            verb: 'get'
        },
        returns: {
            source: true
        }


    })

};
