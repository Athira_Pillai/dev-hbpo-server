'use strict';
var request = require("request-promise")
const moment = require('moment-timezone')
const config = require('../../config')
module.exports = function (Sessions) {
    /* createdOn: 17th December 2019 
       createdBy: KP,
       param: job_task_id, comments, lat, lng, test_id, status, user_id
    */
    Sessions.sendDataToSharePoint = async  function (instance) {
        // console.log('instance in sendDataToSharePoint', instance)
        var sessionObject = null;
        var questionAnswerObject = []; 
        var questionObject = null;
        var relationType = null
        var dataObject = {};
        var response = null;
        var whereClause = {};
        var generalQuestionAnswers = '';
        // var instance = {
        //     clientDate: '',
        //     session_weight: 0,
        //     comments: '',
        //     test_id: '1492813442479873',
        //     status: 0,
        //     lat: '43.647981699999995',
        //     lng: '-79.36949',
        //     user_id: 4,
        //     job_task_id: 1
        // }
        // console.log(instance)

        /* checking job_task_id if it's 1 means 'Vehicle Fit For Duty', 2 means 'Driver Medical Screening
        and 5 means 'Fit For Duty Self Assessment'' 
        created different whereClause because for medical, vehicle and self assessment checklist
         we don't need asset, users and (asset,m_users) relation respectively.*/
        if (instance.job_task_id === 1) {
            relationType = 'trip_driver_questionnaire';
            whereClause = {
                where: {
                    test_id: instance.test_id,
                },
                include: [{
                    relation: relationType,
                    scope: {
                        include: [{
                            relation: 'm_users',
                            scope: {
                                fields:[
                                    "first_name",
                                    "last_name",
                                    "jobFunction_id",
                                    "email"
                                ],
                                include: {
                                    relation: 'job_function'
                                }
                              
                            }
                        }, {
                            relation: 'asset',
                            scope:{
                                fields: {asset_id:true, plate_number : true, asset_model: true,property: 'DASA'}
                            }
                        }]
                    }
                },
                {
                    relation: 'question_answers',
                    scope: {
                        include:{
                            relation:'questions',
                            scope: {
                                include:{
                                    relation:'multi_lang_question',
                                    scope:{
                                        where: {
                                            language: 'en-US'
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    relation: 'job_task'
                }
                ]
            } 
        } else if (instance.job_task_id === 2) {
            relationType = 'trip_nurse_questionnaire';
            whereClause = {
                where: {
                    test_id: instance.test_id,
                },
                include: [{
                    relation: relationType,
                    scope: {
                        include: [{
                            relation: 'm_users',
                            scope: {
                                fields:[
                                    "first_name",
                                    "last_name",
                                    "jobFunction_id",
                                    "email"
                                ],
                                include: {
                                    relation: 'job_function'
                                }
                              
                            }
                        },{
                            relation: 'users',
                            scope: {  
                                fields:[
                                "first_name",
                                "last_name",
                                "jobFunction_id",
                                "email"
                            ],
                                include: {
                                    relation: 'job_function'
                                },
                            }
                        }]
                    }
                },
                {
                    relation: 'question_answers',
                    scope: {
                        include:{
                            relation:'questions',
                            scope: {
                                include:{
                                    relation:'multi_lang_question',
                                    scope:{
                                        where: {
                                            language: 'en-US'
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    relation: 'job_task'
                }
                ]
            }
        } else if (instance.job_task_id === 5) {
            relationType = 'trip_driverself_questionnaire';
            whereClause = {
                where: {
                    test_id: instance.test_id,
                },
                include: [{
                    relation: relationType,
                    scope: {
                        include: [{
                            relation: 'm_users',
                            scope: {
                                fields:[
                                    "first_name",
                                    "last_name",
                                    "jobFunction_id",
                                    "email"
                                ],
                                include: {
                                    relation: 'job_function'
                                }
                            }
                        }]
                    }
                },
                {
                    relation: 'question_answers',
                    scope: {
                        include:{
                            relation:'questions',
                            scope: {
                                include:{
                                    relation:'multi_lang_question',
                                    scope:{
                                        where: {
                                            language: 'en-US'
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    relation: 'job_task'
                }
                ]
            }
        }
        // console.log('relationType', relationType);
        /* fetching data by test_id, including relation with question_answers and trip table */
      sessionObject =  await Sessions.app.models.Sessions.find(whereClause)
        // console.log('sessionObject', sessionObject[0].question_answers()[0].questions());
        dataObject["assesmentType"] = sessionObject[0].job_task().jobTaskName
        dataObject["testId"] = sessionObject[0].test_id
        dataObject['tripData'] = sessionObject[0][relationType]()[0]


        /* checking if checklist has been done for vehicle checklist then use m_users object to fetch user's data*/
                if (sessionObject[0][relationType]()[0].m_users()) {
            dataObject['user'] = [{
                id: sessionObject[0][relationType]()[0].m_users().id,
                name: sessionObject[0][relationType]()[0].m_users().first_name,
                jobFunction: sessionObject[0][relationType]()[0].m_users().job_function().jobFunctionName
            }]
        }
        /* checking if checklist has been done for medical checklist then use users object to fetch user's data*/
        if (sessionObject[0][relationType]()[0].users()) {
            dataObject['user'] = [ dataObject['user'][0],{
                id: sessionObject[0][relationType]()[0].users().id,
                name: sessionObject[0][relationType]()[0].users().first_name,
                jobFunction: sessionObject[0][relationType]()[0].users().job_function().jobFunctionName
            }]
        }
        /* checking if checklist has been done for vehicl checklist then use asset object to fetch vehicle's data*/
        if (sessionObject[0][relationType]()[0].asset() && instance.job_task_id === 1) {
            dataObject['vehicle'] = sessionObject[0][relationType]()[0].asset().plate_number
        }else {
            dataObject['vehicle'] = 'N/A'
        }
        /* fetching questionId, answer and textanswer from question_answers 
            also it's checking for general questions(priority id = 1) because for general questions we have to display actual text
            of that answer which has been clicked
            for ex, gasoline level has options 'empty', '1/4', '1/2', 'Full' so we have to show 
            gasoline level is 'empty' not answerId 0;
         */
        questionAnswerObject = sessionObject[0].question_answers().map((currentValue) => {
            generalQuestionAnswers = ''
            if(currentValue.questions().priority_id === 1){
                if(currentValue.answerId === 0) {
                    generalQuestionAnswers = currentValue.questions().multi_lang_question()[0].option1
                }
                if(currentValue.answerId === 1) {
                    generalQuestionAnswers = currentValue.questions().multi_lang_question()[0].option2
                }
                if(currentValue.answerId === 2) {
                    generalQuestionAnswers = currentValue.questions().multi_lang_question()[0].option3
                }
                if(currentValue.answerId === 3) {
                    generalQuestionAnswers = currentValue.questions().multi_lang_question()[0].option4
                }
                if(currentValue.answerId === 4) {
                    generalQuestionAnswers = currentValue.questions().multi_lang_question()[0].option5
                }
            }
            return {
                "question": currentValue.questionId,
                "answer": (currentValue.comments || generalQuestionAnswers) ? null : (currentValue.answerId === 1 ? 'No' : 'Yes'),
                "textAnswer":generalQuestionAnswers ? generalQuestionAnswers : currentValue.comments
            }
        })
        dataObject["questionAnswer"] = questionAnswerObject
        if (instance.status === 0) {
            dataObject['status'] = 'Fail'
        } else {
            dataObject['status'] = 'Pass'
        }

        // console.log('dataObject', JSON.stringify(dataObject));
        // console.log('dataObject', dataObject);

        // console.log('questionAnswerObject', questionAnsObject);
        var options = {
            method: 'POST',
            uri: 'https://prod-28.westus.logic.azure.com/workflows/2f4f926432f64aea8328fbffb749f4b3/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=lvEvqSjCt575YHpD08TXVr0m_IgSr517WQVNSZsPg0I',
            body: JSON.stringify(dataObject)
        }
    
        try {
            response = await request(options);
            console.log('response', response);
            // do notjing
        } catch (error) {
            // console.log('error', error);
            throw error
        }
        return "Nothing"
    
    }


    Sessions.remoteMethod('sendDataToSharePoint', {
        description: 'custom remote method to send checklist data on sharepoint',
        createdOn: "17th December, 2019",
        updatedOn: "17th December,2019",
        createdBy: "KP",
        accepts: [],
        http: {
            path: "/sendDataToSharePoint",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })
    // only for torex 
    // Sessions.observe('after save', async function (ctx) {
    //     // console.log('ctx after save session', ctx.instance);
    //     // setTimeout(async() => {
    //     //     // uncomment below function for production
    //     //     await Sessions.sendDataToSharePoint(ctx.instance);
    //     // },20000)
    //     // return;
    // })

    Sessions.totalAndAverageNoOfTest = async function(period){
        // console.log('period',period);
        const end = moment().tz(config.defaultTimeZone).startOf('day').endOf('day').toDate()
        //+period.periodId
        const start = moment(end).subtract(+period.days, 'days').toDate()
        const dateRange = {
            start: start,
            end: end
        }
        let sessionArray = []
        let response = {};
        console.log('dateRange',dateRange);
     sessionArray = await Sessions.find({
            where:{
                clientDate:{between: [dateRange.start, dateRange.end]}
            }
        })
        response['totalNoOfTests'] = sessionArray.length;
        response['avgNoOfTests'] = (sessionArray.length) / +period.days;
        console.log('response',response);
        return response;

    }
    Sessions.remoteMethod('totalAndAverageNoOfTest',{
        descriptions: 'Custom remote method to get total number of answers per periods and average of tests',
        createdOn: '03rd April 2020',
        updatedOn: "03rd April 2020",
        createdBy: "KP",
        accepts: [{ arg: "period", type: 'object' }],
        http: {
            path: "/totalAndAverageNoOfTest",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })
    // Sessions.observe('after save', async function(ctx){
    //     // console.log("Firebase admin",Sessions.app.admin)    
    //     console.log('session ctx', ctx.instance)
    //     let questionAnswer = Sessions.app.models.question_answers;
    //     let questionAnswerArray = [];
    //     let flag = true;
    //     setTimeout(async() => {
    //         questionAnswerArray = await questionAnswer.find({
    //             where:{
    //                 test_id: ctx.instance.test_id
    //             }, 
    //             include:[
    //                 {
    //                     relation:'questions',
    //                     scope:{
    //                         include:{
    //                             relation:'question_group_header'
    //                         }
    //                     }
    //                 }
    //             ]
    //         })
    //     // console.log('questionanswerarray ....................', questionAnswerArray[0].questions);
    //     // console.log('questionanswerarray ....................', questionAnswerArray[0].questions().question_group_header);
    //     // console.log('questionanswerarray ....................', questionAnswerArray[0].questions().question_group_header());
    //         questionAnswerArray.forEach((_questionAnswer) => {
    //             if(_questionAnswer.questions().question_group_header().priority === 'H' && flag){
    //                 sendPushNotification()
    //                     flag =false;
    //             }
    //         })
    //     },3000)
    // })

   function sendPushNotification(){
       var regiteredToken = "e4eomsXWFo2H95uQMyCBLo:APA91bFMBSVziXwMPgeyOsb7EW5v65KFdnQ-by_oAf-YPhBAFTzOgkl_UnxzFRAqgrJ2zrugMsWf2L0VMAscEKV1Mzr4oPeeIukBxaC3LzPqhXT63CwmTglRTPxuXWZv_4w1kJkFVQmD"
    var message = {
        notification: {
          title: '$GOOG up 1.43% on the day',
          body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.'
        },
        token: regiteredToken
      };
      Sessions.app.admin.messaging().send(message).then((response) =>{
        console.log('firebase notification response', response);
      })
      
    }
};
