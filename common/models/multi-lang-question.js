'use strict';

module.exports = function(Multilangquestion) {


    /*

    createdOn: 26th Nov. 2019
    updatedOn: 26th Nov. 2019
    createdBy: The fallen One

    Remote method to transfer questions data to the multi lang question table
    The method takes the unformatted questions from the questions table and add data to the 
    multi-lang-question table.
    
    */

    Multilangquestion.transferData=async function(){

        var questionList=null;
        var MultilangquestionArray=[]
        var enMulti=null;
        var esMulti=null;
        var question=null;
        var option1=null;
        var option2=null;
        var option3=null;
        var option4=null;
        var option5=null;
        var answer=null;
        var addOnlyEnglish=false

        try {
            questionList=await Multilangquestion.app.models.questions.find()
        } catch (error) {
            throw error;
        }



        for(var i=0;i<questionList.length;i++){

            enMulti=new Multilangquestion()
            esMulti=new Multilangquestion()
            question=questionList[i].__data.question;
            option1=questionList[i].__data.option1
            option2=questionList[i].__data.option2
            option3=questionList[i].__data.option3
            option4=questionList[i].__data.option4
            option5=questionList[i].__data.option5
                 

            if(question.indexOf("|")<0){
                addOnlyEnglish=true
                enMulti.question=question
                esMulti.question=question
            }
            else{
                question=question.split("|")
                addOnlyEnglish=false
                esMulti.question=question[0].trim()
                enMulti.question=question[1].trim()
            }


            if(option1.indexOf("|")<0){
                enMulti.option1=option1
                esMulti.option1=option1
            }
            else{
                option1=option1.split("|")
                enMulti.option1=option1[0]
                esMulti.option1=option1[1]
            }


            if(option2.indexOf("|")<0){
                enMulti.option2=option2
                esMulti.option2=option2
            }
            else{
                option2=option2.split("|")
                enMulti.option2=option2[1]
                esMulti.option2=option2[0]
            }

            if(option3.indexOf("|")<0){
                enMulti.option3=option3
                esMulti.option3=option3
            }
            else{
                option3=option3.split("|")
                enMulti.option3=option3[1]
                esMulti.option3=option3[0]
            }

            if(option4.indexOf("|")<0){
                enMulti.option4=option4
                esMulti.option4=option4
            }
            else{
                option4=option4.split("|")
                enMulti.option4=option4[1]
                esMulti.option4=option4[0]
            }

            if(option5.indexOf("|")<0){
                enMulti.option5=option5
                esMulti.option5=option5
            }
            else{
                option5=option5.split("|")
                enMulti.option5=option5[1]
                esMulti.option5=option5[0]
            }


            enMulti.language="en-US"
            esMulti.language="es-ES"

            enMulti.main_question_id=questionList[i].__data.questionId
            esMulti.main_question_id=questionList[i].__data.questionId
       
            MultilangquestionArray.push(enMulti)
            // if(addOnlyEnglish==false){
                MultilangquestionArray.push(esMulti)
            // }
        }

        try {
            console.log("UUUUU")
            MultilangquestionArray=await Multilangquestion.create(MultilangquestionArray) 
            console.log("TTTTT")

        } catch (error) {
            console.log(error)
            throw error;
        }

        return MultilangquestionArray;

    }
    Multilangquestion.remoteMethod("transferData", {
        description:
          "(custom) remote method to add meetup data for all space to meetup",
        createdBy: "The fallen One",
        createdOn: "21st Nov. 2019",
        updatedOn: "21st Nov. 2019",
        accepts: [],
        http: {
          path: "/transferData",
          verb: "get",
          source: "body"
        },
        returns: {
          root: true
        }
      });

};
