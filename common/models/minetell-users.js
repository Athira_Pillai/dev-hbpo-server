'use strict'
var path = require('path');
var senderAddress = 'hacksstable@gmail.com';
var spauth = require('node-sp-auth');
var request = require("request-promise")
const admin = require('firebase-admin');
const firebaseConfig = require('../../configFirestore')
const moment = require('moment-timezone')


module.exports = function (Minetellusers) {

    /*
    createdOn
    updatedOn: 4th Dec 2019
    updateNote: added documentation

    createdBy: The fallen One

    Remote method to import User From Sharepoint
    The remote method uses the following remote methods
    
    i) getListDataFromSharepoint - Get the list data from the sharepoint list. The function returns a JSON Array
    ii) getAuthFromSharePoint - Get the auth headers from sharepoint. The function returns options array

    The method uses the nextUrl variable for pagination 
    */
    Minetellusers.importUserFromSharePoint = async function (res) {
        console.log("Trying to imprt user")

        var SharePointUrl = 'https://torexgold.sharepoint.com/sites/ApplicationsMasterData';

        var SharePointUserName = "minetell@torexgold.com";
        var SharePointPassword = "Torex.2019"
        var reportObject = null

        // console.log(res)
        var requestData = null;
        var afterAuthOptions = null;
        var user = null;
        var userrole = null;
        let userRoleArray = [];
        var userArr = [];
        var isThereNextUrl = true;
        var nextUrl = null;

        try {
            afterAuthOptions = await this.getAuthFromSharePoint();
        } catch (error) {
            throw error
        }

        /* check if there is a nextUrl to enable pagination */
        while (isThereNextUrl == true) {
            /* Get the requestData from the url */
            console.log("Trying to fetch Data")
            try {
                if (!nextUrl) {
                    requestData = await this.getListDataFromSharePoint(afterAuthOptions, SharePointUrl + "/_api/web/lists/getByTitle('Users and Roles MD')/items?$top=200");
                }
                else {
                    requestData = await this.getListDataFromSharePoint(afterAuthOptions, nextUrl);
                }
            } catch (error) {
                throw error;
            }
            /* set the nextUrl. If no nextUrl end the while loop */
            nextUrl = requestData['nextUrl']
            console.log("Checking for next Url")

            if (!nextUrl) {
                isThereNextUrl = false;
                console.log("No next Url found")
            }

            console.log("Is there nextUrl varialbe set to ==", isThereNextUrl)
            requestData = requestData['data']

            console.log("Total Data found====", requestData.length)

            /* create an array of user object from the requestData */
            for (var i = 0; i < requestData.length; i++) {
                // console.log(i)
                user = new Minetellusers()
                userRoleArray = []
                user.first_name = requestData[i].UserName;
                user.email = requestData[i].EmailAddress ? requestData[i].EmailAddress.toLowerCase().trim() : ''
                user.username = String(requestData[i].UserName).replace(' ', '').toLowerCase().trim();
                user.torex_guid = requestData[i].GUID
                if (!user.email) {
                    user.email = user.first_name.replace(/\s/g, '').toLowerCase().trim() + "@minetell.com";
                }
                user.emailVerified = 1;
                // requestData[i].Role = "DRIVER,DOCTOR"
                
                if (requestData[i].Role.includes(',') ? (requestData[i].Role.split(',')[0].trim() == "DRIVER" || (requestData[i].Role.split(',')[1]? requestData[i].Role.split(',')[1].trim() == "DRIVER": 'no value')): requestData[i].Role === 'DRIVER') {
                    userrole = new Minetellusers.app.models.userrole()
                    // console.log('driver if')
                    user.jobFunction_id = 9
                    userrole.jobFunctionId = 9
                    userRoleArray.push(userrole);
                    user.password = "T0r3xG0ld2o2o",
                        user.isPasswordChanged = 1
                }

                if (requestData[i].Role.includes(',') ? ((requestData[i].Role.split(',')[1]? requestData[i].Role.split(',')[1].trim() == "DOCTOR" :'no value') || requestData[i].Role.split(',')[0].trim() == "DOCTOR"): requestData[i].Role === 'DOCTOR') {
                    // console.log('doctor if')
                    userrole = new Minetellusers.app.models.userrole()
                    user.jobFunction_id = 19
                    userrole.jobFunctionId = 19
                    userRoleArray.push(userrole);
                    user.password = "T0r3xG0ld2o2o",
                        user.isPasswordChanged = 1
                }

                if (requestData[i].Role.includes(',') ? (requestData[i].Role.split(',')[0].trim() == "DISPATCH" || (requestData[i].Role.split(',')[1]? requestData[i].Role.split(',')[1].trim() == "DISPATCH": 'no value')): requestData[i].Role === 'DISPATCH') {
                    user.jobFunction_id = 7
                    userrole = new Minetellusers.app.models.userrole()
                    userrole.jobFunctionId = 7
                    userRoleArray.push(userrole);
                    user.password = "12345",
                        user.isPasswordChanged = 0
                }
                // console.log('user object', user)
                // console.log('userRoleArray object', userRoleArray)
                try {
                    await Minetellusers.upsertWithWhere({
                        torex_guid: user.torex_guid
                    }, user).then((data) => {
                        if (data) {
                            // console.log('userRoleArray object after', userRoleArray)
                            userRoleArray.forEach(async (_userRoleObject) => {
                                // console.log('_userRoleObject object after', _userRoleObject)

                                await Minetellusers.app.models.userrole.upsertWithWhere({
                                    userId: data.id,
                                    jobFunctionId: _userRoleObject.jobFunctionId
                                }, {
                                    userId: data.id,
                                    jobFunctionId: _userRoleObject.jobFunctionId
                                }, function (err, userrole) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    // console.log('userrole created',userrole);
                                })
                            })
                        }
                    })


                } catch (error) {
                    console.log(error)
                    //do nothing
                    // throw error
                }
                // userArr.push(user);
            }

        }

        return {
            "status": "ok"
        }



    }


    Minetellusers.remoteMethod("importUserFromSharePoint", {
        description: "(custom) remote method to import users from slack",
        createdOn: "13th November, 2019",
        updatedOn: "13th November,2019",
        createdBy: "The fallen One",
        accepts: [
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        http: {
            path: "/importUserFromSharePoint",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })



    /*
    createdOn
    updatedOn: 4th Dec 2019
    updatedOn: 9th Dec 2019
    updateNote: added documentation
    updateNode[9th Dec 2019] - removed bulk create (sad) and added a for loop to do upsertWithWhere
    createdBy: The fallen One

    Remote method to import Light vehicles data  From Sharepoint. The remote method sets the assetId to 2 since all 
    of the vehicles are light vehicles
    The remote method uses the following remote methods
    
    i) getListDataFromSharepoint - Get the list data from the sharepoint list. The function returns a JSON Array
    ii) getAuthFromSharePoint - Get the auth headers from sharepoint. The function returns options array

    The method uses the nextUrl variable for pagination 
    */
    Minetellusers.importLightVehiclesFromSharePoint = async function () {
        var SharePointUrl = 'https://torexgold.sharepoint.com/sites/ApplicationsMasterData';

        var SharePointUserName = "minetell@torexgold.com";
        var SharePointPassword = "Torex.2019"

        var requestData = null;
        var afterAuthOptions = null;
        var singleAsset = null;
        var assetArr = [];
        var nextUrl = null;
        var isNextUrl = true;

        // get auth options from sharepoint 
        try {
            afterAuthOptions = await this.getAuthFromSharePoint();

        } catch (error) {
            throw error;
        }

        /* Check if the list contains more data(pagination) if it doesn't end the loop */
        while (isNextUrl == true) {

            /* Request the data from sharepont */
            try {

                if (nextUrl) {
                    requestData = await this.getListDataFromSharePoint(afterAuthOptions, nextUrl);
                }
                else {
                    requestData = await this.getListDataFromSharePoint(afterAuthOptions, SharePointUrl + "/_api/web/lists/getByTitle('Light Vehicles Master Data')/items?$top=200");
                }
            } catch (error) {
                throw error;
            }

            /* Set the next url from the requestData. if no nextUrl from end the while loop */
            nextUrl = requestData['nextUrl']
            requestData = requestData['data'];

            if (!nextUrl) {
                isNextUrl = false;
            }

            /* Create asset object from sharepoint and push it to the asset array */
            for (var i = 0; i < requestData.length; i++) {
                singleAsset = new Minetellusers.app.models.asset()
                if (String(requestData[i]["OData__x0023_ECO_x007c_ID_x0023_"]).includes('-')) {
                    singleAsset.asset_id = String(requestData[i]["OData__x0023_ECO_x007c_ID_x0023_"]).replace('-', '');
                } else {
                    singleAsset.asset_id = requestData[i]["OData__x0023_ECO_x007c_ID_x0023_"]
                }
                singleAsset.plate_number = requestData[i]["Plates_x007c_Placas"]
                singleAsset.color = requestData[i]["kvki"]
                singleAsset.asset_model = requestData[i]['ifyl']
                singleAsset.property = requestData[i]['gxwh']
                singleAsset.model = requestData[i]['OData__x006e_916'] || null
                singleAsset.assetTypeId = 2

                if (!singleAsset.plate_number) {
                    singleAsset.plate_number = singleAsset.asset_id;
                }

                assetArr.push(singleAsset)

                try {
                    Minetellusers.app.models.asset.upsertWithWhere(
                        { asset_id: singleAsset.id },
                        singleAsset
                    )
                } catch (error) {
                    console.log(error)
                    // do nothing
                }
            }
        }

        return {
            status: "ok"
        };
    }

    Minetellusers.remoteMethod("importLightVehiclesFromSharePoint", {
        description: "(custom) remote method to import light vehicles from sharepoints",
        createdOn: "14th November,2019",
        updatedOn: "14th November,2019",
        createdBy: "The Fallen One",
        accepts: [],
        http: {
            path: "/importLightVehiclesFromSharePoint",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }

    })


    Minetellusers.getListDataFromSharePoint = async function (afterAuthOptions, url) {
        var SharePointUrl = 'https://torexgold.sharepoint.com/sites/ApplicationsMasterData';
        var singleElement = null;
        var requestData = null;
        var nextUrl = null;
        var headers = afterAuthOptions.headers;
        headers['Accept'] = 'application/json;odata=verbose';

        var requestHeaders = {
            // url: SharePointUrl+"/_api/web/lists/getByTitle(listName)/items",
            url: url,
            headers: headers,
            json: true
        }

        try {
            requestData = await request.get(requestHeaders);
        } catch (error) {
            throw error;
        }

        // console.log(requestData)
        if (requestData.d && requestData.d.results) {
            if (requestData.d.__next) {
                console.log(requestData.d.__next)

                nextUrl = requestData.d.__next;
            }

            requestData = requestData.d.results
        }
        else {
            var error = new Error("Unable to get result")
            error.statusCode = 422
            throw error;
        }

        if (Array.isArray(requestData) == false) {
            requestData = [requestData]
        }


        return {
            data: requestData,
            nextUrl: nextUrl
        }

    }

    Minetellusers.remoteMethod("getListDataFromSharePoint", {
        description: "(custom) remote method to get list data from sharepoint",
        createdOn: "13th Nov,2019",
        updatedOn: "13th Nov,2019",
        createdBy: "The fallen One",
        accepts: [
            { arg: "url", type: "string" },
            { arg: "afterAuthOptions", type: "string" }
        ],
        http: {
            path: "/getListDataFromSharePoint",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })

    Minetellusers.getAuthFromSharePoint = async function () {
        var SharePointUrl = 'https://torexgold.sharepoint.com/sites/ApplicationsMasterData';
        var SharePointUserName = "minetell@torexgold.com";
        var SharePointPassword = "Torex.2019";
        var options = null;
        try {
            options = await spauth.getAuth(SharePointUrl, {
                username: SharePointUserName,
                password: SharePointPassword
            })
        } catch (error) {
            throw error;
        }

        return options


    }

    /*
    */

    Minetellusers.importQuestionsFromSharePoint = async function (url, type) {
        console.log("Url===", url)
        console.log("Type===", type)
        var SharePointUrl = 'https://torexgold.sharepoint.com/sites/ApplicationsMasterData';

        var SharePointUserName = "minetell@torexgold.com";
        var SharePointPassword = "Torex.2019"

        var requestData = null;
        var afterAuthOptions = null;
        var multi_lang_question = null;
        var question = null;
        var multiQuestionArr = [];
        var questionArr = [];
        var isThereNextUrl = true;
        var nextUrl = null;
        var _question = null;
        try {
            afterAuthOptions = await this.getAuthFromSharePoint();
        } catch (error) {
            throw error
        }
        // console.log("isThereNextUrl=",isThereNextUrl)
        while (isThereNextUrl == true) {
            try {
                console.log("nextUrl=", nextUrl)
                if (!nextUrl) {
                    requestData = await this.getListDataFromSharePoint(afterAuthOptions, SharePointUrl + url);
                    nextUrl = SharePointUrl + "/_api/web/lists/getByTitle('Self Check Questions')/items?$top=200"
                }
                else {
                    requestData = await this.getListDataFromSharePoint(afterAuthOptions, nextUrl);
                }
            } catch (error) {
                throw error;
            }
            nextUrl = requestData['nextUrl']

            if (!nextUrl) {
                isThereNextUrl = false;
            }
            requestData = requestData['data']

            let questionIdArray = []
            // let finalArray = [];



            requestData.forEach((_data) => {
                questionIdArray.push(_data.CodeComponent || _data.Title)
            })

            try {
                _question = await Minetellusers.app.models.multi_lang_question.find({
                    where: {
                        main_question_id: { inq: questionIdArray }
                    }
                })
            } catch (error) {
                throw error
            }

            // console.log('_question',_question)


            // _question.forEach((_questionObject) => {
            //     finalArray.push(_questionObject.main_question_id);
            // })
            for (var i = 0; i < requestData.length; i++) {
                question = new Minetellusers.app.models.questions()
                multi_lang_question = new Minetellusers.app.models.multi_lang_question();
                if (type == "light_vehicle") {
                    question.questionId = requestData[i].CodeComponent;
                    question.question = requestData[i].ComponentName;
                    question.cc_id = 8;
                    question.rm_id = 2;
                    if (requestData[i].CriteriaifFail === 'Medium' || requestData[i].CriteriaifFail === 'Medio') {
                        question.questgroup_id = 2;
                        question.priority_id = 3
                    }
                    else if (requestData[i].CriteriaifFail === 'High' || requestData[i].CriteriaifFail === 'Alto') {
                        question.questgroup_id = 3
                        question.priority_id = 2
                    }
                    else {
                        question.questgroup_id = 1
                        question.priority_id = 1
                    }
                    question.type_of_input = requestData[i].QuestionType
                    questionArr.push(question);

                    // console.log(requestData[i])
                    // if (finalArray.find((element) => {
                    //     return element === requestData[i].CodeComponent ? true : false
                    // })) {
                    multi_lang_question.question = requestData[i].ComponentName;
                    multi_lang_question.main_question_id = requestData[i].CodeComponent;
                    multi_lang_question.option3 = 'NAN';
                    multi_lang_question.option4 = 'NAN';
                    multi_lang_question.option5 = 'NAN';
                    if (requestData[i].Language === 'ENG') {

                        if (!requestData[i].fsaa && !requestData[i].QuestionType) {
                            multi_lang_question.option1 = 'Yes';
                            multi_lang_question.option2 = 'No';
                        }
                        else {
                            multi_lang_question.option1 = requestData[i].fsaa ? requestData[i].fsaa : "NAN"
                            multi_lang_question.option2 = requestData[i].fsaa ? requestData[i].yskd : "NAN"
                        }

                        multi_lang_question.language = 'en-US';
                    } else if (requestData[i].Language === 'SPA') {
                        if (!requestData[i].fsaa && !requestData[i].QuestionType) {
                            multi_lang_question.option1 = 'Si';
                            multi_lang_question.option2 = 'No';
                        }
                        else {
                            multi_lang_question.option1 = requestData[i].fsaa ? requestData[i].fsaa : "NAN"
                            multi_lang_question.option2 = requestData[i].fsaa ? requestData[i].yskd : "NAN"
                        }
                        multi_lang_question.language = 'es-ES';
                    }



                    multi_lang_question.option3 = requestData[i].fsaa ? requestData[i].z1w4 : "NAN"
                    multi_lang_question.option4 = requestData[i].fsaa ? requestData[i].khnv : "NAN"
                    multi_lang_question.option5 = requestData[i].fsaa ? requestData[i].g8k6 : "NAN"

                    multiQuestionArr.push(multi_lang_question);
                    // } else {
                    //     // console.log('data already exists');
                    // }
                    // console.log('multiQuestionArr..........', multiQuestionArr);
                    // self checkout
                } else if (type == "self_checkout") {
                    question.cc_id = 13;
                    question.rm_id = 2;
                    question.questionId = requestData[i].Code;
                    question.question = requestData[i].Question;
                    question.correct_answer = requestData[i].Result
                    if (requestData[i].Action === 'Medium' || requestData[i].Action === 'Medio') {
                        question.questgroup_id = 4;
                        question.priority_id = 10
                    }
                    if (requestData[i].Action === 'High' || requestData[i].Action === 'Alto') {
                        question.questgroup_id = 5
                        question.priority_id = 11
                    }
                    questionArr.push(question);
                    // if (finalArray.find((element) => {
                    //     return element === requestData[i].CodeComponent ? true : false
                    // })) {
                    multi_lang_question.question = requestData[i].Question;
                    multi_lang_question.main_question_id = requestData[i].Code;
                    multi_lang_question.option3 = 'NAN';
                    multi_lang_question.option4 = 'NAN';
                    multi_lang_question.option5 = 'NAN';
                    if (requestData[i].Language === 'ENG') {
                        multi_lang_question.option1 = 'Yes';
                        multi_lang_question.option2 = 'No';
                        multi_lang_question.language = 'en-US';
                    } else if (requestData[i].Language === 'SPA') {
                        multi_lang_question.option1 = 'Si';
                        multi_lang_question.option2 = 'No';
                        multi_lang_question.language = 'es-ES';
                    }
                    multiQuestionArr.push(multi_lang_question);
                    // } else {
                    //     // console.log('data already exists');
                    // }
                    // console.log('multiQuestionArr..........', multiQuestionArr);

                }
                else if (type == "tank_truck") {
                    question.questionId = requestData[i].CodeComponent;
                    question.question = requestData[i].Component;


                    if (requestData[i].Criteria === 'Medium' || requestData[i].Criteria === 'Medio') {
                        question.questgroup_id = 24;
                        question.priority_id = 3
                    }
                    if (requestData[i].Criteria === 'High' || requestData[i].Criteria === 'Alto') {
                        question.questgroup_id = 23
                        question.priority_id = 2
                    }
                    questionArr.push(question);

                    // console.log('questionArr', questionArr)
                    // if (finalArray.find((element) => {
                    //     return element === requestData[i].CodeComponent ? true : false
                    // })) {
                    // console.log('in if')
                    multi_lang_question.question = requestData[i].Component;
                    multi_lang_question.main_question_id = requestData[i].CodeComponent;
                    multi_lang_question.option3 = 'NAN';
                    multi_lang_question.option4 = 'NAN';
                    multi_lang_question.option5 = 'NAN';
                    if (requestData[i].Language === 'ENG') {
                        multi_lang_question.option1 = 'Yes';
                        multi_lang_question.option2 = 'No';
                        multi_lang_question.language = 'en-US';
                    } else if (requestData[i].Language === 'SPA') {
                        multi_lang_question.option1 = 'Si';
                        multi_lang_question.option2 = 'No';
                        multi_lang_question.language = 'es-ES';
                    }
                    multiQuestionArr.push(multi_lang_question);
                    // } else {
                    //     // console.log('data already exists');
                    // }
                    // console.log('multiQuestionArr..........', multiQuestionArr);
                    // self checkout
                }

                else if (type == "medical") {
                    question.cc_id = 5;
                    question.rm_id = 2;
                    question.questionId = requestData[i].Title;
                    question.question = requestData[i].OData__x0072_hu9;
                    question.correct_answer = requestData[i].pnxf

                    if (requestData[i].nhhi === 'Medium' || requestData[i].nhhi === 'Medio') {
                        question.questgroup_id = 20;
                        question.priority_id = 8
                    }

                    question.type_of_input = requestData[i].QuestionType
                    questionArr.push(question);


                    // console.log('requestData........',requestData[i])
                    // console.log('finalArray.........',finalArray)
                    // if (finalArray.find((element) => {
                    //     return element === requestData[i].Title ? true : false
                    // })) {
                    multi_lang_question.question = requestData[i].OData__x0072_hu9;
                    multi_lang_question.main_question_id = requestData[i].Title;
                    multi_lang_question.option3 = 'NAN';
                    multi_lang_question.option4 = 'NAN';
                    multi_lang_question.option5 = 'NAN';

                    if (requestData[i].ka8q === 'ENG') {

                        multi_lang_question.option1 = 'Yes';
                        multi_lang_question.option2 = 'No';

                        multi_lang_question.language = 'en-US';
                    } else if (requestData[i].ka8q === 'SPA') {
                        multi_lang_question.option1 = 'Si';
                        multi_lang_question.option2 = 'No';
                        multi_lang_question.language = 'es-ES';
                    }
                    multi_lang_question.option3 = "NAN"
                    multi_lang_question.option4 = "NAN"
                    multi_lang_question.option5 = "NAN"
                    multiQuestionArr.push(multi_lang_question);
                    // }
                }

                try {
                    // console.log("question=====",question)
                    // console.log("MUlti lang question===",multi_lang_question)
                    // console.log("aaaaaa=",question.questionId)
                    // console.log("BBBBBBB=",multi_lang_question.language)
                    await Minetellusers.app.models.multi_lang_question.upsertWithWhere(
                        {
                            main_question_id: question.questionId,
                            language: multi_lang_question.language
                        },
                        multi_lang_question
                    )

                    await Minetellusers.app.models.questions.upsertWithWhere(
                        {
                            questionId: question.questionId,
                        },
                        question
                    )
                } catch (error) {
                    // do nothing
                    console.log(error)
                }
            }
        }

        return {
            "status": "ok"
        }



    }
    Minetellusers.remoteMethod("importQuestionsFromSharePoint", {
        description: "(custom) remote method to import questions from sharepoint",
        createdOn: "2nd December, 2019",
        updatedOn: "2nd December,2019",
        createdBy: "KP",
        accepts: [],
        http: {
            path: "/importQuestionsFromSharePoint",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })


    Minetellusers.importAllQuestionsFromAllChecklistFromSharePoint = async function () {
        var checklist = [
            ["/_api/web/lists/getByTitle('Light Vehicles checklist')/items?$top=200", "light_vehicle"],
            ["/_api/web/lists/getByTitle('Self Check Questions')/items?$top=200", "self_checkout"],
            ["/_api/web/lists/getByTitle('Tank Trucks Checklist')/items?$top=100", "tank_truck"],
            ["/_api/web/lists/getByTitle('Medical Assessment')/items?$top=100", "medical"]
        ]

        for (var i = 0; i < checklist.length; i++) {
            console.log(checklist[i])
            try {
                await this.importQuestionsFromSharePoint(checklist[i][0], checklist[i][1])
            } catch (error) {
                // console.log(error)
                throw error
            }
        }

        return {
            "status": "ok"
        }

    }
    Minetellusers.remoteMethod("importAllQuestionsFromAllChecklistFromSharePoint", {
        description: "(custom) remote method to import questions from sharepoint",
        createdOn: "4th December, 2019",
        updatedOn: "4th December,2019",
        createdBy: "the fallen one",
        accepts: [],
        http: {
            path: "/importAllQuestionsFromAllChecklistFromSharePoint",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })
}
