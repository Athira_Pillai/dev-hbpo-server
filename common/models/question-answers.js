'use strict';
const request = require('request-promise');
const minetellLink = require('../../server/url')
const config = require('../../config')
const moment = require('moment-timezone')
const question = require('../../server/js/question');
module.exports = function (Questionanswers) {
    Questionanswers.criticalControlAvgPoints = async function (criticalControl) {
        console.log('question answer instance', criticalControl);
        // console.log('minetellLink', minetellLink);

        let criticalControlResponse;
        const config = {
            params: {
                period: 'current'
            }
        };
        JSON.parse(criticalControl).forEach(async(cc_id) =>{
        const options = {
            method: 'GET',
            uri: `${minetellLink.url}/v1/api/critical-controls/` + `${cc_id}` + `?period=current`
        }
        criticalControlResponse = await request(options)
        // console.log('criticalControlResponse', criticalControlResponse)
        return criticalControlResponse;
    })
    }
    Questionanswers.remoteMethod('criticalControlAvgPoints', {
        descriptions: 'Custom remote method to trigger critical control avg point api',
        createdOn: '6th January 2020',
        updatedOn: "6th January 2020",
        createdBy: "KP",
        accepts: { arg: "criticalControl", type: 'string' },
        http: {
            path: "/criticalControlAvgPoints",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })
    Questionanswers.perfMovingAvgPoints = async function (questionID) {
        // console.log('questionID in perfMovingAvgPoints remote method', questionID);
        let perfMovingAvgResponse;
        let options;
        questionID.forEach(async(_questionId) => {
             options = {
                method: 'GET',
                uri: `${minetellLink.url}/v1/api/questions/` + `${_questionId}` + `?period=week`
            }
            perfMovingAvgResponse = await request(options)
            // console.log('perfMovingAvgResponse', perfMovingAvgResponse)
            return perfMovingAvgResponse;
        })
    }
    Questionanswers.remoteMethod('perfMovingAvgPoints', {
        descriptions: 'Custom remote method to trigger performance requirement avg point api',
        createdOn: '15th January 2020',
        updatedOn: "15th January 2020",
        createdBy: "KP",
        accepts: { arg: "questionID", type: 'array' },
        http: {
            path: "/perfMovingAvgPoints",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })
    Questionanswers.perfColorSummary = async function (period, performanceId, locationId) {
        // console.log('daterange in perfColorSummary', +period.periodId)
        // console.log('performanceId in perfColorSummary', performanceId)
        // console.log('performanceId in perfColorSummary', typeof performanceId)
        console.log('locationId in perfColorSummary', locationId)
        const end = moment().tz(config.defaultTimeZone).endOf('day').toDate()
        const start = moment(end).subtract(+period.periodId, 'days').toDate()
        const dateRange = {
            start: start,
            end: end
        }
        // console.log('daterange in perfColorSummary', dateRange);
        const perfAverage = Questionanswers.app.models.perf_average;
        let perfAverageData = [];
        let performanceAvgArray = [];
        let performanceMovingAvgArray = [];
        const response = {}
        try {
            perfAverageData = await perfAverage.find({
                where: {
                    date: {
                        between: [dateRange.start, dateRange.end]
                    },
                },
                include:{
                    relation:'performance_requirements',
                    scope:{
                        include: {
                            relation: "questions",
                            scope:{
                                where:{
                                    questionId: performanceId
                                },
                                include:{
                                    relation:'question_answers',
                                    scope:{
                                        include:{
                                            relation:'sessions',
                                            // scope:{
                                            //     where:{
                                            //         locationId: locationId != 'undefined' || locationId !={} ? locationId : undefined
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            }
                          }
                    }
                }
            })
        }catch(err){
            throw err;
        }
        var a = moment(dateRange.end);
        var b = moment(dateRange.start)
       
        // console.log('a-b', a.diff(b,'days'));
        perfAverageData = perfAverageData.filter((_perfObj) => {
            return _perfObj.performance_requirements().questions();
        })
        let labels = [];
       labels =  labelsForDays(+period.periodId)
        console.log('labels', labels);
        // console.log('perfAverageData time', perfAverageData[1].performance_requirements().questions().question_answers())
        // console.log('perfAverageDat label',labels[0])
        // console.log('perfAverageDat label',labels[0] === moment(perfAverageData[0]['date']).format('Do MMM'))
        // console.log('after filter ', labels)

        console.log('after filter perfAverageData length', perfAverageData[0].performance_requirements().questions().question_answers());
        for (let i = 0; i < labels.length; i++) {
          for (let j = 0; j < perfAverageData.length; j++) {
            //   if(perfAverageData[j].performance_requirements().questions().question_answers()[j].sessions()){
                if (moment(perfAverageData[j]['date']).utc().format('Do MMM') == labels[i]) {
                    performanceMovingAvgArray[i] = perfAverageData[j].moving_average;
              performanceAvgArray[i] = perfAverageData[j].average;
            }
            //   }
          }
          if(!performanceAvgArray[i]){
            performanceAvgArray[i] = null;
          } 
            if(!performanceMovingAvgArray[i]){
                performanceMovingAvgArray[i] = performanceMovingAvgArray[i-1] ;
            }
        }
        // console.log('after perfAverageData', perfAverageData);
        console.log('after performanceAvgArray', performanceAvgArray);
        console.log('after performanceMovingAvgArray', performanceMovingAvgArray);

        const model = question(null, performanceId);
        response['performance'] = performanceAvgArray;
        response['moving_average'] = performanceMovingAvgArray;
        response['colourSummary'] = model.colourSummary(performanceAvgArray);
        // console.log(dateRange);
        // console.log('perfAverageData', perfAverageData);
        // console.log('performanceAvgArray', performanceAvgArray);
        // console.log('performanceMovingAvgArray', performanceMovingAvgArray);
        // console.log('response', response);
        return response;
    }
    Questionanswers.remoteMethod('perfColorSummary', {
        descriptions: 'Custom remote method to get performance requirement color summary',
        createdOn: '15th January 2020',
        updatedOn: "15th January 2020",
        createdBy: "KP",
        accepts: [{ arg: "period", type: 'object' }, { arg: 'performanceId', type: 'string' },{arg:'locationId',type:'string'}],
        http: {
            path: "/perfColorSummary",
            verb: "get",
            source: "body"
        },
        returns: {
            root: true
        }
    })
    function labelsForDays(days) {
        // this.logger.log('days in labelsForDays', days);
        const result = [];
        const now = new Date();
        const date = new Date();
//   let monthShortFormat = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        // this.logger.log('days in labelsForDays', now.getDate() + 1);
        date.setDate(now.getDate() + 1 - days);
        while (date <= now) {
          const dateString = '' + moment(date).format('Do MMM');
          result.push(dateString);
          date.setDate(date.getDate() + 1);
        }
        // this.logger.log(result);
        return result;
      }
};
